﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml;
using System.Xml.XPath;
using System.Windows.Forms;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Xml.Linq;
using System.Security.Cryptography;
using System.IO;
using System.Reflection;

namespace INInsigteManager
{
    public class INManager
    {
        
        public string SessionID { get; set; }
        public string IdAccessLevel { get; set; }
        public string UserName { get; set; }
        public string IdAccess { get; set; }
        public string IdClient { get; set; }
        public string CodUser { get; set; }
        public string CodCliente { get; set; }
        public string CostumPDFLogo { get; set; }

        public string NomCliente { get; set; }
        public string LangCliente { get; set; }
        public string TemaCliente { get; set; }
        public string CorCliente { get; set; } //02/12/2013

        public INinsigteManager.INUser inUser { get; set; }
        
        public string msgError { get; set; }
        public Boolean IsLogged { get; set; }

        private const String connString = ("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");
        private const string COD = "ALPS!22&%TEST90";

        public DataSet ds = new DataSet();

        public string getConn()
        {
            return connString;
        }
        public DataTable publuicacoes;
        public DataTable dtNewsRelated;

        public INManager(String SessionID, String IPAddr, String IPLanAddr)
        {
            this.inUser = new INinsigteManager.INUser(IPAddr, IPLanAddr);
            this.SessionID = SessionID;
        }

        

        public DataTable getTableQuery(String Query, String QueryType)
        {

            SqlConnection myConnection = new SqlConnection(connString);
            try
            {
                myConnection.Open();

                SqlCommand myCommand = new SqlCommand(Query, myConnection);
                myCommand.CommandTimeout = 0;
                SqlDataReader myReader = myCommand.ExecuteReader();

                if (ds.Tables.Contains(QueryType))
                {
                    ds.Tables.Remove(QueryType);
                    //ds.Tables.Clear();
                    DataTable dt = new DataTable(QueryType);
                    ds.Tables.Add(dt);
                    ds.Load(myReader, LoadOption.PreserveChanges, ds.Tables[QueryType]);
                }
                else
                {
                    DataTable dt = new DataTable(QueryType);
                    ds.Tables.Add(dt);
                    ds.Load(myReader, LoadOption.PreserveChanges, ds.Tables[QueryType]);
                }

                return ds.Tables[QueryType];
            }
            catch (Exception ex)
            {
                msgError = ex.Message.ToString();
            }
            finally
            {
                myConnection.Close();
                myConnection.Dispose();
            }

            return null;
        }

        public Dictionary<string,string> pathfile(string directory,string path)
        {
            return _pathfile(directory,path);
        }

        private Dictionary<string, string> _pathfile(string directory,string path)
        {
            string pathOriginal = directory + path;
            //G:\files\2013/04/29/Exame_01052013_p68.pdf 
            if (!File.Exists(pathOriginal))
           {
               //int lastindex = strFileName.Replace("\\","/").LastIndexOf("/");
               //strFileName = "http://insigte.com/ficheiros/" + strFileName.Substring(lastindex);
               directory = "http://insigte.com/ficheiros/";
               
               //pathOriginal = pathOriginal.Replace("\\", "/").Replace(directory, "http://insigte.com/ficheiros/");
           }

            Dictionary<string, string> rtn = new Dictionary<string, string>();
            rtn.Add("directoria", directory);
            rtn.Add("ficheiro", path);
            return rtn;
        }


        

        public String executeSP (String SP, String PARAM_CLIENT)
        {
            SqlConnection myConnection = new SqlConnection(connString);

            try
            {
                myConnection.Open();

                SqlCommand command = new SqlCommand(SP, myConnection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@IdClient", SqlDbType.VarChar).Value = PARAM_CLIENT;
                //command.Parameters.Add("@Name", SqlDbType.DateTime).Value = txtName.Text;
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                return ex.Message.ToString(); 
            }
            finally
            {
                myConnection.Close();
                myConnection.Dispose();
            }

            return "OK";
        }

        public SqlDataReader getQuery(String Query)
        {
            SqlConnection myConnection = new SqlConnection(connString);

            try
            {
                myConnection.Open();

                SqlDataReader myReader = null;
                SqlCommand myCommand = new SqlCommand(Query, myConnection);
                myReader = myCommand.ExecuteReader();

                return myReader;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                myConnection.Close();
                myConnection.Dispose();
            }
        }

        protected void _Log(String cmd)
        {

        }

        public Boolean Login(String username, String Password)
        {
            username.Replace("'", "''");
            Password.Replace("'", "''");

            UserName = username;
            IsLogged = false;

            SqlConnection myConnection = new SqlConnection(connString);
            try
            {
                myConnection.Open();
                SqlCommand myCommand = new SqlCommand("select a.ID_CLIENT_ACCESS, a.ID_CLIENT, a.USERNAME, a.PASSWORD, a.ID_ACCESS_LEVEL from dbo.CL10H_CLIENT_ACCESS a with(nolock) join dbo.CL10H_CLIENT_USERS u with(nolock) on u.ID_CLIENT_USER = a.ID_CLIENT_ACCESS and u.ID_CLIENT = a.ID_CLIENT where UPPER(username) = UPPER('" + username + "')", myConnection);
                SqlDataReader myReader = myCommand.ExecuteReader();

                while (myReader.Read())
                {
                    if (myReader["PASSWORD"].ToString() == Password)
                    {
                        IsLogged = true;

                        this.UserName = myReader["USERNAME"].ToString();
                        this.IdAccess = myReader["ID_CLIENT_ACCESS"].ToString();
                        this.IdAccessLevel = myReader["ID_ACCESS_LEVEL"].ToString();
                        this.IdClient = myReader["ID_CLIENT"].ToString();
                        this.CodUser = myReader["USERNAME"].ToString().Split('@').First();
                        this.CodCliente = myReader["USERNAME"].ToString().Split('@').Last();

                        getClientInfo();
                        getUserInfo();
                        getUserTemas();

                    }
                }
            }
            catch (Exception ex)
            {
                msgError = ex.Message.ToString();
            }
            finally
            {
                myConnection.Close();
                myConnection.Dispose();
            }
            return IsLogged;
        }

        protected void getUserInfo()
        {
            SqlConnection myConnection = new SqlConnection(connString);
            try
            {
                myConnection.Open();
                SqlCommand myCommand = new SqlCommand("select * from dbo.CL10H_CLIENT_USERS with(nolock) where id_client = '" + this.IdClient + "' and cod_user = '" + this.CodUser + "'", myConnection);
                SqlDataReader myReader = myCommand.ExecuteReader();

                while (myReader.Read())
                {
                    
                    this.inUser.IdClientUser = myReader["ID_CLIENT_USER"].ToString();
                    this.inUser.Name = myReader["NOM_USER"].ToString();
                    this.inUser.Email = myReader["EMAIL_USER"].ToString();

                    this.inUser.UserVisability = myReader["COD_USER_VISABILITY"].ToString();
                    this.inUser.DashVisability = myReader["COD_DASHBOARD_VISABILITY"].ToString();

                    //---------------------------------Para descontinuar e substituir pelo codigo acima.
                    this.inUser.HasPastas = Convert.ToInt32(myReader["IND_PASTAS"].ToString());
                    this.inUser.HasAlerts = Convert.ToInt32(myReader["IND_ALERTS"].ToString());
                    this.inUser.HasAdvSearch = Convert.ToInt32(myReader["IND_ADV_SEARCH"].ToString());
                    this.inUser.HasDashBoard = Convert.ToInt32(myReader["IND_DASHBOARD"].ToString());
                    //----------------------------------------------------------------------------------

                    this.inUser.CodLanguage = myReader["COD_USER_LANGUAGE"].ToString();

                    this.inUser.LastOnline = DateTime.Now;

                    this.inUser.Cores = myReader["COD_TEMAS_ACCESS"].ToString().Split(';');
                    this.inUser.UserPortalFilter = myReader["COD_EDITORS_FILTER"].ToString();

                    updateUserState();
                }
            }
            catch (Exception ex)
            {
                msgError = ex.Message.ToString();
            }
            finally
            {
                myConnection.Close();
                myConnection.Dispose();
            }
        }

        protected void getClientInfo()
        {
            SqlConnection myConnection = new SqlConnection(connString);
            try
            {
                myConnection.Open();
                SqlCommand myCommand = new SqlCommand("select * from dbo.CL10D_CLIENT with(nolock) where id_client = '" + this.IdClient + "' ", myConnection);
                SqlDataReader myReader = myCommand.ExecuteReader();

                while (myReader.Read())
                {
                    this.NomCliente = myReader["NOM_CLIENT"].ToString();
                    this.LangCliente = myReader["COD_PAIS"].ToString();
                    this.TemaCliente = myReader["COD_CLIENT_TEMA"].ToString();
                    this.CorCliente = myReader["COD_CLIENT_COR"].ToString();//02/12/2013
                    this.CostumPDFLogo = myReader["IND_CPDF_LOGO"].ToString();
                }
            }
            catch (Exception ex)
            {
                msgError = ex.Message.ToString();
            }
            finally
            {
                myConnection.Close();
                myConnection.Dispose();
            }
        }

        protected void getUserTemas()
        {
            String SQLqueryCli = " select * from  CL10H_CLIENT_SUBJECT with(nolock) where id_client = " + this.IdClient + " and id_client_user = " + this.inUser.IdClientUser + " order by COD_TAB desc ";
            DataTable CliTemas = new DataTable();
            CliTemas = getTableQuery(SQLqueryCli, "CliTemas");

            this.inUser.Temas.Clear();

            foreach (DataRow row in CliTemas.Rows)
            {
                INinsigteManager.UserTemas x = new INinsigteManager.UserTemas();

                if (this.inUser.CodLanguage != "pt")
                {
                    x.Name = row["DES_TAB" + "_" + this.inUser.CodLanguage.ToUpper()].ToString();
                }
                else
                {
                    x.Name = row["DES_TAB"].ToString();
                }
                
                x.IdTema = row["COD_TEMA_ACCESS"].ToString();
                x.SubTemas = row["COD_SUBTEMAS"].ToString();
                x.FILTER_EDITORS_HOMEPAGE = row["COD_EDITORS_HOMEPAGE"].ToString();
                x.FILTER_EDITORS_NEWSLETTER = row["COD_EDITORS_NEWSLETTER"].ToString();
                x.FILTER_EDITORS_PESQUISA = row["COD_EDITORS_PESQUISA"].ToString();
                x.FILTER_EDITORS_TEMA = row["COD_EDITORS_TEMAS"].ToString();
                x.TabOrder = Convert.ToInt32(row["COD_TAB"].ToString());
                x.BackDays = Convert.ToInt32(row["NUM_BACK_DAYS"].ToString());
                x.TemaCores = row["COD_TEMA_COLORS"].ToString();
                x.IndBrandMining = Convert.ToInt32(row["IND_BRANDMINING"].ToString());
                this.inUser.Temas.Add(Convert.ToInt32(row["ID_CLIENT_SUBJECT"].ToString()), x);


            }

        }

        public void updateUserState()
        {
            if (this.inUser.IPAddress != "81.84.141.206")
            {
                SqlConnection myConnection = new SqlConnection(connString);
                try
                {
                    myConnection.Open();
                    SqlCommand myCommand = new SqlCommand("update CL10H_CLIENT_USERS set LAST_IP_ADDRESS = '" + this.inUser.IPAddress + "', IP_LAN_ADDRESS = '" + this.inUser.IPLanAddress + "', DAT_LAST_ONLINE = getdate() where id_client = '" + this.IdClient + "' and cod_user = '" + this.CodUser + "'", myConnection);
                    SqlDataReader myReader = myCommand.ExecuteReader();
                }
                catch (Exception ex)
                {
                    msgError = ex.Message.ToString();
                }
                finally
                {
                    myConnection.Close();
                    myConnection.Dispose();
                }
            }
        }

        public String Translate(String texto, String From, String To)
        {
            if (From != To)
            {

                string clientID = "In2013sigteTrans";
                string clientSecret = "BWZ7K5uc0+61tuLmOS9vzAQ03NGeuRmA9KwhiFBzEp8";

                String strTranslatorAccessURI = "https://datamarket.accesscontrol.windows.net/v2/OAuth2-13";
                String strRequestDetails = string.Format("grant_type=client_credentials&client_id={0}&client_secret={1}&scope=http://api.microsofttranslator.com", HttpUtility.UrlEncode(clientID), HttpUtility.UrlEncode(clientSecret));
                System.Net.WebRequest webRequest = System.Net.WebRequest.Create(strTranslatorAccessURI);

                webRequest.ContentType = "application/x-www-form-urlencoded";
                webRequest.Method = "POST";
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(strRequestDetails);
                webRequest.ContentLength = bytes.Length;

                using (System.IO.Stream outputStream = webRequest.GetRequestStream())
                {
                    outputStream.Write(bytes, 0, bytes.Length);
                }

                System.Net.WebResponse webResponse = webRequest.GetResponse();
                System.Runtime.Serialization.Json.DataContractJsonSerializer serializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(typeof(AdmAccessToken));

                //Get deserialized object from JSON stream 
                AdmAccessToken token = (AdmAccessToken)serializer.ReadObject(webResponse.GetResponseStream());
                string headerValue = "Bearer " + token.access_token;


                string txtToTranslate = texto;
                string uri = "http://api.microsofttranslator.com/v2/Http.svc/Translate?text=" + System.Web.HttpUtility.UrlEncode(txtToTranslate) + "&from=" + From + "&to=" + To + "";
                System.Net.WebRequest translationWebRequest = System.Net.WebRequest.Create(uri);
                translationWebRequest.Headers.Add("Authorization", headerValue);
                System.Net.WebResponse response = null;
                response = translationWebRequest.GetResponse();
                System.IO.Stream stream = response.GetResponseStream();
                System.Text.Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                System.IO.StreamReader translatedStream = new System.IO.StreamReader(stream, encode);
                System.Xml.XmlDocument xTranslation = new System.Xml.XmlDocument();
                xTranslation.LoadXml(translatedStream.ReadToEnd());
                return xTranslation.InnerText;
            }

            else
            {
                return texto;
            }

        }

        public String FirstCharToUpper(String input)
        {
            if (String.IsNullOrEmpty(input))
                return input;

            String rsl = "";

            String[] xxx = input.Split(' ');
            foreach (String x in xxx)
            {
                if (x.Length > 2)
                {
                    if (x.Length == 3 && x.Substring(x.Length - 1, 1).ToLower() == "s")
                    {
                        rsl += x + " ";
                    }
                    else
                    {
                        rsl += x.First().ToString().ToUpper() + String.Join("", x.Skip(1)) + " ";
                    }
                }
                else
                {
                    rsl += x + " ";
                }
            }

            return rsl.TrimEnd(' ');
        }

        public void Refresh()
        {

            getUserTemas();

        }

        public string getDecrypt(string cipherText)
        {
            return Decrypt(cipherText);
        }


        public string geEncrypt(string clearText)
        {
            return Encrypt(clearText);
        }

        private string Decrypt(string cipherText)
        {
            string EncryptionKey = COD;
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return HttpUtility.HtmlDecode(cipherText);// cipherText.Replace('+', '_');
        }

        private string Encrypt(string clearText)
        {
            string EncryptionKey = COD;
            byte[] clearBytes = Encoding.Unicode.GetBytes(HttpUtility.HtmlEncode(clearText));//clearText.Replace('_', '+'));
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }

        //public static List<T> ToList<T>(this DataTable datatable) where T : new()
        //{
        //    List<T> Temp = new List<T>();
        //    try
        //    {
        //        List<string> columnsNames = new List<string>();
        //        foreach (DataColumn DataColumn in datatable.Columns)
        //            columnsNames.Add(DataColumn.ColumnName);
        //        Temp = datatable.AsEnumerable().ToList().ConvertAll<T>(row => getObject<T>(row, columnsNames));
        //        return Temp;
        //    }
        //    catch { return Temp; }
        //}

        //private static T getObject<T>(DataRow row, List<string> columnsName) where T : new()
        //{
        //    T obj = new T();
        //    try
        //    {
        //        string columnname = "";
        //        string value = "";
        //        PropertyInfo[] Properties; Properties = typeof(T).GetProperties();
        //        foreach (PropertyInfo objProperty in Properties)
        //        {
        //            columnname = columnsName.Find(name => name.ToLower() == objProperty.Name.ToLower());
        //            if (!string.IsNullOrEmpty(columnname))
        //            {
        //                value = row[columnname].ToString();
        //                if (!string.IsNullOrEmpty(value))
        //                {
        //                    if (Nullable.GetUnderlyingType(objProperty.PropertyType) != null)
        //                    {
        //                        value = row[columnname].ToString().Replace("$", "").Replace(",", "");
        //                        objProperty.SetValue(obj, Convert.ChangeType(value, Type.GetType(Nullable.GetUnderlyingType(objProperty.PropertyType).ToString())), null);
        //                    }
        //                    else
        //                    {
        //                        value = row[columnname].ToString().Replace("%", "");
        //                        objProperty.SetValue(obj, Convert.ChangeType(value, Type.GetType(objProperty.PropertyType.ToString())), null);
        //                    }
        //                }
        //            }
        //        } return obj;
        //    }
        //    catch { return obj; }
        //}

        //public String executeSP(String SP, String PARAM_CLIENT)
        //{
        //    SqlConnection myConnection = new SqlConnection(connString);

        //    try
        //    {
        //        myConnection.Open();

        //        SqlCommand command = new SqlCommand(SP, myConnection);
        //        command.CommandType = CommandType.StoredProcedure;
        //        command.Parameters.Add("@IdClient", SqlDbType.VarChar).Value = PARAM_CLIENT;
        //        //command.Parameters.Add("@Name", SqlDbType.DateTime).Value = txtName.Text;
        //        command.ExecuteNonQuery();

        //    }
        //    catch (Exception ex)
        //    {
        //        return ex.Message.ToString();
        //    }
        //    finally
        //    {
        //        myConnection.Close();
        //        myConnection.Dispose();
        //    }

        //    return "OK";
        //}
    }
}
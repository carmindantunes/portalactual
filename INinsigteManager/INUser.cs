﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace INinsigteManager
{
    public class INUser
    {
        public String IdClientUser { get; set; }
        public String Name { get; set; }
        public String Email { get; set; }
        
        public SortedList<Int32, UserTemas> Temas { get; set; }
        public String IPAddress { get; set; }
        public String IPLanAddress { get; set; }
        public DateTime LastOnline { get; set; }

        //Var de permissoes
        public int HasPastas { get; set; }
        public int HasAlerts { get; set; }
        public int HasAdvSearch { get; set; }
        public int HasDashBoard { get; set; }
        public String UserVisability { get; set; }
        public String DashVisability { get; set; }

        //ControlVariables
        public String SQLLastSearch { get; set; }
        public String RSVLastReport { get; set; }
        public String EmailShareSend { get; set; }

        //Idioma
        public String CodLanguage { get; set; }

        //Cores
        public String[] Cores { get; set; }

        //Filtros
        public String UserPortalFilter { get; set; }

        public INUser(String userIPAddress, String userIPLanAddress)
        {
            Name = "Desconhecido";
            Email = "None";
            HasPastas = 0;
            HasAlerts = 0;
            HasAdvSearch = 0;
            HasDashBoard = 0;
            UserPortalFilter = "*";
            IPAddress = userIPAddress;
            IPLanAddress = userIPLanAddress;
            UserVisability = "";
            DashVisability = "";
            LastOnline = DateTime.Now;
            Cores = new String[] { "#000000", "#000000", "#000000", "#000000" };
            Temas = new SortedList<int, UserTemas>();
            CodLanguage = "pt-PT";
        }

    }

    public class UserTemas
    {
        public String IdTema { get; set; }
        public String Name { get; set; }
        public String SubTemas { get; set; }
        public String FILTER_EDITORS_TEMA { get; set; }
        public String FILTER_EDITORS_NEWSLETTER { get; set; }
        public String FILTER_EDITORS_PESQUISA { get; set; }
        public String FILTER_EDITORS_HOMEPAGE { get; set; }
        public Int32 TabOrder { get; set; }
        public Int32 BackDays { get; set; }
        public String TemaCores { get; set; }
        public Int32 IndBrandMining { get; set; }

        public UserTemas()
        {
            IdTema = "0";
            Name = "Desconhecido";
            FILTER_EDITORS_HOMEPAGE = null;
            FILTER_EDITORS_NEWSLETTER = null;
            FILTER_EDITORS_PESQUISA = null;
            FILTER_EDITORS_TEMA = null;
            BackDays = 0;
            TabOrder = 0;
            IndBrandMining = 0;
            TemaCores = "";
        }
    }


}

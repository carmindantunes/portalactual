﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Recycle.aspx.cs" Inherits="Insigte.Recycle" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script type="text/javascript">
    function chkconfirm() {
        return confirm("<%= getResource("recycleConfirm") %>");
    }

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div id="Geral" style="float:left; padding:0px; margin:0px; height:100%;">
    <div id="infoText" style="float:left; padding:0px; margin: 2px 0px 2px 0px; height:35px; min-height:35px; width:100%"> 
        <div style="margin-top:12px">
            <asp:Label runat="server" ID="lblDataToday" Font-Names="Arial" Font-Size="12px" Visible="false" />
        </div>
    </div>
    <div class="form_caja">
    <asp:DataGrid runat="server" ID="dgNewsRecycleACN" AutoGenerateColumns="false" AllowPaging="True"
     GridLines="None" CellPadding="0" CellSpacing="0" BorderWidth="0px" Visible="false"
     BorderStyle="None" Width="800px" Font-Names="Arial" Font-Size="11px" PageSize="30" AlternatingItemStyle-BackColor="#EEEFF0" PagerStyle-BackColor="#686C6E" 
     PagerStyle-Font-Size="12px" PagerStyle-ForeColor="#ffffff" PagerStyle-Font-Underline="false" PagerStyle-VerticalAlign="Middle" PagerStyle-HorizontalAlign="Center" PagerStyle-Mode="NumericPages">
        <ItemStyle BackColor="White" Height="24px" />
        <AlternatingItemStyle  BackColor="#EEEFF0" />
        <HeaderStyle BackColor="#8D8D8D" Height="24px" Font-Names="Arial" Font-Size="12px"  CssClass="headbg" />
        <FooterStyle Height="24px" Font-Names="Arial" Font-Size="12px"  CssClass="headbg" />
        <Columns>
            <asp:TemplateColumn HeaderText="&nbsp; <%$Resources:insigte.language,defColTitulo%>" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Justify" ItemStyle-ForeColor="#000000" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>&nbsp;
                    <a href="<%# string.Format("Article.aspx?ID={0}", DataBinder.Eval(Container.DataItem, "IDMETA")) %>" target="_self" style="color:#000000; text-decoration:none;" onmouseout="this.style.textDecoration='none';" onmouseover="this.style.textDecoration='underline';"><%# sReturnTitle(DataBinder.Eval(Container.DataItem, "IDMETA").ToString(), DataBinder.Eval(Container.DataItem, "TITLE").ToString())%></a>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="EDITOR" HeaderText="<%$Resources:insigte.language,defColFonte%>" HeaderStyle-HorizontalAlign="Left"
                HeaderStyle-ForeColor="white" ItemStyle-ForeColor="#000000" ItemStyle-HorizontalAlign="Justify" />
            <asp:BoundColumn DataField="DATE" HeaderText="<%$Resources:insigte.language,defColData%>" DataFormatString="{0:d}" HeaderStyle-HorizontalAlign="Center"
                HeaderStyle-ForeColor="white" ItemStyle-ForeColor="#000000" ItemStyle-HorizontalAlign="Center"  ItemStyle-Width="100px" />
            <asp:TemplateColumn HeaderText="<%$Resources:insigte.language,defColAccoes%>" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" >
                <ItemTemplate>
                    <asp:Table ID="Table1" runat="server">
                        <asp:TableRow ID="TableRow1" runat="server">
                            <asp:TableCell ID="TableCell1" runat="server">
                                <a href="<%# string.Format("Article.aspx?ID={0}", DataBinder.Eval(Container.DataItem, "IDMETA")) %>" target="_self" style="color:#000000; text-decoration:none;">
                                    <img src="Imgs/icons/black/png/doc_lines_stright_icon_16.png" width="16px" height="16px" alt="<%= getResource("defColAccoesTexto") %>" title="<%= getResource("defColAccoesTexto") %>" style="border-width:0px;"/>
                                </a>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell2" runat="server">
                                <a href="javascript:window.open('Email.aspx?ID=<%# DataBinder.Eval(Container.DataItem, "IDMETA")%>','CustomPopUp', 'width=400, height=400, menubar=no, resizeble=no, location=no'); void(0)" style="border-width:0px;text-decoration:none;">
                                    <img src="Imgs/icons/black/png/share_icon_16.png" width="16px" height="16px" alt="<%= getResource("defColAccoesShare") %>" title="<%= getResource("defColAccoesShare") %>" style="border-width:0px;"/>
                                </a>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell3" runat="server">
                                <a href="<%# (DataBinder.Eval(Container.DataItem, "page_url").ToString()) == string.Empty ? string.Format("http://insigte.com/ficheiros/{0}", DataBinder.Eval(Container.DataItem, "filepath").ToString()) : string.Format("{0}", DataBinder.Eval(Container.DataItem, "page_url").ToString()) %>" target="_blank"> 
                                    <img src="<%# (DataBinder.Eval(Container.DataItem, "page_url").ToString()) == string.Empty ? "Imgs/icons/black/png/doc_export_icon_16.png" : "Imgs/icons/black/png/globe_1_icon_16.png" %>" width="16px" height="16px" 
                                    alt="<%# (DataBinder.Eval(Container.DataItem, "page_url").ToString()) == string.Empty ? "Download PDF" : "Link" %>" style="border-width:0px;" 
                                    title="<%# (DataBinder.Eval(Container.DataItem, "page_url").ToString()) == string.Empty ? "Download PDF" : "Link" %>"/>
                                </a>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell4" runat="server">
<%--                                <%# sReturnLinkDetalhe(DataBinder.Eval(Container.DataItem, "IDMETA").ToString())%>--%>
                                    <asp:ImageButton ID="bt_Recycle" runat="server" 
                                        OnCommand="bt_Recycle_Command"
                                        CommandArgument='<%# sReturnIdPDF(DataBinder.Eval(Container.DataItem, "IDMETA").ToString())%>'
                                        CommandName='<%# Container.ItemIndex %>'
                                        CausesValidation="True" 
                                        OnClientClick='return chkconfirm();'
                                        ImageUrl='Imgs/icons/black/png/playback_reload_icon_16.png' width='16px' height='16px' alt='<%# getResource("defColAccoesReciclar") %>' title='<%# getResource("defColAccoesReciclar") %>' style='border-width:0px;' >
                                    </asp:ImageButton >
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell5" runat="server" Visible="false">
                                <%# sReturnLinkPDF(DataBinder.Eval(Container.DataItem, "IDMETA").ToString())%>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </ItemTemplate>
            </asp:TemplateColumn>
        </Columns>
    </asp:DataGrid>
    </div>
    <div style="float:right; padding-right:7px;">
        <asp:Table ID="tblbtnAction" runat="server">
            <asp:TableRow ID="TbrbtnAction" runat="server">
                <asp:TableCell ID="tbcbtnAction" runat="server"><asp:Button runat="server" ID="btnClearRecycleList" Text="<%$Resources:insigte.language,defColAccoesLimparTodo%>" CssClass="btn-pdfs" OnClick="btnClearRecycleList_Click" /></asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </div>
    <div style="float:left; padding-right:7px;">
        <asp:Label runat="server" ID="lblWarning" Font-Names="Arial" Font-Size="12px" Visible="false" Font-Bold="true" ForeColor="#000000"/>
    </div>
    
    
</div>
</asp:Content>

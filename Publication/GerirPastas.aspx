﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GerirPastas.aspx.cs" Inherits="Insigte.GerirPastas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div style="width: 180px; padding-top:5px; margin-left:10px; margin-top:50px; ">
    <asp:Table ID="tblArchives" runat="server">
        <asp:TableRow ID="TableRow3" runat="server">
            <asp:TableCell ID="TableCell6" runat="server" VerticalAlign="Middle" >
                <asp:Label ID="lb" runat="server" Text="Pastas:" style="color:#000000; font-size:12px; font-family:Arial;"></asp:Label>
            </asp:TableCell>
            <asp:TableCell ID="TableCell7" runat="server">
                <asp:DropDownList ID="tvPastas" runat="server" AutoPostBack="True" ></asp:DropDownList>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRow10" runat="server">
            <asp:TableCell ID="TableCell0" runat="server" VerticalAlign="Middle" >
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Imgs/nova_pasta.png" Width="16px" Height="16px" ToolTip="Ver Pastas" />
            </asp:TableCell>
            <asp:TableCell ID="TableCell1" runat="server">
                <a href="javascript:window.open('PastAdd.aspx','CustomPopUp', 'width=400, height=400, menubar=no, resizeble=no, location=no'); void(0)" style="text-decoration:underline; color:#000000; font-size:12px; font-family:Arial;">Criar Pasta</a>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRow1" runat="server">
            <asp:TableCell ID="TableCell4" runat="server" VerticalAlign="Middle" >
                <asp:Image ID="Image2" runat="server" ImageUrl="~/Imgs/mod_pasta.png" Width="16px" Height="16px" ToolTip="Ver Pastas" />
            </asp:TableCell>
            <asp:TableCell ID="TableCell5" runat="server">
                <a href="javascript:window.open('PastMod.aspx?PID=<%: getPastaID() %>','CustomPopUp', 'width=400, height=400, menubar=no, resizeble=no, location=no'); void(0)" style="text-decoration:underline; color:#000000; font-size:12px; font-family:Arial;">Modificar Pasta</a>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRow2" runat="server">
            <asp:TableCell ID="TableCell2" runat="server" VerticalAlign="Middle" >
                <asp:Image ID="Image3" runat="server" ImageUrl="~/Imgs/apaga_pasta.png" Width="16px" Height="16px" ToolTip="Ver Pastas" />
            </asp:TableCell>
            <asp:TableCell ID="TableCell3" runat="server" >
                <a href="javascript:window.open('PastRmv.aspx?PID=<%: getPastaID() %>','CustomPopUp', 'width=400, height=400, menubar=no, resizeble=no, location=no'); void(0)" style="text-decoration:underline; color:#000000; font-size:12px; font-family:Arial;">Apagar Pasta</a>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</div>

</asp:Content>

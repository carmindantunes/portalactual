﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using AjaxControlToolkit;

namespace Insigte
{
    public partial class researchcfg : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                fillDD();
            }
        }

        protected void fillDD()
        {
            DataTable Pasta = Global.InsigteManager[Session.SessionID].getTableQuery("select ID_RESEARCH, NOM_RESEARCH from CL10D_RESEARCH with(nolock) where id_client = " + Global.InsigteManager[Session.SessionID].IdClient + " and id_client_user = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser, "getTema");

            DDPasta.Items.Add(new ListItem("Novo", "-1"));
            foreach (DataRow row in Pasta.Rows)
            {
                DDPasta.Items.Add(new ListItem(row["NOM_RESEARCH"].ToString(), row["ID_RESEARCH"].ToString()));
            }

            DDPasta.Visible = true;
            Lbl_Pasta.Visible = true;
            
            if (DDPasta.SelectedValue == "-1")
            {
                DDTema.Visible = false;
                Lbl_Tema.Visible = false;

                Lkb_Tema_Mod.Visible = false;
                Lkb_Tema_Del.Visible = false;

                nov_tema.Visible = false;
                nov_pasta.Visible = true;
            }
            else
            {
                Lkb_Pasta_Mod.Visible = true;
                Lkb_Pasta_Del.Visible = true;

                DataTable Tema = Global.InsigteManager[Session.SessionID].getTableQuery("select ID_RESEARCH_TEMA, NOM_RESEARCH_TEMA from CL10D_RESEARCH_TEMA with(nolock) where id_client = " + Global.InsigteManager[Session.SessionID].IdClient + " and id_client_user = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + " AND ID_RESEARCH = " + DDPasta.SelectedValue, "getSumTema");
                DDTema.Items.Clear();

                DDTema.Items.Add(new ListItem("Novo", "-1"));
                foreach (DataRow row in Tema.Rows)
                {
                    DDTema.Items.Add(new ListItem(row["NOM_RESEARCH_TEMA"].ToString(), row["ID_RESEARCH_TEMA"].ToString()));
                }

                DDTema.Visible = true;
                Lbl_Tema.Visible = true;
                nov_tema.Visible = false;
                nov_pasta.Visible = false;

                Lkb_Tema_Mod.Visible = true;
                Lkb_Tema_Del.Visible = true;

                if (DDTema.SelectedValue == "-1")
                {
                    nov_tema.Visible = true;
                    Lkb_Tema_Mod.Visible = false;
                    Lkb_Tema_Del.Visible = false;
                }
            }
        }

        protected String getResource(String resx)
        {
            return Resources.insigte.language.ResourceManager.GetObject(resx).ToString();
        }

        protected void DDPasta_SelectedIndexChanged(object sender, EventArgs e)
        {
            mod_pasta.Visible = false;
            DDTema.Items.Clear();

            if (DDPasta.SelectedValue == "-1")
            {
                DDTema.Visible = false;
                Lbl_Tema.Visible = false;
                nov_tema.Visible = false;
                nov_pasta.Visible = true;
                Lkb_Pasta_Mod.Visible = false;
                Lkb_Pasta_Del.Visible = false;
                
            }
            else
            {

                DataTable Tema = Global.InsigteManager[Session.SessionID].getTableQuery("select ID_RESEARCH_TEMA, NOM_RESEARCH_TEMA from CL10D_RESEARCH_TEMA with(nolock) where id_client = " + Global.InsigteManager[Session.SessionID].IdClient + " and id_client_user = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + " AND ID_RESEARCH = " + DDPasta.SelectedValue, "getSumTema");

                DDTema.Items.Add(new ListItem("Novo", "-1"));
                foreach (DataRow row in Tema.Rows)
                {
                    DDTema.Items.Add(new ListItem(row["NOM_RESEARCH_TEMA"].ToString(), row["ID_RESEARCH_TEMA"].ToString()));
                }

                DDTema.Visible = true;
                Lbl_Tema.Visible = true;
                nov_pasta.Visible = false;
                nov_tema.Visible = false;

                Lkb_Pasta_Mod.Visible = true;
                Lkb_Pasta_Del.Visible = true;

                if (DDTema.SelectedValue == "-1")
                {
                    nov_tema.Visible = true;
                    Lkb_Tema_Mod.Visible = false;
                    Lkb_Tema_Del.Visible = false;
                }
                else
                {
                    Lkb_Tema_Mod.Visible = true;
                    Lkb_Tema_Del.Visible = true;
                }

            }

        }

        protected void DDTema_SelectedIndexChanged(object sender, EventArgs e)
        {
            mod_tema.Visible = false;
            if (DDTema.SelectedValue == "-1")
            {
                nov_tema.Visible = true;
                Lkb_Tema_Mod.Visible = false;
                Lkb_Tema_Del.Visible = false;
            }
            else
            {
                nov_tema.Visible = false;
                Lkb_Tema_Mod.Visible = true;
                Lkb_Tema_Del.Visible = true;
            }
        }

        protected void Lkb_Pasta_Mod_Click(object sender, EventArgs e)
        {
            mod_pasta.Visible = !mod_pasta.Visible;

            if (mod_pasta.Visible)
            {


                DataTable rst = Global.InsigteManager[Session.SessionID].getTableQuery("select * from CL10D_RESEARCH with(nolock) WHERE ID_RESEARCH = " + DDPasta.SelectedValue, "getPastasResearch");

                foreach (DataRow x in rst.Rows)
                {
                    txt_mod_des_pasta.Text = x["DES_RESEARCH"].ToString();
                    txt_mod_nome_pasta.Text = x["NOM_RESEARCH"].ToString();
                }
            }
        }

        protected void Lkb_Pasta_Del_Click(object sender, EventArgs e)
        {

            String SQL = "";
            SQL = "delete from CL10D_RESEARCH where ID_RESEARCH = " + DDPasta.SelectedValue;
            DataTable rst = Global.InsigteManager[Session.SessionID].getTableQuery(SQL, "DelResearchPasta");
            SQL = "delete from CL10D_RESEARCH_TEMA where ID_RESEARCH = " + DDPasta.SelectedValue;
            DataTable rst2 = Global.InsigteManager[Session.SessionID].getTableQuery(SQL, "DelResearchTemaPasta");
            SQL = "delete from CL10H_NEWS_RESEARCH where ID_RESEARCH = " + DDPasta.SelectedValue;
            DataTable rst3 = Global.InsigteManager[Session.SessionID].getTableQuery(SQL, "DelResearchPastaSubs");

            Response.Redirect("researchcfg.aspx");

        }

        protected void Lkb_Tema_Mod_Click(object sender, EventArgs e)
        {
            mod_tema.Visible = !mod_tema.Visible;

            if (mod_tema.Visible)
            {
                DataTable rst = Global.InsigteManager[Session.SessionID].getTableQuery("select * from CL10D_RESEARCH_TEMA with(nolock) WHERE ID_RESEARCH_TEMA = " + DDTema.SelectedValue, "getPastasResearch");

                foreach (DataRow x in rst.Rows)
                {
                    txt_mod_tema_des.Text = x["DES_RESEARCH_TEMA"].ToString();
                    txt_mod_tema_nome.Text = x["NOM_RESEARCH_TEMA"].ToString();
                }
            }
        }

        protected void Lkb_Tema_Del_Click(object sender, EventArgs e)
        {

            String SQL = "";
            SQL = "delete from CL10D_RESEARCH_TEMA where ID_RESEARCH_TEMA = " + DDTema.SelectedValue;
            DataTable rst = Global.InsigteManager[Session.SessionID].getTableQuery(SQL, "DelResearchTema");
            SQL = "delete from CL10H_NEWS_RESEARCH where ID_RESEARCH_TEMA = " + DDTema.SelectedValue;
            DataTable rst2 = Global.InsigteManager[Session.SessionID].getTableQuery(SQL, "DelResearchTemaSubs");

            Response.Redirect("researchcfg.aspx");

        }

        protected void nova_Pasta_click(object sender, EventArgs e)
        {
            if (DDPasta.Items.FindByText(txt_pasta.Text) != null)
            {
                Lbl_Info.Text = "Já existe uma Pasta com o mesmo nome!";
            }
            else
            {
                String SQL = "";

                SQL += " insert into [dbo].[CL10D_RESEARCH] ";
                SQL += " (ID_CLIENT, ID_CLIENT_USER, NOM_RESEARCH, DES_RESEARCH, DAT_CRIACAO, DAT_MODIFICACAO) ";
                SQL += " select " + Global.InsigteManager[Session.SessionID].IdClient + ", " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + ", '" + txt_pasta.Text.Replace("'", "''") + "', '" + txt_pasta_des.Text.Replace("'", "''") + "', getdate(), getdate()";

                DataTable insTema = Global.InsigteManager[Session.SessionID].getTableQuery(SQL, "InsertResearchTema");

                Response.Redirect("researchcfg.aspx");
            }
        }

        protected void novo_Tema_click(object sender, EventArgs e)
        {
            if (DDTema.Items.FindByText(txt_tema.Text) != null)
            {
                Lbl_Info.Text = "Já existe um Tema com o mesmo nome!";
            }
            else
            {
                String SQL = "";

                SQL += " insert into [dbo].[CL10D_RESEARCH_TEMA] ";
                SQL += " (ID_RESEARCH, ID_CLIENT, ID_CLIENT_USER, NOM_RESEARCH_TEMA, DES_RESEARCH_TEMA, DAT_CRIACAO, DAT_MODIFICACAO) ";
                SQL += " select " + DDPasta.SelectedValue + ", " + Global.InsigteManager[Session.SessionID].IdClient + ", " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + ", '" + txt_tema.Text.Replace("'", "''") + "', '" + txt_tema_des.Text.Replace("'", "''") + "', getdate(), getdate()";

                DataTable insTema = Global.InsigteManager[Session.SessionID].getTableQuery(SQL, "InsertResearchTema");

                Response.Redirect("researchcfg.aspx");
            }
        }

        protected void LnkTemaMod_click(object sender, EventArgs e)
        {

            String SQL = "";
            SQL += "update CL10D_RESEARCH_TEMA set NOM_RESEARCH_TEMA = '" + txt_mod_tema_nome.Text.Replace("'", "''") + "', DES_RESEARCH_TEMA = '" + txt_mod_tema_des.Text.Replace("'", "''") + "' WHERE ID_RESEARCH_TEMA = " + DDTema.SelectedValue;
            DataTable rst = Global.InsigteManager[Session.SessionID].getTableQuery(SQL,"updResearchTema");
            Lbl_Info.Visible = true;
            Lbl_Info.Text = "Alterado com Sucesso!";
            mod_tema.Visible = false;

            DDTema.SelectedItem.Text = txt_mod_tema_nome.Text.Replace("'", "''");

        }

        protected void LnkPastaMod_click(object sender, EventArgs e)
        {
            
            String SQL = "";
            SQL += "update CL10D_RESEARCH set NOM_RESEARCH = '" + txt_mod_nome_pasta.Text.Replace("'", "''") + "', DES_RESEARCH = '" + txt_mod_des_pasta.Text.Replace("'", "''") + "' WHERE ID_RESEARCH = " + DDPasta.SelectedValue;
            DataTable rst = Global.InsigteManager[Session.SessionID].getTableQuery(SQL, "updResearchPasta");
            Lbl_Info.Visible = true;
            Lbl_Info.Text = "Alterado com Sucesso!";
            mod_pasta.Visible = false;

            DDPasta.SelectedItem.Text = txt_mod_nome_pasta.Text.Replace("'", "''");

        }

    }
}
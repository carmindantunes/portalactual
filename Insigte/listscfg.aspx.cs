﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using AjaxControlToolkit;

namespace Insigte
{
    public partial class listscfg : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(0, 1) == "0")
            {
                Response.Redirect("Default.aspx");
            }

            if (!Page.IsPostBack)
            {
                getListas();
            }

            if (tvListas.SelectedIndex < 0)
            {
                lnkDelete.Visible = false;
                imgDelete.Visible = false;
                lnkModificar.Visible = false;
                imgModificar.Visible = false;
            }
            else
            {
                lnkDelete.Visible = true;
                imgDelete.Visible = true;
                lnkModificar.Visible = true;
                imgModificar.Visible = true;
            }

        }

        protected String getResource(String resx)
        {
            return Resources.insigte.language.ResourceManager.GetObject(resx).ToString();
        }

        public string getListaID()
        {
            if (tvListas.SelectedIndex < 0)
                return string.Empty;
            else
                return tvListas.SelectedItem.Value;
        }

        protected void nova_click(object sender, EventArgs e)
        {
            Response.Redirect("ListAdd.aspx");
        }

        protected void mod_click(object sender, EventArgs e)
        {
            Response.Redirect("ListMod.aspx?PID=" + getListaID());
        }

        protected void del_click(object sender, EventArgs e)
        {
            Response.Redirect("ListRmv.aspx?PID=" + getListaID());
        }


        public void getListas()
        {

            DataTable Lists = Global.InsigteManager[Session.SessionID].getTableQuery("select * from CL10D_LISTS with(nolock) where id_client = " + Global.InsigteManager[Session.SessionID].IdClient + " and id_client_user = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser, "getListas");

            tvListas.Visible = true;

            tvListas.DataSource = Lists;
            tvListas.DataTextField = "NOM_LIST";
            tvListas.DataValueField = "ID_LIST";
            tvListas.DataBind();
            
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using APToolkitNET;

namespace Insigte
{
    public partial class addList : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                string qsIDAutor = Request.QueryString["ID"];

                getAutor(qsIDAutor);
                getLists();
                getLastNews(qsIDAutor);
            }

        }

        protected void getLastNews(String IdAutor)
        {
            DataTable gNews = Global.InsigteManager[Session.SessionID].getTableQuery("select top 5 m.id, m.date, rtrim(ltrim(m.title)) as title, substring(c.text,1,500) + '...' as text, m.editor from metadata m with(nolock) inner join contents c with(nolock) on m.id = c.id where m.authorId = " + IdAutor + " order by m.id desc ", "getAutor");

            rep_last5news.DataSource = gNews;
            rep_last5news.DataBind();
        }

        protected void getAutor(String IdAutor)
        {
            DataTable gaut = Global.InsigteManager[Session.SessionID].getTableQuery("select * from authors with(nolock) where authorid = " + IdAutor, "getAutor");
            foreach (DataRow x in gaut.Rows)
            {
                lt_Autor_photo.Text = "<img src='../ficheiros/autores/" + x["photo"].ToString() + "' alt=\"\" height=\"42px\" width=\"42px\" />";
                lt_Autor_name.Text = x["name"].ToString();
                if (x["email"].ToString().Length > 0)
                    lt_Autor_Email.Text = "(" + x["email"].ToString() + ")";

                if (x["funcao"].ToString().Length > 0)
                    lt_Autor_funcao.Text = "<small>Função: " + x["funcao"].ToString() + "</small>";
                else
                    lt_Autor_funcao.Text = "<small>Função: n/a </small>";

                if (x["facebook"].ToString().Length > 0)
                    fb_desc.Text = "<a href=\"" + (x["facebook"].ToString().Substring(0, 4) == "http" ? x["facebook"].ToString() + "\" target=\"_blank\">" + x["facebook"].ToString() : "http://" + x["facebook"].ToString() + "\" target=\"_blank\">" + x["facebook"].ToString()) + "</a>";
                else
                    fb_desc.Text = "n/a";

                if (x["twitter"].ToString().Length > 0)
                    tw_desc.Text = "<a href=\"" + (x["twitter"].ToString().Substring(0, 4) == "http" ? x["twitter"].ToString() + "\" target=\"_blank\">" + x["twitter"].ToString() : "http://" + x["twitter"].ToString() + "\" target=\"_blank\">" + x["twitter"].ToString()) + "</a>";
                else
                    tw_desc.Text = "n/a";

                if (x["linkedin"].ToString().Length > 0)
                    lk_desc.Text = "<a href=\"" + (x["linkedin"].ToString().Substring(0, 4) == "http" ? x["linkedin"].ToString() + "\" target=\"_blank\">" + x["linkedin"].ToString() : "http://" + x["linkedin"].ToString() + "\" target=\"_blank\">" + x["linkedin"].ToString()) + "</a>";
                else
                    lk_desc.Text = "n/a";

                if (x["phone"].ToString().Length > 0)
                    lt_telefone.Text = x["phone"].ToString();
                else
                    lt_telefone.Text = "n/a";

                if (x["mobile"].ToString().Length > 0)
                    lt_telemovel.Text = x["mobile"].ToString();
                else
                    lt_telemovel.Text = "n/a";

                if (x["adress"].ToString().Length > 0)
                    lt_morada.Text = x["adress"].ToString();
                else
                    lt_morada.Text = "n/a";



            }
        }

        protected void getLists()
        {
            String Query = "select * from CL10D_LISTS with(nolock) ";
            Query += "where ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient + " and ID_CLIENT_USER = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser;

            DataTable Lists = Global.InsigteManager[Session.SessionID].getTableQuery(Query, "getAutor");

            DD_Listas.DataSource = Lists;
            DD_Listas.DataTextField = "NOM_LIST";
            DD_Listas.DataValueField = "ID_LIST";
            DD_Listas.DataBind();
        }

        protected void btAdicionar_Click(object sender, EventArgs e)
        {
            string qsIDAutor = Request.QueryString["ID"];

            String QueryChk = "Select * from CL10H_LIST_AUTOR with(nolock) ";
                   QueryChk += "where ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient + " and ID_CLIENT_USER = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser;
                   QueryChk += " And ID_LISTS = " + DD_Listas.SelectedValue + " and ID_AUTOR = " + qsIDAutor;

            DataTable ChkAutor = Global.InsigteManager[Session.SessionID].getTableQuery(QueryChk, "chkAutor");

            if (ChkAutor.Rows.Count > 0)
            {
                Lbl_Info.Text = "O Autor já se encontra adicionado á lista seleccionada!";
            }
            else
            {
                if (DD_Listas.SelectedValue.Length > 0)
                {
                    String Query = "insert into [dbo].[CL10H_LIST_AUTOR] ";
                    Query += " (ID_CLIENT, ID_CLIENT_USER, ID_LISTS, ID_AUTOR, TXT_NOTA, DAT_CRIACAO, DAT_MODIFICACAO) ";
                    Query += " select " + Global.InsigteManager[Session.SessionID].IdClient + ", " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + ", " + DD_Listas.SelectedValue + ", " + qsIDAutor + ", '" + tbNota.Text.Replace("'", "''") + "', getdate(), getdate() ";

                    DataTable InsAutor = Global.InsigteManager[Session.SessionID].getTableQuery(Query, "InsAutor");
                    Lbl_Info.Text = "Adicionado com sucesso á lista " + DD_Listas.SelectedItem.Text + "!";
                    Response.Write("<script language='javascript'> { self.close() }</script>");
                }
                else
                {
                    Lbl_Info.Text = "Precisa de Selecionar uma lista!";
                }
            }

        }
    }
}
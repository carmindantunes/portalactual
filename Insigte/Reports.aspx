﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Reports.aspx.cs" Inherits="Insigte.Reports" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

<script type="text/javascript">
    $(function () {
        $("#sharePdf-dialog-form").dialog({
            autoOpen: false,
            height: 300,
            width: 350,
            modal: true,
            beforeClose: function (event, ui) {
                $("#MainContent_txt_aux_sharePdf_sendby").val($("#MainContent_pdfsendby").val());
                $("#MainContent_txt_aux_sharePdf_email").val($("#MainContent_pdfemail").val());
                $("#MainContent_txt_aux_sharePdf_comment").val($("#MainContent_pdfcomment").val());
                
                
            }
        });


    });

    function shareThePdf(nId, nTitulo) {
        $('.newDesc').text(nId);
        $("#MainContent_txt_aux_sharePdf_id").val(nId);
        $("#MainContent_txt_aux_sharePdf_link").val(nTitulo);
        $("#sharePdf-dialog-form").dialog("open");
    };

</script>

<style type="text/css">
    #section.sector
    {
        float:left;
        border :1px solid Black;
        width: 100%;
        min-height: 300px;
        min-width:400px;
        color: #000;
        text-decoration: none;
        font-size: 12px;
        font-family:Arial;
        padding:0px;
        margin:0px;
    }
    .form_search
    {
        float:left;
        width: 100%;
        height:10px;
        padding-top: 4px;
        padding-bottom: 10px;
        margin: 0 auto 20px auto;
        background-color: #323232;
        color: #FFFFFF;
    } 
    .doc_link
    {
        float:left;
        width:100%;
        color: #000000;
        line-height:1.5em;
    }
    
    .doc_link:hover
    {
        background-color: #f0f0f0;
    }
    
    .doc_link a
    {
        color: #000000;
    }  
    
    #sharePdf-dialog-form
    {
        display:none;
        height:auto;
        color: #000;
        text-decoration: none;
        font-weight: bold;
        font-size: 12px;
        font-family: Arial;
    }
        
    #sharePdf-dialog-form p.newDesc { color: #000; }
    
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
        <input type="hidden" id="txt_aux_sharePdf_id" runat="server" name="name" value="" />
        <input type="hidden" id="txt_aux_sharePdf_sendby" runat="server" name="name" value="" />
        <input type="hidden" id="txt_aux_sharePdf_email" runat="server" name="name" value="" />
        <input type="hidden" id="txt_aux_sharePdf_comment" runat="server" name="name" value="" />
        <input type="hidden" id="txt_aux_sharePdf_link" runat="server" name="name" value="" />
        
    
        <div id="sharePdf-dialog-form" title="Partilha Dossier">
	        <p class="newDesc"><asp:Literal ID="Ltdesc" runat="server" Text="Partilha Dossier de Imprensa" /></p>
		        <fieldset>
			        <label for="pdfsendby"><asp:Literal ID="Ltsendby" runat="server" Text="<%$Resources:insigte.language,shareFormSendby%>" /></label>
                    <input type="text" runat="server" name="sendby" id="pdfsendby" value="" style="width:90%;"/><br />

                    <label for="pdfemail"><asp:Literal ID="LtEmail" runat="server" Text="<%$Resources:insigte.language,shareFormEmail%>" /></label>
                    <input type="text" runat="server" name="email" id="pdfemail" value="" style="width:90%;"/><br />

                    <label for="pdfcomment"><asp:Literal ID="LtComment" runat="server" Text="<%$Resources:insigte.language,shareFormComm%>" /></label>
                    <input type="text" runat="server" name="comment" id="pdfcomment" value="" style="width:90%;"/><br />

                    <br />
		        </fieldset>
            <p>
                <asp:LinkButton ID="lb_pdf_Share" CssClass="btn-pdfs" ForeColor="White" runat="server" OnClientClick='$("#sharePdf-dialog-form").dialog("close");' OnClick="Share_Pdf_Click" Text="<%$Resources:insigte.language,shareFormBtn%>"></asp:LinkButton>
            </p>
        </div>

<div id="Geral" style="float:left; padding:0px; margin:0px; height:100%; width:800px;">
    <div id="infoText" style="float:left; padding:0px; margin: 2px 0px 2px 0px; height:35px; min-height:35px; width:100%"> 
        <div style="margin-top:12px">
            <asp:Label runat="server" ID="lblDataToday" Font-Names="Arial" Font-Size="12px" Visible="false" />
        </div>
    </div>
    <div id="section" class="sector ui-corner-top" style="float:left;width:100%;">
        <div id="titulo" class="form_search headbg ui-corner-top" style="float:left;width:100%;">
            <div id="caixa" style="float:left;width:80%;">
                &nbsp;<asp:Label runat="server" ID="lblTitleAdvSearch" Font-Names="Arial" Font-Size="12px" ForeColor="#FFFFFF" Text=" <%$Resources:insigte.language,topMnReports%>" />
            </div>
            <div id="help" style="float:left;width:20%; ">
                <div style="float:right;padding-right:5px;">
                    <img alt="Ajuda" src="Imgs/icons/white/png/info_icon_16.png" />
                </div>
            </div>
        </div>
        <div id="sectionBody" style="float:left; width:100%;" >
            <div style="margin: 0px 10px 0px 10px;">
                <asp:DropDownList runat="server" ID="DDDocuments" AutoPostBack="true" OnSelectedIndexChanged="DDDocuments_itemChange">
                </asp:DropDownList><br /><br />
                <asp:Repeater id="repGroups" runat="server" OnItemDataBound='repGroups_OnItemDataBound'>
                    <ItemTemplate>
                        <div class="doc_link">
                            <div style="float:left;width:5%;">
                                <p>
                                    <img src="Imgs/icons/black/png/doc_lines_stright_icon_32.png" alt="" />
                                </p>
                            </div>
                            <div style="float:left;width:85%; line-height:1.5em;">
                                <p>
                                    <asp:LinkButton runat="server" ID="lnk_download" OnCommand="lnk_download_Command" 
                                        CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ID_DOCUMENT").ToString() %>'
                                         ><%# DataBinder.Eval(Container.DataItem, "NOM_DOCUMENT") %></asp:LinkButton><br />
                                    <%--<a href="<%# DataBinder.Eval(Container.DataItem, "FILE_PATH") %>"><%# DataBinder.Eval(Container.DataItem, "NOM_DOCUMENT") %></a><br />--%>
                                    <small><%# getId() %><%# Eval("COD_DOCUMENT_TYPE").ToString() == "4" ? Eval("FILE_PATH").ToString() : Eval("DES_DOCUMENT").ToString() %></small>
                                </p>
                            </div>
                            <div style="float:left;width:5%; line-height:1.5em;">
                                <p>
                                    <%# Eval("COD_DOCUMENT_TYPE").ToString() == "4" ? "<a id=\"share-pdf\" onclick=\"shareThePdf('" + Eval("NOM_DOCUMENT").ToString() + "','" + getId() + Eval("FILE_PATH").ToString() + "')\" title='" + Eval("NOM_DOCUMENT").ToString() + "' style=\"color:#000000; text-decoration:none;\" href=\"#\"><img src=\"Imgs/icons/black/png/share_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"" + getResource("defColAccoesShare") + "\" title=\"" + getResource("defColAccoesShare") + "\" style=\"border-width:0px;\"/> </a>" : ""%>
                                </p>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
</div>

</asp:Content>

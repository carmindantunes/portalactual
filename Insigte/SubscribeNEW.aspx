﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SubscribeNEW.aspx.cs" Inherits="Insigte.SubscribeNEW" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script type="text/javascript">

    function chkconfirm() 
    {
        return confirm("<%= getResource("nlApagarConfig") %>");
    }

    function lbEditores_DoubleClick(value) 
    {
        if ($('#MainContent_ta_editores_codes').tagExist(value)) 
        {
            alert("Editor '" + value + "' já Existe");
        }  
        else 
        {
            if (value == "Todos") 
            {
                $('#MainContent_ta_editores_codes').importTags('');
                $('#MainContent_ta_editores_codes').addTag(value);
            }
            else 
            {
                if ($('#MainContent_ta_editores_codes').tagExist("Todos")) 
                    {
                        $('#MainContent_ta_editores_codes').importTags('');
                        $('#MainContent_ta_editores_codes').addTag(value);
                    } 
                    else 
                    {
                        $('#MainContent_ta_editores_codes').addTag(value);
                    }
            }
        }
    };


//    $(function () {
//        $('.show_on_hover').hide();
//        $('.sector').hover(function () {
//            $(this).find('.show_on_hover').show();
//        },
//        function () {
//            $(this).find('.show_on_hover').hide();
//        }
//        );
//    });

    $(function () {
        $('.show_on_hover').hide();
        $('.sector').hover(function () {
            $(this).find('.show_on_hover').stop(true, true).fadeIn(400);
        },
        function () {
            $(this).find('.show_on_hover').stop(true, true).fadeOut(400);
        }
        );
    });


    $(function () {
        var name = $("#MainContent_name"),
		email = $("#email"),
		password = $("#password"),

		tips = $(".validateTips");
        function updateTips(t) {
            tips
			.text(t)
			.addClass("ui-state-highlight");
            setTimeout(function () {
                tips.removeClass("ui-state-highlight", 1500);
            }, 500);
        }

        $('#MainContent_ta_editores_codes').tagsInput({
            width: '95%',
            interactive: false
         });

    });

    $( document ).ready(function() 
        {

            $("#dialog-form").dialog(
            {
                autoOpen: false,
                height: 200,
                width: 350,
                beforeClose: function (event, ui) 
                {
                    $("#MainContent_txt_aux_novo").val($("#MainContent_name").val());
                },
                modal: true, 
            } );

    $("#criar-novo").click(function () 
        {
            $("#dialog-form").dialog("open");
        });


    });

</script>
<style type="text/css">
       
    a.lbk
    {
        color: #000;
        text-decoration: none;
        font-weight:bold;
        font-size: 12px;
        font-family:Arial;
    }
    
    a.lbk:hover
    {
        color: #000;
        text-decoration: underline;
        font-weight: bold;
        font-size: 12px;
        font-family: Arial;
    }
    
    #dialog-form
    {
        height:auto;
        color: #000;
        text-decoration: none;
        font-weight: bold;
        font-size: 12px;
        font-family: Arial;
    }
    
    #dialog-form p.newDesc { color: #000; }
    
    .ui-widget-header  
    {
        border: 1px solid #aaaaaa; 
        color:White;
        font-weight: bold;
        font-size: 12px;
        font-family: Arial;                     
    }
    
    .actionBtn
    {
        width: 16px;
        height: 16px;
        padding: 1px 2px 3px 2px;
        border: none;
        background: transparent url(Imgs/icons/white/png/checkbox_unchecked_icon_16.png);
    }    
    .actionBtn-snone {
      background: transparent url(Imgs/icons/white/png/checkbox_unchecked_icon_16.png);
    }

    .actionBtn-snone:hover {
      background: transparent url(Imgs/icons/blue/png/checkbox_unchecked_icon_16.png);
    }

    .actionBtn-snone:active {
      background: transparent url(Imgs/icons/black/png/checkbox_unchecked_icon_16.png);
    }
    
    .actionBtn-sall {
      background: transparent url(Imgs/icons/white/png/checkbox_checked_icon_16.png);
    }

    .actionBtn-sall:hover {
      background: transparent url(Imgs/icons/blue/png/checkbox_checked_icon_16.png);
    }

    .actionBtn-sall:active {
      background: transparent url(Imgs/icons/black/png/checkbox_checked_icon_16.png);
    }
    
    fieldset { padding:0; border:0; margin-top:25px; }
    
    #section.sector
    {
        float:left;
        border :1px solid Black;
        width: 100%;
        min-height: 80px;
        min-width:300px;
        color: #000;
        text-decoration: none;
        font-size: 12px;
        font-family:Arial;
        padding:0px;
        margin: 0px 0px 5px 0px;
    }
    
    .form_search
    {
        float:left;
        width: 100%;
        height:10px;
        padding-top: 4px;
        padding-bottom: 10px;
        margin: 0 auto 0px auto;
        background-color: #323232;
        color: #FFFFFF;
    }    
</style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div id="dialog-form" title="<%= getResource("dlgMailTitulo")%>">
	<p class="newDesc"><%= getResource("dlgMailTitulo")%></p>
		<fieldset>
			<label for="name"><%= getResource("advSdlNome")%></label>
            <input type="text" runat="server" name="name" id="name" value="" style="width:90%;"/><br />
		</fieldset>
    <p>
        <asp:LinkButton ID="lb_novo" CssClass="btn-pdfs" ForeColor="White" runat="server" OnClientClick='$("#dialog-form").dialog("close");' OnClick="novo_Click"><%= getResource("advSdlButton")%></asp:LinkButton>
    </p>
</div>
    
<div id="Geral" style="float:left; padding:0px; margin:0px; height:100%; width:800px;">
    <div id="infoText" style="float:left; padding:0px; margin: 2px 0px 2px 0px; height:35px; min-height:35px; width:100%"> 
        <div style="margin-top:8px">
            <div id="TopInfo">
                <asp:DropDownList ID="DDLNewsSub" AutoPostBack="true" Font-Names="Arial" Font-Size="12px" ForeColor="#000000" OnSelectedIndexChanged="getSelected" runat="server"></asp:DropDownList>&nbsp;
                <a id="criar-novo" class="btn-pdfs" href="#"><%= getResource("defcolNovo")%></a>&nbsp;
                <asp:LinkButton CssClass="btn-pdfs" ID="lb_delete" Text="<%$Resources:insigte.language,advSMainDelBtn%>" OnClick="Delete_News_Click" OnClientClick='return chkconfirm();' runat="server"></asp:LinkButton>&nbsp;
                <asp:LinkButton ID="lnk_guardarTop" CssClass="btn-pdfs" runat="server" OnClick="teste_Click"><%= getResource("advSdlButton")%></asp:LinkButton>&nbsp;&nbsp;
                <asp:Label Font-Names="Arial" Font-Size="12px" ForeColor="#000000" runat="server" ID="lblDataToday" Visible="true" />&nbsp;&nbsp;
                <input type="hidden" id="txt_aux_novo" runat="server" name="name" value="" />
            </div>
        </div>
    </div>
    <div id="Data" style="float:left;width:100%;">
        <div id="groups" style="float:left;width:48%;margin-right:20px">
            <asp:Repeater id="repSubscriptions" runat="server">
               <ItemTemplate>
                    <div id="section" class="sector ui-corner-top">
                        <div id="sectionTitle" style="float:left;width:100%;" class="form_search headbg ui-corner-top">
                            <div id="titlel" style="float:left;width:70%;">
                                &nbsp;<asp:Label Font-Names="Arial" Font-Size="12px" ForeColor="#FFFFFF" ID="Label1" runat="server" text='<%# DataBinder.Eval(Container.DataItem, "ItemTitle") %>' ></asp:Label>
                            </div>
                            <div id="titler" style="float:right;width:30%;">
                                <div id="btn_action" class="show_on_hover" style="float:right;margin-right:2px;">
                                    <asp:Button ToolTip="limpar todos" ID="Button1" CssClass="actionBtn actionBtn-snone" runat="server" CommandName="limpar" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"ItemID").ToString() %>' OnClick="Limpar_Click"  />
                                    <asp:Button ToolTip="seleccionar todos" ID="Button2" CssClass="actionBtn actionBtn-sall" runat="server" CommandName="seleccionar" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"ItemID").ToString() %>' OnClick="Limpar_Click"  />
                                </div>
                            </div>
                        </div>
                        <div id="sectionBody" style="float:left; margin:1px;">
                            <asp:CheckBoxList Font-Names="Arial" Font-Size="12px" ID="Chk_temas" CommandName='<%# DataBinder.Eval(Container.DataItem,"ItemID").ToString() %>' runat="server" DataSource='<%# FillinCbl(DataBinder.Eval(Container.DataItem,"ItemID").ToString(),DataBinder.Eval(Container.DataItem,"ItemSort").ToString()) %>' DataTextField="ItemName" DataValueField="ItemValue"></asp:CheckBoxList>
                        </div>
                    </div><br />
               </ItemTemplate>
            </asp:Repeater>
        </div>
        <div id="config" style="float:left;width:48%;">
            <div id="section" class="sector ui-corner-top" style="float:left;width:100%;">
                <div id="sectionTitle" style="float:left;width:100%;" class="form_search headbg ui-corner-top">
                        <span>&nbsp;<%= getResource("defConfiguracao")%></span>
                </div>
                <div id="sectionBody" style="float:left; width:100%; margin:5px 5px 5px 5px;">
                    <div id="alertaCfg" style="float:left; margin:0px 4px 0px 2px; padding: 0px 8px 0px 5px;">
                        <asp:Label runat="server" ID="lb_Alerta" Visible="false">&nbsp;</asp:Label>
                    </div>
                    <asp:Table ID="teet" runat="server" CellPadding="0" CellSpacing="2" Width="100%">
                        <asp:TableRow ID="TableRow2" runat="server" VerticalAlign="Middle">
                            <asp:TableCell Width="20%" ID="TableCell1" runat="server" VerticalAlign="Middle"></asp:TableCell>
                            <asp:TableCell Width="80%" ID="TableCell2" runat="server" VerticalAlign="Middle"><asp:CheckBox ID="chkActivo" runat="server" Text="<%$Resources:insigte.language,defActivar%>" /></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow11" runat="server" VerticalAlign="Middle">
                            <asp:TableCell Width="20%" ID="TableCell21" runat="server" VerticalAlign="Middle"></asp:TableCell>
                            <asp:TableCell Width="80%" ID="TableCell22" runat="server" VerticalAlign="Middle"><asp:CheckBox ID="chkCompact" runat="server" Text="<%$Resources:insigte.language,defFormato%>" /></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow6" runat="server" VerticalAlign="Middle">
                            <asp:TableCell Width="20%" ID="TableCell10" runat="server" VerticalAlign="Middle"></asp:TableCell>
                            <asp:TableCell Width="80%" ID="TableCell11" runat="server" VerticalAlign="Middle">&nbsp;</asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow3" runat="server">
                            <asp:TableCell Width="20%" ID="TableCell3" runat="server" VerticalAlign="Middle"><asp:Label runat="server" Text="<%$Resources:insigte.language,defNsTitulo%>" ID="LbTitulo"></asp:Label></asp:TableCell>
                            <asp:TableCell Width="80%" ID="TableCell4" runat="server" VerticalAlign="Middle"><asp:Label runat="server" ID="LbNomeCli"><%= global_asax.InsigteManager[Session.SessionID].NomCliente + " | " %></asp:Label>&nbsp;<asp:TextBox ID="txt_titulo_email" runat="server" Text=""></asp:TextBox></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow7" runat="server" VerticalAlign="Middle">
                            <asp:TableCell Width="20%" ID="TableCell12" runat="server" VerticalAlign="Middle"></asp:TableCell>
                            <asp:TableCell Width="80%" ID="TableCell13" runat="server" VerticalAlign="Middle">&nbsp;</asp:TableCell>
                        </asp:TableRow>
<%--                        <asp:TableRow ID="TableRow5" runat="server">
                            <asp:TableCell Width="20%" ID="TableCell7" runat="server" VerticalAlign="Middle"></asp:TableCell>
                            <asp:TableCell Width="80%" ID="TableCell8" runat="server" VerticalAlign="Middle">Duplo click para adicionar um editor</asp:TableCell>
                        </asp:TableRow>--%>
                        <asp:TableRow ID="TableRow1" runat="server">
                            <asp:TableCell Width="20%" ID="TableCell9" runat="server" VerticalAlign="Middle"><asp:Label runat="server" ID="lblEmails" Text="Emails " ForeColor="#000000" Visible="false" /></asp:TableCell>
                            <asp:TableCell Width="80%" ID="TableCell19" runat="server" VerticalAlign="Middle"><asp:ListBox runat="server" Width="95%" Height="250px" ID="lbEmails" SelectionMode="Multiple" Visible="false"></asp:ListBox></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow4" runat="server">
                            <asp:TableCell Width="20%" ID="TableCell5" runat="server" VerticalAlign="Middle">&nbsp;&nbsp;</asp:TableCell>
                            <asp:TableCell Width="80%" ID="TableCell6" runat="server" VerticalAlign="Middle"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow8" runat="server">
                            <asp:TableCell Width="20%" ID="TableCell14" runat="server" VerticalAlign="Middle"></asp:TableCell>
                            <asp:TableCell Width="80%" ID="TableCell15" runat="server" VerticalAlign="Middle"><asp:TextBox ID="txt_addEmail" Width="94%" runat="server" Text="" Visible="false" ></asp:TextBox></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow9" runat="server">
                            <asp:TableCell Width="20%" ID="TableCell16" runat="server" VerticalAlign="Middle">&nbsp;&nbsp;</asp:TableCell>
                            <asp:TableCell Width="80%" ID="TableCell17" runat="server" VerticalAlign="Middle"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow5" runat="server">
                            <asp:TableCell Width="20%" ID="TableCell7" runat="server" VerticalAlign="Middle"></asp:TableCell>
                            <asp:TableCell Width="80%" ID="TableCell8" runat="server" VerticalAlign="Middle">
                                <asp:LinkButton ID="lnk_AddEmail" CssClass="btn-pdfs" runat="server" OnClick="AddEmail_Click" Visible="false"><%= getResource("defAdicionar")%></asp:LinkButton>&nbsp;&nbsp;
                                <asp:LinkButton ID="lnk_DelEmail" CssClass="btn-pdfs" runat="server" OnClick="DelEmail_Click" OnClientClick='return confirm("Tem a certeza que deseja apagar os emails seleccionados?");' Visible="false"><%= getResource("advSMainDelBtn")%></asp:LinkButton>&nbsp;&nbsp;
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow10" runat="server">
                            <asp:TableCell Width="20%" ID="TableCell18" runat="server" VerticalAlign="Middle">&nbsp;&nbsp;</asp:TableCell>
                            <asp:TableCell Width="80%" ID="TableCell20" runat="server" VerticalAlign="Middle"></asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </div>
                <div id="thetags" style="float:left; width:100%; height:100%;">
                    <%--<div id="tagstxt" style="float:left; width:100%; margin:5px 5px 5px 5px;">
                        <textarea cols="10" rows="3" runat="server" style="height:80px;" ID="ta_editores_codes"></textarea>
                    </div>--%>
                </div>
            </div>
            <div id="BtGuardarConfig" class="sector ui-corner-top" style="float:left;width:100%;">
                <div id="btnsave" style="float:right; margin:5px 5px 5px 5px;">
                    <asp:LinkButton ID="lnk_guardarCfg" CssClass="btn-pdfs" runat="server" OnClick="teste_Click"><%= getResource("advSdlButton")%></asp:LinkButton>    
                </div>
            </div>
        </div>
    </div>
    <div id="lastbtn" style="float:left;width:100%; margin:10px 0px 5px 0px;">
        <asp:LinkButton ID="lnk_guardarBottom" CssClass="btn-pdfs" runat="server" OnClick="teste_Click"><%= getResource("advSdlButton")%></asp:LinkButton>
    </div>
</div>
</asp:Content>

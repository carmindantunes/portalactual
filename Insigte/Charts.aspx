﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Charts.aspx.cs" Inherits="Insigte.Charts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .form_search
        {
            float: left;
            width: 100%;
            height: 10px;
            padding-top: 4px;
            padding-bottom: 10px;
            margin: 0 auto 20px auto;
            background-color: #323232;
            color: #FFFFFF;
        }
        
        #section.sector
        {
            float: left;
            border: 1px solid Black;
            width: 100%;
            min-height: 300px;
            min-width: 400px;
            color: #000;
            text-decoration: none;
            font-size: 12px;
            font-family: Arial;
            padding: 0px;
            margin: 0px;
        }
        .brnprint
        {
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="Geral" style="float: left; padding: 0px; margin: 0px; height: 100%; width: 800px;">
        <div id="infoText" style="float: left; padding: 0px; margin: 2px 0px 2px 0px; height: 35px;
            min-height: 35px; width: 100%">
            <div style="margin-top: 12px">
                <asp:Label runat="server" ID="lblDataToday" Font-Names="Arial" Font-Size="12px" Visible="false" />
            </div>
        </div>
        <div id="section" class="sector ui-corner-top" style="float: left; width: 100%;">
            <div id="titulo" class="form_search headbg ui-corner-top" style="float: left; width: 100%;">
                <div id="caixa" style="float: left; width: 80%;">
                    &nbsp;<asp:Label runat="server" ID="Label3" Text="Dashboard" Font-Names="Arial" Font-Size="12px"></asp:Label>
                </div>
                <div id="help" style="float: left; width: 20%;">
                    <div style="float: right; padding-right: 5px;">
                        <img alt="Ajuda" src="Imgs/icons/white/png/info_icon_16.png" />
                    </div>
                </div>
            </div>
            <div id="formSearch" style="float: left; background-color: #ffffff; margin: 5px 0px 0px 10px;
                width: 780px;">
                <asp:DropDownList ID="ddpais" Font-Names="Arial" Font-Size="12px" runat="server"
                    AutoPostBack="True" OnSelectedIndexChanged="ddpais_SelectedIndexChanged" Style="float: right;">
                </asp:DropDownList>
                <br />
                <div id="printable" style="width: 100%; float: left;">
                    <div id="divNumNoticias" style="display: block; float: left;">
                        <asp:Label runat="server" ID="lblNN" Visible="false" Text="<%$Resources:insigte.language,dashGraph1%>"
                            Font-Names="Arial" Font-Size="12px"></asp:Label>
                        <center>
                            <asp:Chart runat="server" ID="ChartNN" Visible="true" Width="780px" Height="300px"
                                Palette="Fire">
                                <Series>
                                    <asp:Series Name="Series1">
                                    </asp:Series>
                                </Series>
                                <ChartAreas>
                                    <asp:ChartArea Name="ChartArea1">
                                    </asp:ChartArea>
                                </ChartAreas>
                            </asp:Chart>
                        </center>
                    </div>
                    <br />
                    <div id="divRoiAve"  style="display: block; float: left;">
                        <asp:Label runat="server" ID="lblRoiAve" Visible="false" Text="<%$Resources:insigte.language,dashGraph2%>"
                            Font-Names="Arial" Font-Size="12px"></asp:Label>
                        <center>
                            <asp:Chart runat="server" ID="ChartRoiAve" Visible="false" Width="780px" Height="300px"
                                Palette="Fire">
                                <Series>
                                    <asp:Series Name="Series1">
                                    </asp:Series>
                                </Series>
                                <ChartAreas>
                                    <asp:ChartArea Name="ChartArea1">
                                    </asp:ChartArea>
                                </ChartAreas>
                            </asp:Chart>
                        </center>
                    </div>
                    <br />
                    <div id="divOts" style="display: block; float: left; width: 100%;">
                        <asp:Label runat="server" ID="lblOts" Visible="false" Text="<%$Resources:insigte.language,dashGraph3%>"
                            Font-Names="Arial" Font-Size="12px"></asp:Label>
                        <center>
                            <asp:Chart runat="server" ID="ChartOTS" Visible="false" Width="780px" Height="300px"
                                Palette="Fire">
                                <Series>
                                    <asp:Series Name="Series1">
                                    </asp:Series>
                                </Series>
                                <ChartAreas>
                                    <asp:ChartArea Name="ChartArea1">
                                    </asp:ChartArea>
                                </ChartAreas>
                            </asp:Chart>
                        </center>
                    </div>
                    <br />
                    <div id="divEditAut" style='display: block; float: left; width: 100%;'>
                        <center>
                            <div style="float: left; width: 385px;">
                                <div style="text-align: left;">
                                    <asp:Label runat="server" ID="lblJorn" Visible="false" Text="<%$Resources:insigte.language,dashGraph4%>"
                                        Font-Names="Arial" Font-Size="12px"></asp:Label>
                                </div>
                                <asp:Chart runat="server" ID="ChartEdit" Visible="false" Width="385px" Height="200px"
                                    Palette="Fire">
                                    <Series>
                                        <asp:Series Name="Series1">
                                        </asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="ChartArea1">
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </div>
                            <div style="float: right; width: 385px;">
                                <div style="text-align: left;">
                                    <asp:Label runat="server" ID="lblEdit" Visible="false" Text="<%$Resources:insigte.language,dashGraph5%>"
                                        Font-Names="Arial" Font-Size="12px"></asp:Label>
                                </div>
                                <asp:Chart runat="server" ID="ChartJorn" Visible="false" Width="385px" Height="200px"
                                    Palette="Fire">
                                    <Series>
                                        <asp:Series Name="Series2">
                                        </asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="ChartArea2">
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </div>
                        </center>
                    </div>
                    <%--  <div style="clear: both;">
                </div>--%>
                    <br />
                    <div id="concorrencia" style="display: block; float: left;">
                        <%-- <asp:Label runat="server" ID="Label7" Text="Empresas" Font-Bold="true" Font-Names="Arial"
                                Font-Size="14px" Visible="false" />
                            <br />
                            <br />--%>
                        <asp:Label runat="server" ID="LBConco" Visible="false" Text="<%$Resources:insigte.language,dashGraph6%>"
                            Font-Names="Arial" Font-Size="12px"></asp:Label>
                        <center>
                            <asp:Chart runat="server" ID="ChartConco" Visible="false" Width="780px" Height="300px"
                                Palette="Fire">
                                <Series>
                                    <asp:Series Name="Series1">
                                    </asp:Series>
                                </Series>
                                <ChartAreas>
                                    <asp:ChartArea Name="ChartArea1">
                                    </asp:ChartArea>
                                </ChartAreas>
                            </asp:Chart>
                        </center>
                    </div>
                    <div id="divmercado" style="display: block; float: left;">
                        <center>
                            <br />
                            <div style="text-align: left;">
                                <asp:Label runat="server" ID="lblMerc" Text="<%$Resources:insigte.language,dashGraph7%>"
                                    Font-Bold="True" Font-Names="Arial" Font-Size="14px" Visible="False" />
                                <br />
                                <br />
                            </div>
                            <asp:Chart runat="server" ID="ChartMerc" Width="780px" Height="400px" Palette="Fire"
                                Visible="False">
                                <Series>
                                    <asp:Series Name="Series2">
                                    </asp:Series>
                                </Series>
                                <ChartAreas>
                                    <asp:ChartArea Name="ChartArea2">
                                    </asp:ChartArea>
                                </ChartAreas>
                            </asp:Chart>
                        </center>
                    </div>
                </div>
            </div>
        </div>
        <%--<asp:Button runat="server" ID="export"  UseSubmitBehavior="false" Text="PDF" 
            onclick="export_Click" />--%>
        <%--<button type="button" onclick="javascript:Print(this);return false" id="btnPrint" >Print</button>--%>
        <a href="javascript:void(processPrint('printable'));">Print</a>
        <%--  <label id="btnPrint">
            Print</label>--%>
    </div>
    <%--<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.min.js"></script>
    <script type="text/javascript" src="https://github.com/niklasvh/html2canvas/releases/download/0.5.0-alpha1/html2canvas.js"></script>
    <script type="text/javascript" src="https://github.com/niklasvh/html2canvas/releases/download/0.5.0-alpha1/html2canvas.svg.js"></script>--%>
    <script type="text/javascript" src="Scripts/Print.js"></script>
    <script type="text/javascript">

        //        function convertImgToBase64(url, callback, outputFormat) {
        //            var img = new Image();
        //            img.crossOrigin = 'Anonymous';
        //            img.onload = function () {
        //                var canvas = document.createElement('CANVAS');
        //                var ctx = canvas.getContext('2d');
        //                canvas.height = this.height;
        //                canvas.width = this.width;
        //                ctx.drawImage(this, 0, 0);
        //                var dataURL = canvas.toDataURL(outputFormat || 'image/png');
        //                callback(dataURL);
        //                canvas = null;
        //            };
        //            img.src = url;
        //        }

        function processPrint(printMe) {

            var printContents = document.getElementById(printMe);

            var originalContents = document.body.innerHTML;

//            var sHtmlBody = "<style type='text/css'>"

//            sHtmlBody = sHtmlBody & ' body { width: 100%; height: 100%;margin: 0;padding: 0;background-color: #FAFAFA;font: 12pt "Tahoma";}';
//            sHtmlBody = sHtmlBody & '* {box-sizing: border-box;-moz-box-sizing: border-box;}';
//            sHtmlBody = sHtmlBody & '.page {width: 210mm; min-height: 297mm;padding: 20mm;margin: 10mm auto;border: 1px #D3D3D3 solid;border-radius: 5px;background: white;box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);}';
//            sHtmlBody = sHtmlBody & '.subpage {padding: 1cm;border: 5px red solid;height: 257mm;outline: 2cm #FFEAEA solid;}';
//            sHtmlBody = sHtmlBody & '@page {size: A4; margin: 0;}';
//            sHtmlBody = sHtmlBody & '@media print {html, body {width: 210mm;height: 297mm;}.page {margin: 0;border: initial;border-radius: initial;width: initial;min-height: initial;box-shadow: initial;background: initial;page-break-after: always;}}';

            //            var sHtmlBody = "<style type='text/css'>"
            //            sHtmlBody = sHtmlBody & "  @media print {";
            //            sHtmlBody = sHtmlBody & " #Header, #Footer { display: none !important; }";
            //            sHtmlBody = sHtmlBody & " }";

//            sHtmlBody = sHtmlBody & " </style>"

            //            var aux = sHtmlBody + "<html><head><title>DashBoard</title></head><body><div style='float:left; heigth:842px; width:100%; display:block;'>Garrigues</div>" + printContents.innerHTML + "</body></html>";
//            var aux = "<html><head>" + sHtmlBody + "<title>DashBoard</title></head><body>";
//            aux += '<div class="book">';
//            aux += '<div class="page">';
//            aux += '<div class="subpage">Page 1/2</div>';
//            aux += '</div>';
//            aux += '<div class="page">';
//            aux += '<div class="subpage">Page 2/2</div>';
//            aux += '</div>';
//            aux += '</div>';



//            aux += "</body></html>";

//            var mywindow = window.open('', 'my div', 'height=400,width=600');
//            mywindow.document.write($('#nNoticias').html());
//            mywindow.document.close();
            //            mywindow.print();


            var aux = '';

            aux += ' <div style="height:95px; text-align:center; font-size:20pt; font-family:Calibri;">DashBoard</div>';
            aux += ' <div style="float:left; height:300px; display:block;margin-bottom:20px">' + $('#divNumNoticias').html() + '</div>';
            aux += ' <div style="float:left; height:300px; display:block;margin-bottom:20px">' + $('#divRoiAve').html() + '</div>';
            aux += ' <div style="float:left; height:300px; display:block;margin-bottom:20px">' + $('#divOts').html() + '</div>';
            aux += ' <div style="float:left; height:200px; display:block;margin-bottom:20px">' + $('#divEditAut').html() + '</div>';
            aux += ' <div style="float:left; height:300px; display:block;margin-bottom:20px">' + $('#concorrencia').html() + '</div>';
            aux += ' <div style="float:left; height:400px; display:block;margin-bottom:20px">' + $('#divmercado').html() + '</div>'; 

            document.body.innerHTML = aux;

            //            var doc = new jsPDF();
            //            var specialElementHandlers = {
            //                '#noprint': function (element, renderer) {
            //                    return true;
            //                }
            //            };

            ////            doc.fromHTML(, 15, 15, {
            ////                'width': 170,
            ////                'elementHandlers': specialElementHandlers
            //            //                        });
            //            var imageUrl = $('#MainContent_ChartConco').attr('src');
            //            alert("http://insigte.com/"+ imageUrl);
            //            var img = new Image();
            //            var teste = "";
            //            convertImgToBase64(imageUrl, function (base64Img) {
            //                
            //                teste = base64Img;
            //            });
            //            alert(base64Img);
            ////            doc.addImage(img, 'JPEG', 15, 40, 180, 180);
            //            doc.save("Teste.pdf");

            window.print();
            document.body.innerHTML = originalContents;
            return false;
        };

        //        $(document).ready(function () {
        //            //            $("#form1").submit(function (event) {
        //            //                event.preventDefault();

        //            $('#btnPrint').click(function (e) {
        //                e.preventDefault();
        //                $("#printable").print();
        //                //                alert("Was preventDefault() called: " + e.isDefaultPrevented());
        //                //                    $(document).find("#printable").print({
        //                //                        globalStyles: true,
        //                //                        mediaPrint: true,
        //                //                        stylesheet: null,
        //                //                        noPrintSelector: ".no-print",
        //                //                        iframe: true,
        //                //                        append: null,
        //                //                        prepend: null,
        //                //                        manuallyCopyFormValues: true,
        //                //                        deferred: $.Deferred()
        //                //                    });
        //            });


        //            //            });

        //        });
   

    </script>
</asp:Content>

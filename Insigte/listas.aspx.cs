﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using AjaxControlToolkit;

namespace Insigte
{
    public partial class listas : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Global.InsigteManager[Session.SessionID].IsLogged || Global.InsigteManager[Session.SessionID] == null)
            {
                Response.Redirect("login.aspx", true);
            }

            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(9, 1) == "0")
            {
                Response.Redirect("Default.aspx");
            }

            if (!Page.IsPostBack)
            {
                getLists();
                fillRptLists();

            }

        }

        protected void getLists()
        {

            DataTable dtListas = Global.InsigteManager[Session.SessionID].getTableQuery("select * from CL10D_LISTS with(nolock) where ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient + " and ID_CLIENT_USER= " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser, "getLists");

            DDLists.Items.Clear();
            foreach (DataRow x in dtListas.Rows)
            {
                DDLists.Items.Add(new ListItem(x["NOM_LIST"].ToString(), x["ID_LIST"].ToString()));
            }

        }

        protected void fillRptLists()
        {

            // 19-06-2014 - resolve excepção null quando não foram criadas listas (PR-gerir listas)
            if (DDLists.SelectedIndex < 0)
            { 
            ImageButton1.Visible = false;
            LinkButton1.Visible = false;
            ImageButton2.Visible = false;
            gerirlistas2.Text = gerirlistas2.Text.Replace("#linkgerir", "Gerir");
            gerirlistas2.Text = gerirlistas2.Text.Replace("#linkmanager", "Manage");
            gerirlistas.Visible = true;
            gerirlistas2.Visible = true;
            gerirlistas3.Visible = true;
            }
            else
            {
            gerirlistas.Visible = false;
            gerirlistas2.Visible = false;
            gerirlistas3.Visible = false;

            String Query = "SELECT * FROM [iadvisers].[dbo].[authors] with(nolock) where authorid in (select ID_AUTOR from CL10H_LIST_AUTOR with(nolock) where ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient + " and ID_CLIENT_USER= " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + " and ID_LISTS = " + DDLists.SelectedItem.Value + ") ";

                DataTable dtListas = Global.InsigteManager[Session.SessionID].getTableQuery(Query, "getLists");

                rpt_lists.DataSource = dtListas;
                rpt_lists.DataBind();

                MailTo.Text = getEmails();
            }
        }

        protected void DDLists_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            fillRptLists();
        }

        protected String getEmails()
        {


                String Emails = "";

                String Query = "SELECT * FROM [iadvisers].[dbo].[authors] with(nolock) where authorid in (select ID_AUTOR from CL10H_LIST_AUTOR with(nolock) where ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient + " and ID_CLIENT_USER= " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + " and ID_LISTS = " + DDLists.SelectedItem.Value + ") ";

                DataTable dtListas = Global.InsigteManager[Session.SessionID].getTableQuery(Query, "getLists");

                foreach (DataRow x in dtListas.Rows)
                {
                    if (x["email"].ToString().Length > 0)
                        Emails += x["email"].ToString() + ";";
                }

                return "<a href=\"mailto:" + Emails + "\">Enviar Email</a>";
        }

        protected String getSocial(String ID_AUTOR)
        {
            String Social = "";

            DataTable aut = Global.InsigteManager[Session.SessionID].getTableQuery("select * from authors with(nolock) where authorid = " + ID_AUTOR, "getMorangos");

            lblDataToday.Visible = true;
            foreach (DataRow x in aut.Rows)
            {
                String Facebook = "";
                String twitter = "";
                String linkedin = "";

                if (x["facebook"].ToString().Length > 0)
                    Facebook = x["facebook"].ToString().Substring(0, 4) == "http" ? x["facebook"].ToString() : "http://" + x["facebook"].ToString();
                if (x["twitter"].ToString().Length > 0)
                    twitter = x["twitter"].ToString().Substring(0, 4) == "http" ? x["twitter"].ToString() : "http://" + x["twitter"].ToString();
                if (x["linkedin"].ToString().Length > 0)
                    linkedin = x["linkedin"].ToString().Substring(0, 4) == "http" ? x["linkedin"].ToString() : "http://" + x["linkedin"].ToString();
                //if (x["facebook"].ToString().Length > 0)
                //    lblDataToday.Text += "<br>" + x["facebook"].ToString().Substring(0, 4);

                if (x["facebook"].ToString().Length > 0)
                    Social += "<a style=\"border-width:0px;text-decoration:none;\" href=\"" + Facebook + "\" target=\"_blank\"><img src=\"Imgs/icons/black/png/facebook_icon_16.png\" alt=\"\" height=\"16px\" width=\"16px\" /><small></small>&nbsp;</a>";
                if (x["twitter"].ToString().Length > 0)
                    Social += "<a style=\"border-width:0px;text-decoration:none;\" href=\"" + twitter + "\" target=\"_blank\"><img src=\"Imgs/icons/black/png/twitter_icon_16.png\" alt=\"\" height=\"16px\" width=\"16px\" /><small></small>&nbsp;</a>";
                if (x["linkedin"].ToString().Length > 0)
                    Social += "<a style=\"border-width:0px;text-decoration:none;\" href=\"" + linkedin + "\" target=\"_blank\"><img src=\"Imgs/icons/black/png/linkedin_icon_16.png\" alt=\"\" height=\"16px\" width=\"16px\" /><small></small>&nbsp;</a>";


                String[] subj = x["subjects"].ToString().Split(';');

                foreach (String omg in subj)
                {
                    if (omg != "-1" && omg.Trim().Length > 0)
                        Social += "<small><br>" + omg + "</small>";
                }

            }
            return Social;
        }

        protected String getEditors(String ID_AUTOR)
        {
            String Social = "";

            DataTable aut = Global.InsigteManager[Session.SessionID].getTableQuery("select * from authors with(nolock) where authorid = " + ID_AUTOR, "getMorangos");

            lblDataToday.Visible = true;
            foreach (DataRow x in aut.Rows)
            {
                String[] Editores = x["cod_editorid"].ToString().Split(';');

                foreach (String ed in Editores)
                {
                    DataTable edt = Global.InsigteManager[Session.SessionID].getTableQuery("select * from editors with(nolock) where editorid = '" + ed + "'", "getMorangos");

                    foreach(DataRow z in edt.Rows)
                        Social += z["name"].ToString() + "<br>";
                }

            }
            return Social;
        }

        protected void lnk_download_Command(object sender, CommandEventArgs e)
        {
        }

        protected void lnk_delete_Command(object sender, CommandEventArgs e)
        {
            String ID = e.CommandArgument.ToString();

            String Query = "Delete from CL10H_LIST_AUTOR where ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient + " and ID_CLIENT_USER = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + " and ID_LISTS = " + DDLists.SelectedValue + " and ID_AUTOR = " + ID;

            //lblDataToday.Visible = true;
            //lblDataToday.Text = Query;

            DataTable delAutor = Global.InsigteManager[Session.SessionID].getTableQuery(Query, "DelAutor");

            lblDataToday.Visible = true;
            lblDataToday.Text = "Autor removido com sucesso!";

            getLists();
            fillRptLists();

        }

        protected String getResource(String resx)
        {
            return Resources.insigte.language.ResourceManager.GetObject(resx).ToString();
        }




        protected void btnExportXML_Click(object sender, EventArgs e)
        {
            String Query = "select d.NOM_List as Lista,a.Name as Nome,a.email as Email,a.facebook as Facebook,a.twitter as Twitter,a.linkedin as Linkedin,a.phone as Telefone,a.mobile as Telemovel,a.description as Descrição,a.adress as Morada,a.subjects as Tema,a.funcao as Funcao, ee.name as Editor ";
            Query += "  from authors a with(nolock) ";
            Query += "       inner join [dbo].[CL10H_LIST_AUTOR] l with(nolock) ";
                   Query += "			   on [ID_CLIENT] = "+ Global.InsigteManager[Session.SessionID].IdClient +" ";
                   Query += "			  and [ID_CLIENT_USER] = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + " ";
                   Query += "			  and [ID_LISTS] = " + DDLists.SelectedValue.ToString() + " ";
                   Query += "			  and l.[ID_AUTOR] = a.[authorid] ";
                   Query += "	   inner join CL10D_LISTS d with(nolock) ";
                   Query += "			   on d.[ID_LIST] = l.[ID_LISTS] ";
                   Query += "            	   left outer join editors ee ";
                   Query += "					on substring(a.cod_editorid + ';',0, charindex(';',a.cod_editorid + ';')) = ee.editorid ";


            DataTable dt = Global.InsigteManager[Session.SessionID].getTableQuery(Query, "getAutorsListExcel");

            //Create a dummy GridView
            GridView GridView1 = new GridView();
            GridView1.AllowPaging = false;
            GridView1.DataSource = dt;
            GridView1.DataBind();

            string timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=" + DDLists.SelectedItem.Text.Replace(" ","") + "_" + timestamp + ".xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";

            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                //Apply text style to each Row
                GridView1.Rows[i].Attributes.Add("class", "textmode");
            }
            GridView1.RenderControl(hw);
            //style to format numbers to string

            string style = @"<style> .textmode { mso-number-format:\@; } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net.Mail;
using AjaxControlToolkit;


namespace Insigte
{
    public partial class SubscribeNl : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Global.InsigteManager[Session.SessionID].IsLogged || Global.InsigteManager[Session.SessionID] == null)
            {
                Response.Redirect("login.aspx", true);
            }

            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(1, 1) == "0")
            {
                Response.Redirect("Default.aspx");
            }

            if (!Page.IsPostBack)
            {
                FillDDL();

                if (DDLNewsSub.SelectedIndex < 0)
                {
                    lb_delete.Visible = false;
                    lnk_guardarTop.Visible = false;
                    lnk_guardarCfg.Visible = false;
                    lnk_guardarBottom.Visible = false;
                    chkActivo.Visible = false;
                    chkCompact.Visible = false;
                    LbTitulo.Visible = false;
                    LbNomeCli.Visible = false;
                    txt_titulo_email.Visible = false;

                    //GERAL
                    txt_addEmail.Visible = false;
                    lnk_DelEmail.Visible = false;
                    lnk_AddEmail.Visible = false;
                    lbEmails.Visible = false;
                    lblEmails.Visible = false;

                    lb_Alerta.Visible = true;
                    lb_Alerta.Text = "<p>" + Resources.insigte.language.nlAlertaNovaNL + "</p>";

                }
                else
                {

                    if (Global.InsigteManager[Session.SessionID].CodUser.ToLower() == "admin")
                    {
                        txt_addEmail.Visible = true;
                        lnk_DelEmail.Visible = true;
                        lnk_AddEmail.Visible = true;
                        lbEmails.Visible = true;
                        lblEmails.Visible = true;
                    }

                    FillRepeater();
                    popEditors();
                    getSelected(sender, e);
                }
                
            }
            
        }

        protected String getResource(String resx)
        {
            return Resources.insigte.language.ResourceManager.GetObject(resx).ToString();
        }

        protected void popEditors()
        {
            SqlConnection conn = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");
            
            //try
            //{
            //    conn.Open();
            //    string cmdQuery = string.Empty;
            //    cmdQuery = "select distinct name as editor from dbo.editors ORDER BY name ASC";

            //    SqlCommand cmd = new SqlCommand(cmdQuery);
            //    cmd.Connection = conn;
            //    cmd.CommandType = CommandType.Text;

            //    SqlDataReader reader = cmd.ExecuteReader();

            //    int num = 1;

            //    lbEditores.Items.Insert(0, "Todos");

            //    while (reader.Read())
            //    {
            //        lbEditores.Items.Insert(num, reader.GetValue(0).ToString());
            //        num++;
            //    }
            //}
            //catch (Exception exp)
            //{
            //    exp.Message.ToString();
            //}

        }

        protected void FillRepeater()
        {
            DataTable tempTbl = new DataTable();
            tempTbl.Columns.Add("ItemTitle", typeof(String));
            tempTbl.Columns.Add("ItemID", typeof(String));
            tempTbl.Columns.Add("ItemSort", typeof(Int32));

            DataRow tempRow;
            int tempCount = 0;
            foreach (var a in Global.InsigteManager[Session.SessionID].inUser.Temas)
            {

                tempRow = tempTbl.NewRow();
                tempRow["ItemTitle"] = a.Value.Name;
                tempRow["ItemID"] = a.Value.IdTema;
                tempRow["ItemSort"] = a.Value.TabOrder;
                tempTbl.Rows.Add(tempRow);
                tempCount++;

            }

            //DataView tempView = new DataView(tempTbl);
            DataView tempView = tempTbl.DefaultView;
            tempView.Sort = ("ItemSort desc");
            DataTable tempTblSorted = tempView.ToTable();

            repSubscriptions.DataSource = tempTblSorted;
            repSubscriptions.DataBind();
        }

        protected void Limpar_Click(object sender, EventArgs e)
        {
            String Grupo = ((Button)sender).CommandArgument;
            String Cmd = ((Button)sender).CommandName;

            for (int i = 0; i < repSubscriptions.Items.Count; i++)
            {
                CheckBoxList chk = (CheckBoxList)repSubscriptions.Items[i].FindControl("Chk_temas");

                if (chk.Attributes["CommandName"].ToString() == Grupo)
                {
                    foreach (ListItem li in chk.Items)
                    {
                        if (Cmd == "limpar")
                            li.Selected = false;
                        else
                            li.Selected = true;
                    }
                }
            }

        }

        protected DataTable FillinCbl(String ID, String Tab)
        {
            DataTable tempTbl = new DataTable();
            tempTbl.Columns.Add("ItemName", typeof(String));
            tempTbl.Columns.Add("ItemValue", typeof(String));

            foreach (var a in Global.InsigteManager[Session.SessionID].inUser.Temas)
            {
                if (a.Value.IdTema == ID && a.Value.TabOrder.ToString() == Tab)
                {
                    String[] AuxTema = a.Value.SubTemas.Split(';');

                    String Name = "name";
                    String Name_cfg = "name";
                    if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
                    {
                        Name = "name_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage + " as name";
                        Name_cfg = "name_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage;
                    }

                    if (a.Value.SubTemas.Length == 7 && a.Value.SubTemas.Substring(4,3) == "000")
                    {
                        String SQLTema = "select cast(clienteid as varchar) + cast(subjectid as varchar) as SubTema, " + Name + " from subject with(nolock) where clienteid ='" + a.Value.SubTemas.Substring(0, 4) + "' ";
                        DataTable subTema = new DataTable();
                        subTema = Global.InsigteManager[Session.SessionID].getTableQuery(SQLTema, "subTema");
                        String subs = "";
                        foreach (DataRow row in subTema.Rows)
                        {
                            subs += row["SubTema"].ToString() + ";";
                        }
                        subs = subs.TrimEnd(';');
                        AuxTema = subs.Split(';');
                    }

                    if (a.Value.SubTemas.Length < 7)
                    {

                        AuxTema = new String[] {a.Value.IdTema};
                    }

                    DataRow tempRow;
                    
                    foreach (String tema in AuxTema)
                    {
                        tempRow = tempTbl.NewRow();

                        String SQLqueryTema = "select * from subject with(nolock) where cast(clienteid as varchar) + cast(subjectid as varchar) ='" + tema + "' ";
                        DataTable TemaDes = new DataTable();
                        TemaDes = Global.InsigteManager[Session.SessionID].getTableQuery(SQLqueryTema, "TemaDes");

                        if (TemaDes.Rows.Count > 0)
                        {
                            foreach (DataRow row in TemaDes.Rows)
                            {
                                tempRow["ItemName"] = row[Name_cfg].ToString();
                            }
                        }
                        else
                        {
                            tempRow["ItemName"] = tema;
                        }

                        tempRow["ItemValue"] = tema;
                        tempTbl.Rows.Add(tempRow);
                    }
                }
            }
            return tempTbl;
        }

        protected void FillDDL()
        {
            String SQLquery = "select * from dbo.clientmaildaily with(nolock) where cast(email as varchar(1000)) = '" + Global.InsigteManager[Session.SessionID].inUser.Email + "' and id_client = " + Global.InsigteManager[Session.SessionID].IdClient;
            DataTable dt = new DataTable();
            dt = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "dt");

            DDLNewsSub.DataSource = dt;
            DDLNewsSub.DataTextField = "des_user";
            DDLNewsSub.DataValueField = "clientmaildailyid";
            DDLNewsSub.DataBind();
            
        }

        protected void getSelected(object sender, EventArgs e)
        {
            String SQLquery = "select * from dbo.clientmaildaily with(nolock) where cast(email as varchar(1000)) = '" + Global.InsigteManager[Session.SessionID].inUser.Email + "' and clientmaildailyid = '" + DDLNewsSub.Text + "' and id_client = " + Global.InsigteManager[Session.SessionID].IdClient;
            DataTable dt = new DataTable();
            dt = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "dt");

            String TemasSeleccionados = "";
            String Titulo = "";
            Boolean Activo = false;
            Boolean Compacto = false;
            String Editores = "";
            //ta_editores_codes.Value = "";

            foreach (DataRow row in dt.Rows)
            {
                TemasSeleccionados = row["subjectids"].ToString();
                Titulo = row["emailsubject"].ToString();
                Activo = Convert.ToBoolean(row["active"].ToString());
                Compacto = Convert.ToBoolean(Convert.ToInt32(row["compact"].ToString()));
                Editores = row["editors"].ToString();
            }

            //lblDataToday.Text = TemasSeleccionados;

            for (int i = 0; i < repSubscriptions.Items.Count; i++)
            {
                CheckBoxList chk = (CheckBoxList)repSubscriptions.Items[i].FindControl("Chk_temas");

                foreach (ListItem li in chk.Items)
                {
                    if (TemasSeleccionados.Contains(li.Value))
                        li.Selected = true;
                    else
                        li.Selected = false;
                }
            }

            chkActivo.Checked = Activo;
            chkCompact.Checked = Compacto;
            txt_titulo_email.Text = Titulo;

            if (Global.InsigteManager[Session.SessionID].CodUser.ToLower() == "admin")
            {
                String SQLgeral = "select * from dbo.CL10H_CLIENT_GERAL with(nolock) where ID_CLIENT_NEWSLETTER = '" + DDLNewsSub.Text + "'";
                DataTable dtGeral = new DataTable();
                dtGeral = Global.InsigteManager[Session.SessionID].getTableQuery(SQLgeral, "dtGeral");

                lbEmails.Items.Clear();

                foreach (DataRow emlg in dtGeral.Rows)
                {
                    String[] emailsGerais = emlg["COD_EMAIL"].ToString().Split(',');

                    foreach (String xxx in emailsGerais)
                    {
                        lbEmails.Items.Add(new ListItem(xxx));
                    }

                }

            }

            //foreach (ListItem ed in lbEditores.Items)
            //{
            //    if (Editores.Contains(ed.Value))
            //    {
            //        ed.Selected = true;
            //        ta_editores_codes.Value += ed.Value + ", ";
            //    }
            //    else
            //        ed.Selected = false;
            //}
            //ta_editores_codes.Value = ta_editores_codes.Value.TrimEnd(' ').TrimEnd(',');
            //if (ta_editores_codes.Value.Trim().Length == 0)
            //    ta_editores_codes.Value = "Todos";
        }

        protected void novo_Click(object sender, EventArgs e)
        {
            if ( txt_aux_novo.Value.Length > 0)
            {

                String SQLInsert = " Set Nocount on insert into clientmaildaily ";
                SQLInsert += " (email, id_client, emailsubject, subjectids, emailkeywords, lastsenddate, active, compact, editors, des_user, dat_user_create, dat_user_change) ";
                SQLInsert += " values('" + Global.InsigteManager[Session.SessionID].inUser.Email + "', " + Global.InsigteManager[Session.SessionID].IdClient + " ,'' ,'" + Global.InsigteManager[Session.SessionID].TemaCliente + "','',null , 1, 0, null, '" + txt_aux_novo.Value.Replace("'", "''") + "', getdate(), getdate()) ";
                SQLInsert += " select @@identity as idmail Set Nocount off ";

                lblDataToday.Text = SQLInsert;

                DataTable Insert = new DataTable();
                Insert = Global.InsigteManager[Session.SessionID].getTableQuery(SQLInsert, "Insert");

                String IdMail = "";
                foreach (DataRow row in Insert.Rows) 
                {
                    IdMail = row["idmail"].ToString();

                    if (Global.InsigteManager[Session.SessionID].CodUser.ToLower() == "admin")
                    {
                        String sqlinsgeral = "insert into CL10H_CLIENT_GERAL (ID_CLIENT_NEWSLETTER, COD_EMAIL) select '" + IdMail + "', '' ";
                        DataTable insgeral = new DataTable();
                        insgeral = Global.InsigteManager[Session.SessionID].getTableQuery(sqlinsgeral, "insgeral");
                    }


                    lblDataToday.Text = Resources.insigte.language.defCofNew;
                    ListItem i = new ListItem(txt_aux_novo.Value.Replace("'", "''"), row["idmail"].ToString());
                    DDLNewsSub.Items.Add(i);
                    DDLNewsSub.SelectedValue = i.Value;

                    Response.Redirect("SubscribeNl.aspx");
                    //FillRepeater();
                    //popEditors();
                    //getSelected(sender, e);
                }
            }
            else
            {
                lblDataToday.Text = Resources.insigte.language.defConfInvalid;
            }
        }

        protected void Delete_News_Click(object sender, EventArgs e)
        {
            String SQLDelArt = " delete from clientmaildaily where clientmaildailyid= " + DDLNewsSub.SelectedValue + " and cast(email as varchar(100)) = '" + Global.InsigteManager[Session.SessionID].inUser.Email + "' and id_client = " + Global.InsigteManager[Session.SessionID].IdClient;
            DataTable DelNewsArt = new DataTable();
            DelNewsArt = Global.InsigteManager[Session.SessionID].getTableQuery(SQLDelArt, "DelNewsArt");

            if (Global.InsigteManager[Session.SessionID].CodUser.ToLower() == "admin")
            {
                String SQLDelgeral = " delete from CL10H_CLIENT_GERAL where ID_CLIENT_NEWSLETTER= " + DDLNewsSub.SelectedValue;
                DataTable DelNewsgeral = new DataTable();
                DelNewsgeral = Global.InsigteManager[Session.SessionID].getTableQuery(SQLDelgeral, "DelNewsgeral");
            }



            lblDataToday.Text = Resources.insigte.language.nlApagarEmail.Replace("@NomeConf",DDLNewsSub.SelectedItem.Text);
            //lblDataToday.Text = SQLDelArt;
            DDLNewsSub.Items.Remove(DDLNewsSub.SelectedItem);
            Response.Redirect("SubscribeNl.aspx");
        }

        protected void teste_Click(object sender, EventArgs e)
        {
            if (DDLNewsSub.SelectedValue == "-1" || DDLNewsSub.SelectedValue == null || DDLNewsSub.SelectedValue == String.Empty)
            {
                lblDataToday.Text = Resources.insigte.language.defConfNeedTemas;
            }
            else
            {

                String Seleccionados = "";
                for (int i = 0; i < repSubscriptions.Items.Count; i++)
                {
                    CheckBoxList chk = (CheckBoxList)repSubscriptions.Items[i].FindControl("Chk_temas");

                    foreach (ListItem li in chk.Items)
                    {
                        if (li.Selected)
                            Seleccionados += li.Value + ",";
                    }
                }
                Seleccionados = Seleccionados.TrimEnd(',');

                //ta_editores_codes.Value = ta_editores_codes.Value.Replace("'", "''");
                //ta_editores_codes.Value = "'" + ta_editores_codes.Value.Replace(",","','") + "'";

                String SQLInsert = " update clientmaildaily ";
                SQLInsert += "    set emailsubject = '" + txt_titulo_email.Text.Replace("'", "''") + "' ";
                SQLInsert += " 	  ,subjectids = '" + Seleccionados.Replace("'", "''") + "' ";
                SQLInsert += " 	  ,emailkeywords = '' ";
                SQLInsert += " 	  ,active = '" + chkActivo.Checked.ToString() + "' ";
                SQLInsert += " 	  ,compact = '" + (chkCompact.Checked ? "1" : "0") + "' ";
                //SQLInsert += " 	  ,editors = '" + ta_editores_codes.Value.Replace("'", "''") + "' ";
                SQLInsert += " 	  ,dat_user_change = getdate() ";
                SQLInsert += "  where  clientmaildailyid = '" + DDLNewsSub.SelectedValue + "' and cast(email as varchar(1000)) = '" + Global.InsigteManager[Session.SessionID].inUser.Email + "' and id_client = " + Global.InsigteManager[Session.SessionID].IdClient;

                DataTable Insert = new DataTable();
                Insert = Global.InsigteManager[Session.SessionID].getTableQuery(SQLInsert, "Insert");

                String Emails = "";
                if (Global.InsigteManager[Session.SessionID].CodUser.ToLower() == "admin")
                {
                    foreach (ListItem x in lbEmails.Items)
                    {
                        Emails += x.Value + ",";
                    }

                    Emails = Emails.TrimEnd(',');

                    String SQLupdateGeral = "update CL10H_CLIENT_GERAL set COD_EMAIL = '" + Emails + "' where ID_CLIENT_NEWSLETTER = " + DDLNewsSub.Text;
                    DataTable upGeral = new DataTable();
                    upGeral = Global.InsigteManager[Session.SessionID].getTableQuery(SQLupdateGeral, "upGeral");
                }

                lblDataToday.Text = Resources.insigte.language.defConfsucesso;
            }

        }

        public bool IsValid(string emailaddress)
        {
            try
            {
                MailAddress m = new MailAddress(emailaddress);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        protected void AddEmail_Click(object sender, EventArgs e)
        {
            if (txt_addEmail.Text.Trim().Length > 0 && IsValid(txt_addEmail.Text))
            {
                Boolean notfound = true;

                foreach (ListItem x in lbEmails.Items)
                {
                    if (x.Value.ToLower() == txt_addEmail.Text.ToLower())
                        notfound = false;
                }

                if (notfound)
                    lbEmails.Items.Add(new ListItem(txt_addEmail.Text));
                else
                    lblDataToday.Text = Resources.insigte.language.nlEmailExiste;

                txt_addEmail.Text = "";
            }
            else
            {
                lblDataToday.Text = Resources.insigte.language.nlEmailInvalido;
            }

        }

        protected void DelEmail_Click(object sender, EventArgs e)
        {
            var selected = new List<ListItem>();
            foreach (ListItem listItem in lbEmails.Items)
            {
                if (listItem.Selected == true)
                {
                    selected.Add(listItem);
                    
                }
            }

            foreach (ListItem lt in selected)
            {
                lbEmails.Items.Remove(lt);
            }

        }
    }
}
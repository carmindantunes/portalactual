﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Insigte.UserControls
{
    public partial class Archives : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Global.InsigteManager[Session.SessionID].IsLogged || Global.InsigteManager[Session.SessionID] == null)
            {
                Response.Redirect("login.aspx", true);
            }

            if (!IsPostBack)
            {
                if (Global.InsigteManager[Session.SessionID].IdClient == "3003")
                {
                    LtLink3EA.Visible = true;
                }
                else
                {
                    LtLink3.Visible = true;
                }
            }
        }

    }
}
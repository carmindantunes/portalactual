﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using AjaxControlToolkit;

namespace Insigte
{
    public partial class Fontes : BasePage
    {
        private DataTable Editors;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Global.InsigteManager[Session.SessionID].IsLogged || Global.InsigteManager[Session.SessionID] == null)
            {
                Response.Redirect("login.aspx", true);
            }

            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(6, 1) == "0")
            {
                Response.Redirect("Default.aspx");
            }

            getDataEditors();

            if (!Page.IsPostBack)
            {
                getDataEditors();
                FillRepeater();
                getSelected(sender, e);
            }

        }

        protected void getDataEditors()
        {
            String SQLquery = "select * from dbo.editors with(nolock) order by name asc ";
            if (Global.InsigteManager[Session.SessionID].inUser.UserPortalFilter != "*")
                SQLquery = "select * from dbo.editors with(nolock) where editorid in (" + Global.InsigteManager[Session.SessionID].inUser.UserPortalFilter + ") order by name asc ";
            Editors = new DataTable();
            Editors = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "FilterEditorsTemas");
        }

        protected String getEditorID(String Editor)
        {
            if (Editor == Resources.insigte.language.geralTagTodos)
                return "*";
            else
            {
                foreach (DataRow row in Editors.Rows)
                {
                    if (row["name"].ToString() == Editor)
                        return row["editorid"].ToString();
                }
            }

            return null;
        }

        protected String getEditorName(String Editor)
        {
            if (Editor == "*")
                return Resources.insigte.language.geralTagTodos;
            else
            {
                foreach (DataRow row in Editors.Rows)
                {
                    if (row["editorid"].ToString() == Editor)
                        return row["name"].ToString();
                }
            }

            return null;
        }

        protected void FillRepeater()
        {
            DataTable tempTbl = new DataTable();
            tempTbl.Columns.Add("ItemTitle", typeof(String));
            tempTbl.Columns.Add("ItemID", typeof(String));
            tempTbl.Columns.Add("ItemSort", typeof(String));

            DataRow tempRow;
            int tempCount = 0;
            foreach (var a in Global.InsigteManager[Session.SessionID].inUser.Temas)
            {

                tempRow = tempTbl.NewRow();
                tempRow["ItemTitle"] = a.Value.Name;
                tempRow["ItemID"] = a.Value.IdTema;
                tempRow["ItemSort"] = a.Value.TabOrder;
                tempTbl.Rows.Add(tempRow);
                tempCount++;
            }

            DataView tempView = tempTbl.DefaultView;
            tempView.Sort = ("ItemSort desc");
            DataTable tempTblSorted = tempView.ToTable();
            
            repGroups.DataSource = tempTblSorted;
            repGroups.DataBind();
        }

        protected void repGroups_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            var myLbEditor = (ListBox)e.Item.FindControl("LbEditors");
            var myLbEditorAO = (ListBox)e.Item.FindControl("LbEditorsAO");
            var myLbEditorMZ = (ListBox)e.Item.FindControl("LbEditorsMZ");
            var myLbEditorPT = (ListBox)e.Item.FindControl("LbEditorsPT");
            var myLbEditorOU = (ListBox)e.Item.FindControl("LbEditorsOU");
            //myLbEditor.ToolTip = e.Item.DataItem.ToString();//e.Item.ItemIndex.ToString();

            myLbEditor.Items.Add(Resources.insigte.language.geralTagTodos);
            foreach (DataRow x in Editors.Rows)
            {
                switch (x["country"].ToString())
                {
                    case "Portugal":
                        myLbEditorPT.Items.Add(x["name"].ToString());
                        break;
                    case "Angola":
                        myLbEditorAO.Items.Add(x["name"].ToString());
                        break;
                    case "Moçambique":
                        myLbEditorMZ.Items.Add(x["name"].ToString());
                        break;
                    default:
                        myLbEditorOU.Items.Add(x["name"].ToString());
                        break;
                }

                myLbEditor.Items.Add(x["name"].ToString());
            }


        }

        protected String getResource(String resx)
        {
            return Resources.insigte.language.ResourceManager.GetObject(resx).ToString();
        }

        protected void getSelected(object sender, EventArgs e)
        {
            
            switch (ItemSell.SelectedValue)
            {
                case "Temas":
                    {
                        foreach (KeyValuePair<int, INinsigteManager.UserTemas> pair in Global.InsigteManager[Session.SessionID].inUser.Temas)
                        {
                            for (int i = 0; i < repGroups.Items.Count; i++)
                            {
                                var myLbEditor = (ListBox)repGroups.Items[i].FindControl("LbEditors");
                                System.Web.UI.HtmlControls.HtmlTextArea ta_text_codes = (System.Web.UI.HtmlControls.HtmlTextArea)repGroups.Items[i].FindControl("ta_editores_codes");

                                if (myLbEditor.Attributes["CommandName"].ToString() == pair.Value.IdTema.ToString())
                                {
                                    String[] auxEditors = pair.Value.FILTER_EDITORS_TEMA.Split(',');
                                    String EdiD = "";
                                    foreach (String ed in auxEditors)
                                    {
                                        EdiD += getEditorName(ed) + ",";
                                    }
                                    ta_text_codes.Value = EdiD.TrimEnd(',');
                                }
                            }
                        }
                        break;
                    }
                case "Newsletter":
                    {
                        foreach (KeyValuePair<int, INinsigteManager.UserTemas> pair in Global.InsigteManager[Session.SessionID].inUser.Temas)
                        {
                            for (int i = 0; i < repGroups.Items.Count; i++)
                            {
                                var myLbEditor = (ListBox)repGroups.Items[i].FindControl("LbEditors");
                                System.Web.UI.HtmlControls.HtmlTextArea ta_text_codes = (System.Web.UI.HtmlControls.HtmlTextArea)repGroups.Items[i].FindControl("ta_editores_codes");

                                if (myLbEditor.Attributes["CommandName"].ToString() == pair.Value.IdTema.ToString())
                                {
                                    String[] auxEditors = pair.Value.FILTER_EDITORS_NEWSLETTER.Split(',');
                                    String EdiD = "";
                                    foreach (String ed in auxEditors)
                                    {
                                        EdiD += getEditorName(ed) + ",";
                                    }
                                    ta_text_codes.Value = EdiD.TrimEnd(',');
                                }
                            }
                        }
                        break;
                    }
                case "Pesquisa":
                    {
                        foreach (KeyValuePair<int, INinsigteManager.UserTemas> pair in Global.InsigteManager[Session.SessionID].inUser.Temas)
                        {
                            for (int i = 0; i < repGroups.Items.Count; i++)
                            {
                                var myLbEditor = (ListBox)repGroups.Items[i].FindControl("LbEditors");
                                System.Web.UI.HtmlControls.HtmlTextArea ta_text_codes = (System.Web.UI.HtmlControls.HtmlTextArea)repGroups.Items[i].FindControl("ta_editores_codes");

                                if (myLbEditor.Attributes["CommandName"].ToString() == pair.Value.IdTema.ToString())
                                {
                                    String[] auxEditors = pair.Value.FILTER_EDITORS_PESQUISA.Split(',');
                                    String EdiD = "";
                                    foreach (String ed in auxEditors)
                                    {
                                        EdiD += getEditorName(ed) + ",";
                                    }
                                    ta_text_codes.Value = EdiD.TrimEnd(',');
                                }
                            }
                        }
                        break;
                    }
                case "Homepage":
                    {
                        foreach (KeyValuePair<int, INinsigteManager.UserTemas> pair in Global.InsigteManager[Session.SessionID].inUser.Temas)
                        {
                            for (int i = 0; i < repGroups.Items.Count; i++)
                            {
                                var myLbEditor = (ListBox)repGroups.Items[i].FindControl("LbEditors");
                                System.Web.UI.HtmlControls.HtmlTextArea ta_text_codes = (System.Web.UI.HtmlControls.HtmlTextArea)repGroups.Items[i].FindControl("ta_editores_codes");

                                if (myLbEditor.Attributes["CommandName"].ToString() == pair.Value.IdTema.ToString())
                                {
                                    String[] auxEditors = pair.Value.FILTER_EDITORS_HOMEPAGE.Split(',');
                                    String EdiD = "";
                                    foreach (String ed in auxEditors)
                                    {
                                        EdiD += getEditorName(ed) + ",";
                                    }
                                    ta_text_codes.Value = EdiD.TrimEnd(',');
                                }
                            }
                        }
                        break;
                    }
            }
            

            
            //DataTable dt = new DataTable();
            //dt = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "dt");

        }

        protected void Save_Click(object sender, EventArgs e)
        {
            switch (ItemSell.SelectedValue)
            {

                case "Temas":
                    {
                        foreach (KeyValuePair<int, INinsigteManager.UserTemas> pair in Global.InsigteManager[Session.SessionID].inUser.Temas)
                        {
                            for (int i = 0; i < repGroups.Items.Count; i++)
                            {
                                var myLbEditor = (ListBox)repGroups.Items[i].FindControl("LbEditors");
                                System.Web.UI.HtmlControls.HtmlTextArea ta_text_codes = (System.Web.UI.HtmlControls.HtmlTextArea)repGroups.Items[i].FindControl("ta_editores_codes");

                                if (myLbEditor.Attributes["CommandName"].ToString() == pair.Value.IdTema.ToString())
                                {
                                    String[] auxEditors = ta_text_codes.Value.Split(',');
                                    String EdiD = "";
                                    foreach (String ed in auxEditors)
                                    {
                                        EdiD += getEditorID(ed) + ",";
                                    }

                                    pair.Value.FILTER_EDITORS_TEMA = EdiD.TrimEnd(',');
                                }
                            }
                        }
                        break;
                    }
                case "Newsletter":
                    {
                        foreach (KeyValuePair<int, INinsigteManager.UserTemas> pair in Global.InsigteManager[Session.SessionID].inUser.Temas)
                        {
                            for (int i = 0; i < repGroups.Items.Count; i++)
                            {
                                var myLbEditor = (ListBox)repGroups.Items[i].FindControl("LbEditors");
                                System.Web.UI.HtmlControls.HtmlTextArea ta_text_codes = (System.Web.UI.HtmlControls.HtmlTextArea)repGroups.Items[i].FindControl("ta_editores_codes");

                                if (myLbEditor.Attributes["CommandName"].ToString() == pair.Value.IdTema.ToString())
                                {
                                    String[] auxEditors = ta_text_codes.Value.Split(',');
                                    String EdiD = "";
                                    foreach (String ed in auxEditors)
                                    {
                                        EdiD += getEditorID(ed) + ",";
                                    }

                                    pair.Value.FILTER_EDITORS_NEWSLETTER = EdiD.TrimEnd(',');
                                }
                            }
                        }
                        break;
                    }
                case "Pesquisa":
                    {
                        foreach (KeyValuePair<int, INinsigteManager.UserTemas> pair in Global.InsigteManager[Session.SessionID].inUser.Temas)
                        {
                            for (int i = 0; i < repGroups.Items.Count; i++)
                            {
                                var myLbEditor = (ListBox)repGroups.Items[i].FindControl("LbEditors");
                                System.Web.UI.HtmlControls.HtmlTextArea ta_text_codes = (System.Web.UI.HtmlControls.HtmlTextArea)repGroups.Items[i].FindControl("ta_editores_codes");

                                if (myLbEditor.Attributes["CommandName"].ToString() == pair.Value.IdTema.ToString())
                                {
                                    String[] auxEditors = ta_text_codes.Value.Split(',');
                                    String EdiD = "";
                                    foreach (String ed in auxEditors)
                                    {
                                        EdiD += getEditorID(ed) + ",";
                                    }

                                    pair.Value.FILTER_EDITORS_PESQUISA = EdiD.TrimEnd(',');
                                }
                            }
                        }
                        break;
                    }
                case "Homepage":
                    {
                        foreach (KeyValuePair<int, INinsigteManager.UserTemas> pair in Global.InsigteManager[Session.SessionID].inUser.Temas)
                        {
                            for (int i = 0; i < repGroups.Items.Count; i++)
                            {
                                var myLbEditor = (ListBox)repGroups.Items[i].FindControl("LbEditors");
                                System.Web.UI.HtmlControls.HtmlTextArea ta_text_codes = (System.Web.UI.HtmlControls.HtmlTextArea)repGroups.Items[i].FindControl("ta_editores_codes");

                                if (myLbEditor.Attributes["CommandName"].ToString() == pair.Value.IdTema.ToString())
                                {
                                    String[] auxEditors = ta_text_codes.Value.Split(',');
                                    String EdiD = "";
                                    foreach (String ed in auxEditors)
                                    {
                                        EdiD += getEditorID(ed) + ",";
                                    }

                                    pair.Value.FILTER_EDITORS_HOMEPAGE = EdiD.TrimEnd(',');
                                }
                            }
                        }
                        break;
                    }
            }

            foreach (KeyValuePair<int, INinsigteManager.UserTemas> pair in Global.InsigteManager[Session.SessionID].inUser.Temas)
            {
                String SQLquery = "update CL10H_CLIENT_SUBJECT "; 
                       SQLquery += "  set COD_EDITORS_NEWSLETTER = '"+ pair.Value.FILTER_EDITORS_NEWSLETTER +"'";
                       SQLquery += "     ,COD_EDITORS_TEMAS = '" + pair.Value.FILTER_EDITORS_TEMA + "'";
                       SQLquery += "     ,COD_EDITORS_PESQUISA = '" + pair.Value.FILTER_EDITORS_PESQUISA + "'";
                       SQLquery += "     ,COD_EDITORS_HOMEPAGE = '" + pair.Value.FILTER_EDITORS_HOMEPAGE + "'";
                       SQLquery += " where id_client = " + Global.InsigteManager[Session.SessionID].IdClient.ToString() + " and id_client_user = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser.ToString() + " and id_client_subject = " + pair.Key.ToString();
                DataTable userfilters = new DataTable();
                userfilters = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "userfilters");

                lbInfo.Text = "Alterações guardadas com sucesso!";

                
            }
            
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using AjaxControlToolkit;

namespace Insigte
{
    public partial class Fontes_gerais : BasePage
    {

        private DataTable Editors;

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!Global.InsigteManager[Session.SessionID].IsLogged || Global.InsigteManager[Session.SessionID] == null)
            {
                Response.Redirect("login.aspx", true);
            }

            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(6, 1) == "0")
            {
                Response.Redirect("Default.aspx");
            }

            getDataEditors();

            if (!Page.IsPostBack)
            {
                popEditors();
                getFontes();
            }

        }

        protected String getResource(String resx)
        {
            return Resources.insigte.language.ResourceManager.GetObject(resx).ToString();
        }

        protected void getFontes()
        {
            String SQLquery = "select * from CL10H_CLIENT_USERS with(nolock) where id_client = " + Global.InsigteManager[Session.SessionID].IdClient.ToString() + " and id_client_user = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser.ToString() + " ";
            DataTable filters = new DataTable();
            filters = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "filters");

            foreach (DataRow row in filters.Rows)
            {
                String[] ArrEditor = row["COD_EDITORS_FILTER"].ToString().Split(',');
                foreach (String x in ArrEditor)
                {
                    ta_editores_codes.Value += getEditorName(x) + ",";
                }
            }
        }

        protected void getDataEditors()
        {
            String SQLquery = "select * from dbo.editors with(nolock) order by name asc ";
            Editors = new DataTable();
            Editors = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "FilterEditors");
        }

        protected String getEditorID(String Editor)
        {
            if (Editor == Resources.insigte.language.geralTagTodos)
                return "*";
            else
            {
                foreach (DataRow row in Editors.Rows)
                {
                    if (row["name"].ToString() == Editor)
                        return row["editorid"].ToString();
                }
            }

            return null;
        }

        protected String getEditorName(String Editor)
        {
            if (Editor == "*")
                return Resources.insigte.language.geralTagTodos;
            else
            {
                foreach (DataRow row in Editors.Rows)
                {
                    if (row["editorid"].ToString() == Editor)
                        return row["name"].ToString();
                }
            }

            return null;
        }

        protected void popEditors()
        {
            LbEditors.Items.Add(Resources.insigte.language.geralTagTodos);

            foreach (DataRow x in Editors.Rows)
            {
                switch (x["country"].ToString())
                {
                    case "Portugal":
                        LbEditorsPT.Items.Add(x["name"].ToString());
                        break;
                    case "Angola":
                        LbEditorsAO.Items.Add(x["name"].ToString());
                        break;
                    case "Moçambique":
                        LbEditorsMZ.Items.Add(x["name"].ToString());
                        break;
                    default:
                        LbEditorsOU.Items.Add(x["name"].ToString());
                        break;
                }
                LbEditors.Items.Add(x["name"].ToString());
            }
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            String[] ArrEditor = ta_editores_codes.Value.Split(',');
            String EditorsId = "";

            foreach (String x in ArrEditor)
            {
                EditorsId += getEditorID(x) + ",";
            }
            EditorsId = EditorsId.TrimEnd(',');

            String SQLquery = "update CL10H_CLIENT_USERS set COD_EDITORS_FILTER = '" + EditorsId + "' where id_client = " + Global.InsigteManager[Session.SessionID].IdClient.ToString() + " and id_client_user = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser.ToString() + " ";
            DataTable filters = new DataTable();
            filters = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "filters");

            Global.InsigteManager[Session.SessionID].inUser.UserPortalFilter = EditorsId;

            LbInfo.Text = "Fontes Gerais alteradas com sucesso!";
        }


    }
}
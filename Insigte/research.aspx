﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="research.aspx.cs" Inherits="Insigte.research" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

<style type="text/css">
    #section.sector
    {
        float:left;
        border :1px solid Black;
        width: 100%;
        min-height: 300px;
        min-width:400px;
        color: #000;
        text-decoration: none;
        font-size: 12px;
        font-family:Arial;
        padding:0px;
        margin:0px;
    }
    .form_search
    {
        float:left;
        width: 100%;
        height:10px;
        padding-top: 4px;
        padding-bottom: 10px;
        margin: 0 auto 20px auto;
        background-color: #323232;
        color: #FFFFFF;
    }    
    
    .research
    {
        float:left;
        width: 100%;
    }
    
    .record
    {
        float:left;
        width: 100%;
        margin-bottom:5px;
    }
    
</style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div id="Geral" style="float:left; padding:0px; margin:0px; height:100%; width:800px;">
    <div id="infoText" style="float:left; padding:0px; margin: 2px 0px 2px 0px; height:35px; min-height:35px; width:100%"> 
        <div style="margin-top:8px">
            <asp:Label runat="server" ID="Label1" Font-Names="Arial" Font-Size="12px" Visible="true" Text="Relatório" />
            <asp:DropDownList ID="DDResearch" runat="server" AutoPostBack="True" onselectedindexchanged="DDResearch_SelectedIndexChanged"></asp:DropDownList>
            <asp:DropDownList ID="DDResearch_Tema" runat="server" AutoPostBack="True" onselectedindexchanged="DDResearch_Tema_SelectedIndexChanged"></asp:DropDownList>
        </div>
    </div>

    <div id="section" class="sector ui-corner-top" style="float:left;width:100%;">
        <div id="titulo" class="form_search headbg ui-corner-top" style="float:left;width:100%;">
            <div id="caixa" style="float:left;width:80%;">
                &nbsp;<asp:Label runat="server" ID="lblTitleAdvSearch" Font-Names="Arial" Font-Size="12px" ForeColor="#FFFFFF" Text=" Research" />
            </div>
            <div id="help" style="float:left;width:20%; ">
                <div style="float:right;padding-right:5px;">
                    <img alt="Ajuda" src="Imgs/icons/white/png/info_icon_16.png" />
                </div>
            </div>
        </div>

        <div class="research">
            <div style="margin:10px 10px 10px 10px">
                <asp:Repeater ID="rep_Research" runat="server">
                    <itemtemplate>
                        <div class="record">
                            <div style="padding:2px 2px 2px 2px; border:1px solid #323232;height:40px;">
                                <div style="float:left; width:70% ">
                                    <%# getUrl(Eval("filepath").ToString(), Eval("page_url").ToString(), Eval("tipo").ToString(), Eval("name").ToString()) %>
                                </div>
                                <div style="float:left; width:30%; text-align:right; ">
                                    <%# Eval("tipo").ToString() + " - " + Eval("name").ToString()%>
                                </div>
                                <div style="float:left; width:100% ">
                                    <%# Eval("txt_research_short").ToString() %>
                                </div>
                                <div><%# Eval("txt_nota").ToString()%></div>
                            </div>
                        </div>
                    </itemtemplate>
                </asp:Repeater>
            </div>
        </div>

    </div>


    <div style="float:right; padding-right:7px;">
        <asp:Table ID="Table2" runat="server">
            <asp:TableRow ID="TableRow2" runat="server">
                <asp:TableCell ID="TableCell9" runat="server">
                    <asp:Button runat="server" ID="btnBuildPDF" Text="<%$Resources:insigte.language,pastasCompilarPDF%>" CssClass="btn-pdfs" OnClick="btnBuildPDF_Click" />
                </asp:TableCell>
                <asp:TableCell ID="TableCell10" runat="server">
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </div>
</div>

</asp:Content>

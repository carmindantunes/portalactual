﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using AjaxControlToolkit;
using INInsigteManager;
using System.Net;
using System.Xml.Linq;

namespace Insigte
{
    public partial class cpnews : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Global.InsigteManager.ContainsKey(Session.SessionID) == false)
            {
                INManager IN = new INManager(Session.SessionID, GetVisitorIpAddress(), GetLanIPAddress());
                Global.InsigteManager.Add(Session.SessionID, IN);

            }

            if (!Global.InsigteManager[Session.SessionID].IsLogged || Global.InsigteManager[Session.SessionID] == null)
            {
                Response.Redirect("login.aspx", true);
            }

            //Concursos Publicos
            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(12, 1) == "0")
            {
                Response.Redirect("Default.aspx");
            }

            dgNewsACN.PagerStyle.BackColor = System.Drawing.Color.FromName(Global.InsigteManager[Session.SessionID].inUser.Cores[0].ToString());
            dgNewsACN.HeaderStyle.BackColor = System.Drawing.Color.FromName(Global.InsigteManager[Session.SessionID].inUser.Cores[1].ToString());

            string qsTema = Request.QueryString["tp"];

            if (qsTema == null)
                qsTema = "-1";

            if (!Page.IsPostBack)
            {
                fillDDTema("-1");
                getCP(qsTema, "-1");
            }
        }

        protected void fillDDTema(String Tema)
        {
            DataTable temas = Global.InsigteManager[Session.SessionID].getTableQuery(" select -1 as ID_TEMA, 'Todos' as DES_TEMA union all select ID_TEMA, DES_TEMA from NW10D_CP_TEMA with(nolock) ", "getCPTemas");

            ddTemas.Items.Clear();
            ddTemas.DataSource = temas;
            ddTemas.DataTextField = "DES_TEMA";
            ddTemas.DataValueField = "ID_TEMA";
            ddTemas.DataBind();

            ddTemas.SelectedValue = Tema;

            fillDDSubTema(Tema, "-1");

        }

        protected void fillDDSubTema(String Tema, String SubTema)
        {
            DataTable temas = Global.InsigteManager[Session.SessionID].getTableQuery(" select -1 as ID_SUBTEMA, 'Todos' as DES_SUBTEMA union all select ID_SUBTEMA, DES_SUBTEMA from NW10D_CP_SUBTEMA with(nolock) where ID_TEMA=" + Tema, "getCPTemas");

            if (temas.Rows.Count > 1)
            {
                ddSubTema.Items.Clear();
                ddSubTema.Visible = true;
                LbSubTema.Visible = true;
                ddSubTema.DataSource = temas;
                ddSubTema.DataTextField = "DES_SUBTEMA";
                ddSubTema.DataValueField = "ID_SUBTEMA";
                ddSubTema.DataBind();

                ddSubTema.SelectedValue = SubTema;
            }
            else
            {
                ddSubTema.Items.Clear();
                ddSubTema.Visible = false;
                LbSubTema.Visible = false;
            }

        }

        protected void dgNewsACN_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgNewsACN.CurrentPageIndex = e.NewPageIndex;
            string subtema = (ddSubTema.SelectedItem == null) ? "-1" : ddSubTema.SelectedItem.Value.ToString();
            getCP(ddTemas.SelectedItem.Value.ToString(), subtema);
        }

        protected void ddTemas_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillDDSubTema(ddTemas.SelectedItem.Value.ToString(), "-1");
            dgNewsACN.CurrentPageIndex = 0;
            getCP(ddTemas.SelectedItem.Value.ToString(), "-1");

        }

        protected void ddSubTemas_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgNewsACN.CurrentPageIndex = 0;
            getCP(ddTemas.SelectedItem.Value.ToString(), ddSubTema.SelectedItem.Value.ToString());
        }

        protected string sReturnIconTipoPasta(string url, string file)
        {
            String tipo = "";
            //Imprensa
            //Insigte
            //Online
            //Rádio
            //Televisão

            if (url.Length > 0)
                tipo = "Online";
            else
                tipo = "Imprensa";


            switch (tipo)
            {
                case "Imprensa":
                    String FileHelperPath = "ficheiros";
                    if (Global.InsigteManager[Session.SessionID].CostumPDFLogo == "1")
                    {
                        string auxfile = @"G:\cfiles\" + Global.InsigteManager[Session.SessionID].IdClient + "\\" + file.Replace("/", "\\");
                        bool urlexists = INinsigteManager.Utils.URLExists("http://insigte.com/cficheiros/" + Global.InsigteManager[Session.SessionID].IdClient + "\\" + file.ToString());

                        if (File.Exists(auxfile) || urlexists)
                        {
                            FileHelperPath = "cficheiros/" + Global.InsigteManager[Session.SessionID].IdClient;
                        }

                        //if (File.Exists( @"G:\cfiles\" + Global.InsigteManager[Session.SessionID].IdClient + "\\" + file.Replace("/","\\")))
                        //{
                        //    FileHelperPath = "cficheiros/" + Global.InsigteManager[Session.SessionID].IdClient;
                        //}
                    }
                    return string.Format("<a href=\"http://insigte.com/" + FileHelperPath + "/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/doc_export_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download PDF\" style=\"border-width:0px;\" title=\"Download PDF\" /></a>", file);
                    //return string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/doc_export_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download PDF\" style=\"border-width:0px;\" title=\"Download PDF\" /></a>", file);
                    break;
                case "Online":
                    return string.Format("<a href=\"{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/globe_1_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Link\" style=\"border-width:0px;\" title=\"Link\" /></a>", url);
                    break;
                case "Rádio":
                    return string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/headphones_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download MP3\" style=\"border-width:0px;\" title=\"Download MP3\" /></a>", file);
                    break;
                case "Televisão":
                    return string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/movie_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download MP4\" style=\"border-width:0px;\" title=\"Download MP4\" /></a>", file);
                    break;
                default:
                    return string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/doc_export_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download PDF\" style=\"border-width:0px;\" title=\"Download PDF\" /></a>", file);
                    break;
            }
            return null;
        }

        protected void getCP(String TemaId, String SubTemaId = "-1")
        {
            String SQLQUERY = "";
            //SQLQUERY += " select '&nbsp;' + DES_LOCAL +  as DES_LOCAL, DES_CONCURSO_PUBLICO, DAT_ANUNCIO, COD_FILEPATH, COD_URL, et.nom_entidade from NW10F_CONCURSOS_PUBLICOS cp inner join dim_entidade et on et.id_entidade = cp.ID_ENTIDADE ";
            SQLQUERY += " select DES_LOCAL + ', ' + c.DES_COUNTRY as DES_LOCAL, '&nbsp;' + DES_CONCURSO_PUBLICO as DES_CONCURSO_PUBLICO, DAT_ANUNCIO, COD_FILEPATH, COD_URL, et.nom_entidade, e.name as Editor ";
            SQLQUERY += " from NW10F_CONCURSOS_PUBLICOS cp with(nolock)  ";
            SQLQUERY += " inner join dim_entidade et with(nolock) on et.id_entidade = cp.ID_ENTIDADE ";
            SQLQUERY += " inner join BI10D_COUNTRY c with(nolock) on c.ID_COUNTRY = cp.ID_COUNTRY ";
            SQLQUERY += " inner join editors e with(nolock) on e.editorid = cp.ID_PUBLICACAO where 1=1 ";

            if (TemaId != "-1")
                SQLQUERY += " and ID_TEMA = " + TemaId;
            if (SubTemaId != "-1")
                SQLQUERY += " and ID_SUBTEMA = " + SubTemaId;

            SQLQUERY += " Order by DAT_ANUNCIO desc ";

            DataTable dados = Global.InsigteManager[Session.SessionID].getTableQuery(SQLQUERY, "getDadosCP");

            dgNewsACN.DataSource = null;
            dgNewsACN.DataSource = dados;
            dgNewsACN.DataBind();
            dgNewsACN.Visible = true;

        }

        protected void btnExportXML_Click(object sender, EventArgs e)
        {
        }

        public string GetVisitorIpAddress()
        {
            string stringIpAddress;
            stringIpAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (stringIpAddress == null) //may be the HTTP_X_FORWARDED_FOR is null
            {
                stringIpAddress = Request.ServerVariables["REMOTE_ADDR"];//we can use REMOTE_ADDR
            }
            return stringIpAddress;
        }
        //Get Lan Connected IP address method
        public string GetLanIPAddress()
        {
            //Get the Host Name
            string stringHostName = Dns.GetHostName();
            //Get The Ip Host Entry
            IPHostEntry ipHostEntries = Dns.GetHostEntry(stringHostName);
            //Get The Ip Address From The Ip Host Entry Address List
            IPAddress[] arrIpAddress = ipHostEntries.AddressList;
            return arrIpAddress[arrIpAddress.Length - 1].ToString();
        }

    }
}
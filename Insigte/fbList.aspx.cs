﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using AjaxControlToolkit;
using INInsigteManager;
using System.Net;
using System.Xml.Linq;

namespace Insigte
{
    public partial class fbList : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Global.InsigteManager.ContainsKey(Session.SessionID) == false)
            {
                INManager IN = new INManager(Session.SessionID, GetVisitorIpAddress(), GetLanIPAddress());
                Global.InsigteManager.Add(Session.SessionID, IN);

            }

            //Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("pt-PT");
            //Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("pt-PT");

            if (!Global.InsigteManager[Session.SessionID].IsLogged || Global.InsigteManager[Session.SessionID] == null)
            {
                Response.Redirect("login.aspx", true);
            }

            

            dgNewsACN.PagerStyle.BackColor = System.Drawing.Color.FromName(Global.InsigteManager[Session.SessionID].inUser.Cores[0].ToString());
            dgNewsACN.HeaderStyle.BackColor = System.Drawing.Color.FromName(Global.InsigteManager[Session.SessionID].inUser.Cores[1].ToString());

            dgNewsACN.Columns[3].Visible = chkifinsigte();

            if (!Page.IsPostBack)
            {
                getPubCountry();
                getPublicacoes(0);
            }
            //else
            //{
            //    getPublicacoes(1);
            //}

            if (Global.InsigteManager[Session.SessionID].inUser.HasPastas.ToString() == "1")
            {
                btnDossier.Visible = true;
                btnSelAll.Visible = true;

            }
            else
            {
                btnDossier.Visible = false;
                btnSelAll.Visible = false;
            }

        }

        protected string sgetfbImg(string strValue)
        {
            string rtn = "";
            try
            {
                DataTable find = Global.InsigteManager[Session.SessionID].getTableQuery("SELECT sum(fb_coments_val) as comments FROM [iadvisers].[dbo].[FB10F_NEWS_POST] with(nolock) where  id_noticia = " + strValue, "getFaceNews");

                foreach (DataRow row in find.Rows)
                {
                    if ((int)row["comments"] == 0)
                        rtn = "Imgs/icons/black/png/facebook_icon_16.png";
                    else if ((int)row["comments"] == 1)
                        rtn = "Imgs/icons/black/png/facebook_icon_16_1c.png";
                    else if ((int)row["comments"] == 2)
                        rtn = "Imgs/icons/black/png/facebook_icon_16_2c.png";
                    else if ((int)row["comments"] == 3)
                        rtn = "Imgs/icons/black/png/facebook_icon_16_3c.png";
                    else if ((int)row["comments"] > 3)
                        rtn = "Imgs/icons/black/png/facebook_icon_16_3morec.png";
                }
            }
            catch (Exception)
            {
                rtn = "Imgs/icons/black/png/facebook_icon_16.png"; 
            }

            return rtn;
        }

        protected void getPubCountry()
        {
            string query = "select distinct country  from editors with(nolock)";
            DataTable dtCountry = Global.InsigteManager[Session.SessionID].getTableQuery(query, "getcountNewsPorMkt");

            if (dtCountry.Rows.Count > 0)
            {
                DpCountry.Items.Clear();
                DpCountry.Items.Add("Todos");
                foreach (DataRow row in dtCountry.Rows)
                {
                    DpCountry.Items.Add(row["country"].ToString());

                }
                //DpCountry.DataSource = dtCountry;
            }

        }

        //Facebook
        protected bool sReturnFace(string strValue)
        {
            //FaceBook
            DataTable find = Global.InsigteManager[Session.SessionID].getTableQuery("SELECT * FROM [iadvisers].[dbo].[FB10F_NEWS_POST] with(nolock) where  id_noticia = " + strValue, "getFaceNews");

            if (find.Rows.Count > 0)
                return true;

            return false;
        }

        protected Boolean chkIsVisable()
        {
            if (Global.InsigteManager[Session.SessionID].inUser.HasPastas.ToString() == "1")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected Boolean chkifinsigte()
        {
            if (Global.InsigteManager[Session.SessionID].IdClient == "1000" || Global.InsigteManager[Session.SessionID].inUser.IdClientUser == "178")
                return true;
            else
                return false;
        }

        protected Boolean pdfIsVisable()
        {
            if (Global.InsigteManager[Session.SessionID].inUser.HasPastas.ToString() == "1")
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        protected Boolean FbIsVisable()
        {
            if (Global.InsigteManager[Session.SessionID].inUser.HasAlerts.ToString() == "0")
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        protected Boolean checkDossier(String idArtigo)
        {
            if (HttpContext.Current.Request.Cookies["CookieinsigteInfo"] != null && idArtigo != string.Empty)
            {
                if (HttpContext.Current.Request.Cookies["CookieinsigteSys"]["PDF"] != null)
                {
                    HttpCookie cookie = Request.Cookies["CookieinsigteSys"];
                    string idVal = cookie.Values["PDF"];

                    if (idVal.Contains('|'))
                    {
                        string[] words = idVal.Split('|');
                        if (words.Contains(idArtigo))
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        if (idVal == idArtigo)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }


        }

        protected void btnSelAll_Click(object sender, EventArgs e)
        {
            foreach (DataGridItem drv in dgNewsACN.Items)
            {
                CheckBox chk = (CheckBox)drv.FindControl("chka_AddPDF");
                chk.Checked = true;
            }
        }

        protected void btnDossier_Click(object sender, EventArgs e)
        {
            lblDataToday.Visible = true;

            string ValCookie = string.Empty;
            string PDFCookie = string.Empty;
            string DelCookie = string.Empty;

            HttpCookie cookie = Request.Cookies["CookieinsigteSys"];
            String Artigos = "";
            foreach (DataGridItem drv in dgNewsACN.Items)
            {
                CheckBox chk = (CheckBox)drv.FindControl("chka_AddPDF");
                if (chk.Checked)
                {
                    Artigos += chk.Attributes["NewsId"].ToString() + "|";
                }
            }

            Artigos = Artigos.TrimEnd('|');

            if (cookie.Values["PDF"] != "")
            {
                PDFCookie = cookie.Values["PDF"] + "|" + Artigos;
            }
            else
            {
                PDFCookie = Artigos;
            }

            DelCookie = cookie.Values["DELNOT"];
            ValCookie = cookie.Values["ID"];

            cookie.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(cookie);

            HttpCookie newCookie = new HttpCookie("CookieinsigteSys");
            newCookie["Version"] = "JUL2012";
            newCookie["ID"] = ValCookie;
            newCookie["PDF"] = PDFCookie;
            newCookie["DELNOT"] = DelCookie;
            newCookie.Expires = DateTime.Now.AddDays(360);
            Response.Cookies.Add(newCookie);

            //drv.Cells[0].Text;

            lblDataToday.Text += " Artigos adicionados com sucesso!";

        }

        protected String getResource(String resx)
        {
            return Resources.insigte.language.ResourceManager.GetObject(resx).ToString();
        }

        protected string sReturnIdComposed(string strValue)
        {
            string sLink = string.Empty;

            DataTable find = Global.InsigteManager[Session.SessionID].getTableQuery("select * from ML10F_CLIENT_USER_CONTROL_COMPOSED_ADD with(nolock) where convert(varchar,DAT_CREATE,112)=convert(varchar,getdate(),112) ", "findIdComposta");

            Boolean foundit = false;
            if (find.Rows.Count > 0)
            {
                foreach (DataRow row in find.Rows)
                {
                    if (row["ID_NEWS"].ToString() == strValue)
                        foundit = true;
                }
            }

            if (!foundit)
                sLink = strValue;

            return sLink;
        }

        protected string sReturnImgComposed(string strValue)
        {
            string sLink = "Imgs/icons/black/png/push_pin_icon_16_sell.png";

            DataTable find = Global.InsigteManager[Session.SessionID].getTableQuery("select * from ML10F_CLIENT_USER_CONTROL_COMPOSED_ADD with(nolock) where convert(varchar,DAT_CREATE,112)=convert(varchar,getdate(),112) ", "findIdComposta");

            Boolean foundit = false;
            if (find.Rows.Count > 0)
            {
                foreach (DataRow row in find.Rows)
                {
                    if (row["ID_NEWS"].ToString() == strValue)
                        foundit = true;
                }
            }

            if (foundit)
                sLink = "Imgs/icons/black/png/push_pin_icon_16.png";

            return sLink;
        }

        protected bool sReturnVisComposed()
        {
            //Newsletter Composta
            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(7, 1) == "0")
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        protected void bt_AddComposta_Command(Object sender, CommandEventArgs e)
        {
            if (e.CommandArgument.ToString().Length > 1)
            {
                addLinkComposta(e.CommandArgument.ToString());
                lblDataToday.Visible = true;
                //lblDataToday.Text = e.CommandArgument.ToString() + " : Adicionado com sucesso!";
                lblDataToday.Text = "Adicionado ao email com sucesso!";

                ImageButton b = sender as ImageButton;
                b.ImageUrl = "Imgs/icons/black/png/push_pin_icon_16.png";
            }
            else
            {
                lblDataToday.Visible = true;
                //lblDataToday.Text = e.CommandArgument.ToString() + " : Adicionado com sucesso!";
                lblDataToday.Text = "Já se encontra adicionada email!";
            }
        }

        protected void addLinkComposta(string qsPDFArtigo)
        {
            String Query = "insert into ML10F_CLIENT_USER_CONTROL_COMPOSED_ADD ";
            Query += " (ID_CLIENT, COD_NEWSLETTER_COMPOSE, ID_NEWS, DAT_CREATE) ";
            Query += " Select " + Global.InsigteManager[Session.SessionID].IdClient.ToString() + ", 0, " + qsPDFArtigo + ", getdate() ";
            DataTable insertComp = Global.InsigteManager[Session.SessionID].getTableQuery(Query, "InsertComp");
        }

        private void getPublicacoes(int c)
        {

            String Title = "m.TITLE";
            String Preview = "c.text";
            if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
            {
                Title = "m.TITLE_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage + " as TITLE";
                Preview = "c.text_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage;
            }

            String DataToShow = "m.DATE";
            String DataToShowConfig = "m.DATE";

            if (Global.InsigteManager[Session.SessionID].IdClient == "1000" || Global.InsigteManager[Session.SessionID].inUser.IdClientUser == "178")
            {
                DataToShow = "m.insert_date as DATE";
                DataToShowConfig = "m.insert_date";
            }

            String SQLTemas = "SELECT top 40 m.ID as IDMETA, " + Title + ", m.EDITOR, " + DataToShow + ", m.filepath, m.page_url, replace(substring(" + Preview + ",1,500),'\"','') + '...' as text, m.tipo ";
            SQLTemas += " FROM iadvisers.dbo.metadata m with(nolock) ";
            SQLTemas += " inner join contents c with(nolock) ";
            SQLTemas += " on m.id = c.id ";

            if(c.Equals(1))
                SQLTemas += " inner join editors e on m.editor = e.name";


            SQLTemas += " WHERE m.id in (select distinct id_noticia from [FB10F_NEWS_POST] with(nolock)) and ( ";

            try
            {

                foreach (var tema in Global.InsigteManager[Session.SessionID].inUser.Temas)
                {
                    //SQLTemas += " and ( ";
                    String filtroEditors = (tema.Value.FILTER_EDITORS_HOMEPAGE == "*" ? Global.InsigteManager[Session.SessionID].inUser.UserPortalFilter : tema.Value.FILTER_EDITORS_HOMEPAGE);

                    if (tema.Value.IdTema.Substring(4, 3).ToString() != "000" && tema.Value.SubTemas.Length == 0)
                    {
                        if (filtroEditors != "*")
                            SQLTemas += " m.id in ( select aw.id from clientarticles aw with(nolock) inner join metadata mw with(nolock) on mw.id=aw.id where aw.clientid='" + tema.Value.IdTema.Substring(0, 4).ToString() + "' and aw.subjectid = '" + tema.Value.IdTema.Substring(4, 3).ToString() + "' and mw.editorId in ( " + filtroEditors + " ) ) ";
                        else
                            SQLTemas += " m.id in ( select id from clientarticles with(nolock) where clientid='" + tema.Value.IdTema.Substring(0, 4).ToString() + "' and subjectid = '" + tema.Value.IdTema.Substring(4, 3).ToString() + "' ) ";
                    }
                    if (tema.Value.IdTema.Substring(4, 3).ToString() == "000" && tema.Value.IdTema == tema.Value.SubTemas)
                    {
                        if (filtroEditors != "*")
                            SQLTemas += " m.id in ( select aw.id from clientarticles aw with(nolock) inner join metadata mw with(nolock) on mw.id=aw.id where aw.clientid='" + tema.Value.IdTema.Substring(0, 4).ToString() + "' and mw.editorId in ( " + filtroEditors + " ) ) ";
                        else
                            SQLTemas += " m.id in ( select id from clientarticles with(nolock) where clientid='" + tema.Value.IdTema.Substring(0, 4).ToString() + "' ) ";
                    }
                    if (tema.Value.IdTema.Substring(4, 3).ToString() == "000" && tema.Value.IdTema != tema.Value.SubTemas && tema.Value.IdTema.Length <= tema.Value.SubTemas.Length)
                    {
                        String[] subtemas = tema.Value.SubTemas.Split(';');
                        String AuxTemas = "";
                        foreach (String y in subtemas)
                        {
                            AuxTemas += "'" + y.Substring(4, 3) + "',";
                        }
                        AuxTemas = AuxTemas.TrimEnd(',');

                        if (filtroEditors != "*")
                            SQLTemas += " m.id in ( select aw.id from clientarticles aw with(nolock) inner join metadata mw with(nolock) on mw.id=aw.id where aw.clientid='" + tema.Value.IdTema.Substring(0, 4).ToString() + "' and aw.subjectid in (" + AuxTemas + ") and mw.editorId in ( " + filtroEditors + " ) ) ";
                        else
                            SQLTemas += " m.id in ( select id from clientarticles with(nolock) where clientid='" + tema.Value.IdTema.Substring(0, 4).ToString() + "' and subjectid in (" + AuxTemas + ") ) ";

                    }
                    SQLTemas += "  OR ";
                }
                SQLTemas = SQLTemas.Substring(0, (SQLTemas.Length - 4)) + " ) ";
            }
            catch (Exception e)
            {
                string omfg = e.Message.ToString();
            }
            //// FILTRO DAS FONTES GERAIS
            //if (Global.InsigteManager[Session.SessionID].inUser.UserPortalFilter != "*")
            //    SQLTemas += " and m.editorId in ( " + Global.InsigteManager[Session.SessionID].inUser.UserPortalFilter + " ) ";

            if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
            {
                SQLTemas += " and m.TITLE_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage + " is not null ";
            }
           

            if(c.Equals(1))//get publicacoes com o editor da dropdownlist
            {
                SQLTemas += " and e.country = '" + DpCountry.SelectedValue.ToString() + "'";
            }
            SQLTemas += " order by " + DataToShowConfig + " desc, IDMETA desc";

            //lblDataToday.Visible = true;
            //lblDataToday.Text = SQLTemas;

            SqlConnection conn = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");

            try
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand(SQLTemas);
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;

                DataSet tempSet;
                SqlDataAdapter iadvisers_ACN_NEWS;

                iadvisers_ACN_NEWS = new SqlDataAdapter(cmd);

                tempSet = new DataSet("tempSet");
                iadvisers_ACN_NEWS.Fill(tempSet);

                dgNewsACN.DataSource = tempSet;
                dgNewsACN.DataBind();
                dgNewsACN.Visible = true;

            }
            catch (Exception exp)
            {
                //lblDataToday.Visible = true;
                //lblDataToday.Text = SQLTemas;
            }
        }

        protected void dgNewsACN_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgNewsACN.CurrentPageIndex = e.NewPageIndex;
            //if(DpCountry.SelectedIndex != 0)
            //    getPublicacoes(1);
            //else
            //getPublicacoes(0);
        }

        protected string sReturnImgLinkDetalhe(string strValue)
        {
            string sLink = string.Empty;

            if (HttpContext.Current.Request.Cookies["CookieinsigteInfo"] != null && strValue != string.Empty)
            {
                if (HttpContext.Current.Request.Cookies["CookieinsigteSys"]["ID"] != null)
                {
                    HttpCookie cookie = Request.Cookies["CookieinsigteSys"];
                    string idVal = cookie.Values["ID"];

                    if (idVal.Contains('|'))
                    {
                        string[] words = idVal.Split('|');
                        if (words.Contains(strValue))
                        {
                            sLink = "Imgs/icons/black/png/clipboard_copy_icon_16_sel.png";
                        }
                        else
                        {
                            sLink = "Imgs/icons/black/png/clipboard_copy_icon_16.png";
                        }
                    }
                    else
                    {
                        if (idVal == strValue)
                        {
                            sLink = "Imgs/icons/black/png/clipboard_copy_icon_16_sel.png";
                        }
                        else
                        {
                            sLink = "Imgs/icons/black/png/clipboard_copy_icon_16.png";
                        }
                    }
                }
            }
            else
            {
                sLink = "Imgs/icons/black/png/clipboard_copy_icon_16.png";
            }
            return sLink;
        }

        protected string sReturnIdLinkDetalhe(string strValue)
        {
            string sLink = string.Empty;

            if (HttpContext.Current.Request.Cookies["CookieinsigteInfo"] != null && strValue != string.Empty)
            {
                if (HttpContext.Current.Request.Cookies["CookieinsigteSys"]["ID"] != null)
                {
                    HttpCookie cookie = Request.Cookies["CookieinsigteSys"];
                    string idVal = cookie.Values["ID"];

                    if (idVal.Contains('|'))
                    {
                        string[] words = idVal.Split('|');
                        if (words.Contains(strValue))
                        {
                            sLink = string.Empty;
                        }
                        else
                        {
                            sLink = strValue;
                        }
                    }
                    else
                    {
                        if (idVal == strValue)
                        {
                            sLink = string.Empty;
                        }
                        else
                        {
                            sLink = strValue;
                        }
                    }
                }
            }
            else
            {
                sLink = strValue;
            }
            return sLink;
        }

        protected string sReturnImgPDF(string strValue)
        {
            string sLink = string.Empty;

            if (HttpContext.Current.Request.Cookies["CookieinsigteInfo"] != null && strValue != string.Empty)
            {
                if (HttpContext.Current.Request.Cookies["CookieinsigteSys"]["PDF"] != null)
                {
                    HttpCookie cookie = Request.Cookies["CookieinsigteSys"];
                    string idVal = cookie.Values["PDF"];

                    if (idVal.Contains('|'))
                    {
                        string[] words = idVal.Split('|');
                        if (words.Contains(strValue))
                        {
                            sLink = "Imgs/icons/black/png/book_side_icon_16_sel.png";
                        }
                        else
                        {
                            sLink = "Imgs/icons/black/png/book_side_icon_16.png";
                        }
                    }
                    else
                    {
                        if (idVal == strValue)
                        {
                            sLink = "Imgs/icons/black/png/book_side_icon_16_sel.png";
                        }
                        else
                        {
                            sLink = "Imgs/icons/black/png/book_side_icon_16.png";
                        }
                    }
                }
            }
            else
            {
                sLink = "Imgs/icons/black/png/book_side_icon_16.png";
            }
            return sLink;
        }

        protected string sReturnIdPDF(string strValue)
        {
            string sLink = string.Empty;

            if (HttpContext.Current.Request.Cookies["CookieinsigteInfo"] != null && strValue != string.Empty)
            {
                if (HttpContext.Current.Request.Cookies["CookieinsigteSys"]["PDF"] != null)
                {
                    HttpCookie cookie = Request.Cookies["CookieinsigteSys"];
                    string idVal = cookie.Values["PDF"];

                    if (idVal.Contains('|'))
                    {
                        string[] words = idVal.Split('|');
                        if (words.Contains(strValue))
                        {
                            sLink = string.Empty;
                        }
                        else
                        {
                            sLink = strValue;
                        }
                    }
                    else
                    {
                        if (idVal == strValue)
                        {
                            sLink = string.Empty;
                        }
                        else
                        {
                            sLink = strValue;
                        }
                    }
                }
            }
            else
            {
                sLink = strValue;
            }
            return sLink;
        }

        protected void addLinkPDF(string qsPDFArtigo)
        {
            try
            {
                string ValCookie = string.Empty;
                string PDFCookie = string.Empty;
                string DelCookie = string.Empty;

                HttpCookie cookie = Request.Cookies["CookieinsigteSys"];

                if (cookie.Values["PDF"] != "")
                {
                    PDFCookie = cookie.Values["PDF"] + "|" + qsPDFArtigo;
                }
                else
                {
                    PDFCookie = qsPDFArtigo;
                }

                DelCookie = cookie.Values["DELNOT"];
                ValCookie = cookie.Values["ID"];

                cookie.Expires = DateTime.Now.AddDays(-1d);
                Response.Cookies.Add(cookie);

                HttpCookie newCookie = new HttpCookie("CookieinsigteSys");
                newCookie["Version"] = "JUL2012";
                newCookie["ID"] = ValCookie;
                newCookie["PDF"] = PDFCookie;
                newCookie["DELNOT"] = DelCookie;
                newCookie.Expires = DateTime.Now.AddDays(360);
                Response.Cookies.Add(newCookie);

            }
            catch (Exception x)
            {
                //lblDataToday.Text += "<br>Erro: " + x.Message.ToString(); 
            }
            //lblArtigoAddPDF.Text = "O artigo de ID: " + qsPDFArtigo + ", foi adicionado com sucesso.";

        }

        protected void addNews(string qsIDArtigo)
        {
            string ValCookie = string.Empty;
            string PDFCookie = string.Empty;
            string DelCookie = string.Empty;

            HttpCookie cookie = Request.Cookies["CookieinsigteSys"];

            if (cookie.Values["ID"] != "")
            {
                ValCookie = cookie.Values["ID"] + "|" + qsIDArtigo;
            }
            else
            {
                ValCookie = qsIDArtigo;
            }

            DelCookie = cookie.Values["DELNOT"];
            PDFCookie = cookie.Values["PDF"];

            cookie.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(cookie);

            HttpCookie newCookie = new HttpCookie("CookieinsigteSys");
            newCookie["Version"] = "JUL2012";
            newCookie["ID"] = ValCookie;
            newCookie["PDF"] = PDFCookie;
            newCookie["DELNOT"] = DelCookie;
            newCookie.Expires = DateTime.Now.AddDays(360);
            Response.Cookies.Add(newCookie);

            //lblArtigoAdd.Text = "O artigo de ID: " + qsIDArtigo + ", foi adicionado com sucesso.";
        }

        protected void bt_AddPDF_Command(Object sender, CommandEventArgs e)
        {
            addLinkPDF(e.CommandArgument.ToString());
            lblDataToday.Text = e.CommandArgument.ToString() + " : Adicionado com sucesso!";

            ImageButton b = sender as ImageButton;
            b.ImageUrl = "Imgs/icons/black/png/book_side_icon_16_sel.png";
        }

        protected void bt_AddArquivo_Command(Object sender, CommandEventArgs e)
        {
            addNews(e.CommandArgument.ToString());
            lblDataToday.Text = e.CommandArgument.ToString() + " : Adicionado com sucesso!";

            ImageButton b = sender as ImageButton;
            b.ImageUrl = "Imgs/icons/black/png/clipboard_copy_icon_16_sel.png";
        }

        protected string sReturnLinkPasta(string strValue)
        {
            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(0, 1) == "0")
            {
                return null;
            }

            string sLink = string.Empty;
            string idVal = string.Empty;

            string cmdQueryPastas = "select * from dbo.CL10H_PASTAS_ARTICLES with(nolock) where id_client = " + Global.InsigteManager[Session.SessionID].IdClient + " and id_client_user = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + " and articleid=" + strValue;
            SqlConnection conn = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");
            try
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand(cmdQueryPastas);
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;

                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows == true)
                {
                    while (dr.Read())
                        idVal += dr["articleid"].ToString() + "|";
                }

                idVal.Substring(0, idVal.Length - 1);

                dr.Close();

            }
            catch (Exception exp)
            {
                exp.Message.ToString();
            }
            finally
            {
                conn.Dispose();
                conn.Close();
            }


            if (idVal.Contains('|'))
            {
                string[] words = idVal.Split('|');
                if (words.Contains(strValue))
                {
                    sLink = "<a target=" + "\"" + "_self" + "\"" + " href=" + "\"" + "javascript:window.open('Adicionar.aspx?ID=" + strValue + "','CustomPopUp', 'width=400, height=200, menubar=no, resizeble=no, location=no'); void(0)" + "\"" + " style='border-width:0px;text-decoration:none;' >" + "<img src='Imgs/icons/black/png/folder_open_icon_16_sel.png' width='16px' height='16px' alt='" + Resources.insigte.language.defColAccoesPastas + "' title='" + Resources.insigte.language.defColAccoesPastas + "' style='border-width:0px;'/></a>";
                }
                else
                {
                    sLink = "<a target=" + "\"" + "_self" + "\"" + " href=" + "\"" + "javascript:window.open('Adicionar.aspx?ID=" + strValue + "','CustomPopUp', 'width=400, height=200, menubar=no, resizeble=no, location=no'); void(0)" + "\"" + " style='border-width:0px;text-decoration:none;' >" + "<img src='Imgs/icons/black/png/folder_open_icon_16.png' width='16px' height='16px' alt='" + Resources.insigte.language.defColAccoesPastas + "' title='" + Resources.insigte.language.defColAccoesPastas + "' style='border-width:0px;'/></a>";
                }
            }
            else
            {
                if (idVal == strValue)
                {
                    sLink = "<a target=" + "\"" + "_self" + "\"" + " href=" + "\"" + "javascript:window.open('Adicionar.aspx?ID=" + strValue + "','CustomPopUp', 'width=400, height=200, menubar=no, resizeble=no, location=no'); void(0)" + "\"" + " style='border-width:0px;text-decoration:none;' >" + "<img src='Imgs/icons/black/png/folder_open_icon_16_sel.png' width='16px' height='16px' alt='" + Resources.insigte.language.defColAccoesPastas + "' title='" + Resources.insigte.language.defColAccoesPastas + "' style='border-width:0px;'/></a>";
                }
                else
                {
                    sLink = "<a target=" + "\"" + "_self" + "\"" + " href=" + "\"" + "javascript:window.open('Adicionar.aspx?ID=" + strValue + "','CustomPopUp', 'width=400, height=200, menubar=no, resizeble=no, location=no'); void(0)" + "\"" + " style='border-width:0px;text-decoration:none;' >" + "<img src='Imgs/icons/black/png/folder_open_icon_16.png' width='16px' height='16px' alt='" + Resources.insigte.language.defColAccoesPastas + "' title='" + Resources.insigte.language.defColAccoesPastas + "' style='border-width:0px;'/></a>";
                }
            }

            return sLink;
        }

        protected string sReturnTitle(string idArticle, string sTitle)
        {
            string sLink = string.Empty;

            SqlConnection conn = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");
            try
            {
                conn.Open();

                string cmdQuery = "select clientid, subjectid from clientarticles with(nolock) where id='" + idArticle + "'";

                SqlCommand cmd = new SqlCommand(cmdQuery);
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;

                SqlDataReader reader = cmd.ExecuteReader();

                bool isACNArticle = false;

                while (reader.Read())
                {
                    if (reader.GetValue(0).ToString() == Global.InsigteManager[Session.SessionID].TemaCliente.Substring(0, 4) && reader.GetValue(1).ToString() == Global.InsigteManager[Session.SessionID].TemaCliente.Substring(4, 3))
                    {
                        isACNArticle = true;
                    }
                }

                if (isACNArticle)
                {
                    sLink = "<b>" + sTitle + "</b>";
                }
                else
                {
                    sLink = sTitle;
                }

                cmd.Connection.Close();
                cmd.Connection.Dispose();

                return sLink;
            }
            catch (Exception exp)
            {
                exp.Message.ToString();
                return sTitle;
            }
        }

        private DataTable GetData(SqlCommand cmd)
        {
            DataTable dt = new DataTable();
            SqlConnection con = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");
            SqlDataAdapter sda = new SqlDataAdapter();
            cmd.CommandType = CommandType.Text;
            cmd.Connection = con;

            try
            {
                con.Open();
                sda.SelectCommand = cmd;
                sda.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                sda.Dispose();
                con.Dispose();
            }

        }

        protected void btnExportXML_Click(object sender, EventArgs e)
        {
            //Get the data from database into datatable
            String SQLTemas = "SELECT top 40 distinct m.ID as IDMETA, m.TITLE, m.EDITOR, m.DATE, m.filepath, m.page_url";
            SQLTemas += " FROM iadvisers.dbo.metadata m with(nolock) ";
            SQLTemas += " INNER JOIN iadvisers.dbo.clientarticles a with(nolock) ";
            SQLTemas += " ON a.id = m.ID WHERE ";

            foreach (var tema in Global.InsigteManager[Session.SessionID].inUser.Temas)
            {
                if (tema.Value.IdTema.Substring(4, 3).ToString() != "000" && tema.Value.SubTemas.Length == 0)
                {
                    //SQLTemas += " a.clientid='" + tema.Value.IdTema.Substring(0, 4).ToString() + "' and a.subjectid = '" + tema.Value.IdTema.Substring(4, 3).ToString() + "' and m.date >= getdate()-" + tema.Value.BackDays.ToString();
                    SQLTemas += " a.clientid='" + tema.Value.IdTema.Substring(0, 4).ToString() + "' and a.subjectid = '" + tema.Value.IdTema.Substring(4, 3).ToString() + "' ";
                }
                if (tema.Value.IdTema.Substring(4, 3).ToString() == "000" && tema.Value.IdTema == tema.Value.SubTemas)
                {
                    //SQLTemas += " a.clientid='" + tema.Value.IdTema.Substring(0, 4).ToString() + "' and m.date >= getdate()-" + tema.Value.BackDays.ToString();
                    SQLTemas += " a.clientid='" + tema.Value.IdTema.Substring(0, 4).ToString() + "' ";
                }
                if (tema.Value.IdTema.Substring(4, 3).ToString() == "000" && tema.Value.IdTema != tema.Value.SubTemas && tema.Value.IdTema.Length <= tema.Value.SubTemas.Length)
                {
                    String[] subtemas = tema.Value.SubTemas.Split(';');
                    String AuxTemas = "";
                    foreach (String y in subtemas)
                    {
                        AuxTemas += "'" + y.Substring(4, 3) + "',";
                    }
                    AuxTemas = AuxTemas.TrimEnd(',');
                    //SQLTemas += " a.clientid='" + tema.Value.IdTema.Substring(0, 4).ToString() + "' and a.subjectid in (" + AuxTemas + ") and m.date >= getdate()-" + tema.Value.BackDays.ToString();
                    SQLTemas += " a.clientid='" + tema.Value.IdTema.Substring(0, 4).ToString() + "' and a.subjectid in (" + AuxTemas + ") ";
                }
                SQLTemas += " OR ";
            }
            SQLTemas = SQLTemas.Substring(0, (SQLTemas.Length - 4));
            SQLTemas += " order by m.date desc, IDMETA desc";

            SqlCommand cmd = new SqlCommand(SQLTemas);
            DataTable dt = GetData(cmd);

            //Create a dummy GridView
            GridView GridView1 = new GridView();
            GridView1.AllowPaging = false;
            GridView1.DataSource = dt;
            GridView1.DataBind();

            string timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=Geral_" + timestamp + ".xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";

            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                //Apply text style to each Row
                GridView1.Rows[i].Attributes.Add("class", "textmode");
            }
            GridView1.RenderControl(hw);
            //style to format numbers to string

            string style = @"<style> .textmode { mso-number-format:\@; } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }

        protected string sReturnIconTipoPasta(string url, string tipo, string file)
        {
            //Imprensa
            //Insigte
            //Online
            //Rádio
            //Televisão
            switch (tipo)
            {
                case "Imprensa":
                    String FileHelperPath = "ficheiros";
                    if (Global.InsigteManager[Session.SessionID].CostumPDFLogo == "1")
                    {
                        string auxfile = @"G:\cfiles\" + Global.InsigteManager[Session.SessionID].IdClient + "\\" + file.Replace("/", "\\");
                        bool urlexists = INinsigteManager.Utils.URLExists("http://insigte.com/cficheiros/" + Global.InsigteManager[Session.SessionID].IdClient + "\\" + file.ToString());

                        if (File.Exists(auxfile) || urlexists)
                        {
                            FileHelperPath = "cficheiros/" + Global.InsigteManager[Session.SessionID].IdClient;
                        }

                        //if (File.Exists( @"G:\cfiles\" + Global.InsigteManager[Session.SessionID].IdClient + "\\" + file.Replace("/","\\")))
                        //{
                        //    FileHelperPath = "cficheiros/" + Global.InsigteManager[Session.SessionID].IdClient;
                        //}
                    }
                    return string.Format("<a href=\"http://insigte.com/" + FileHelperPath + "/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/doc_export_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download PDF\" style=\"border-width:0px;\" title=\"Download PDF\" /></a>", file);
                    //return string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/doc_export_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download PDF\" style=\"border-width:0px;\" title=\"Download PDF\" /></a>", file);
                    break;
                case "Online":
                    return string.Format("<a href=\"{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/globe_1_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Link\" style=\"border-width:0px;\" title=\"Link\" /></a>", url);
                    break;
                case "Rádio":
                    return string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/headphones_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download MP3\" style=\"border-width:0px;\" title=\"Download MP3\" /></a>", file);
                    break;
                case "Televisão":
                    return string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/movie_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download MP4\" style=\"border-width:0px;\" title=\"Download MP4\" /></a>", file);
                    break;
                default:
                    return string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/doc_export_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download PDF\" style=\"border-width:0px;\" title=\"Download PDF\" /></a>", file);
                    break;
            }
            return null;
        }

        public string GetVisitorIpAddress()
        {
            string stringIpAddress;
            stringIpAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (stringIpAddress == null) //may be the HTTP_X_FORWARDED_FOR is null
            {
                stringIpAddress = Request.ServerVariables["REMOTE_ADDR"];//we can use REMOTE_ADDR
            }
            return stringIpAddress;
        }

        //Get Lan Connected IP address method
        public string GetLanIPAddress()
        {
            //Get the Host Name
            string stringHostName = Dns.GetHostName();
            //Get The Ip Host Entry
            IPHostEntry ipHostEntries = Dns.GetHostEntry(stringHostName);
            //Get The Ip Address From The Ip Host Entry Address List
            IPAddress[] arrIpAddress = ipHostEntries.AddressList;
            return arrIpAddress[arrIpAddress.Length - 1].ToString();
        }

        protected void DpCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(!DpCountry.SelectedIndex.Equals(0))
              getPublicacoes(1);
            else
                getPublicacoes(0);
        }

    }
}
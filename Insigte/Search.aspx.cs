﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using AjaxControlToolkit;


namespace Insigte
{
    public partial class Search : BasePage
    {

        private DataTable Editors;
        private DataTable Autores;

        protected void Page_Load(object sender, EventArgs e)
        {
            dgSearchACN.PagerStyle.BackColor = System.Drawing.Color.FromName(Global.InsigteManager[Session.SessionID].inUser.Cores[0].ToString());
            dgSearchACN.HeaderStyle.BackColor = System.Drawing.Color.FromName(Global.InsigteManager[Session.SessionID].inUser.Cores[1].ToString());
            lblDataToday.Text = "";

            if (Global.InsigteManager[Session.SessionID].inUser.EmailShareSend != null)
            {
                if (Global.InsigteManager[Session.SessionID].inUser.EmailShareSend == "1")
                {
                    lblDataToday.Visible = true;
                    lblDataToday.Text = "Email enviado.";
                    Global.InsigteManager[Session.SessionID].inUser.EmailShareSend = null;
                }
                else
                {
                    lblDataToday.Visible = true;
                    lblDataToday.Text = "Email não enviado.";

                    Global.InsigteManager[Session.SessionID].inUser.EmailShareSend = null;
                }
            }

            if (!Page.IsPostBack)
            {
                getDataEditors();
                getDataAutor();

                if (Session["ADVSearchBool"] != null && Session["ADVSearchBool"].ToString() == "true")
                {
                    advSearchGrid();
                }

            }                
            else //solucao mais rapida ao problema advSearch 
            {
                getDataEditors();
                getDataAutor();
            }

            string clienteid = Global.InsigteManager[Session.SessionID].IdClient;
            if (Global.InsigteManager[Session.SessionID].inUser.HasPastas.ToString() == "1")
            {

                if (clienteid == "3003")
                {
                    btnDossierEA.Visible = true;
                    btnSelAll.Visible = false;
                }
                else
                {
                    btnDossier.Visible = true;
                    btnSelAll.Visible = false;
                }

            }
            else
            {
                btnDossier.Visible = false;
                btnDossierEA.Visible = false;
                btnSelAll.Visible = false;
            }

            if (clienteid == "3003")
            {
                btnDossierSearchEA.Visible = true;
            }
            else
            {
                btnDossierSearch.Visible = true;
            }

        }

        protected Boolean FbIsVisable()
        {
            if (Global.InsigteManager[Session.SessionID].inUser.HasAlerts.ToString() == "0")
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        protected string sReturnImgAttach(string attach)
        {
            return attach.Equals("1") ? "<img id=\"imagemt\" src=\"Imgs/icons/black/png/clip_icon_16.png\" alt=\"\" runat=\"server\" />" : "";
        }
		

        protected bool sReturnFace(string strValue)
        {
            //FaceBook
            DataTable find = Global.InsigteManager[Session.SessionID].getTableQuery("SELECT * FROM [iadvisers].[dbo].[FB10F_NEWS_POST] with(nolock) where  id_noticia = " + strValue, "getFaceNews");

            if (find.Rows.Count > 0)
                return true;

            return false;
        }

        protected Boolean chkIsVisable()
        {
            if (Global.InsigteManager[Session.SessionID].inUser.HasPastas.ToString() == "1")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected Boolean pdfIsVisable()
        {
            if (Global.InsigteManager[Session.SessionID].inUser.HasPastas.ToString() == "1")
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        protected Boolean checkDossier(String idArtigo)
        {
            if (HttpContext.Current.Request.Cookies["CookieinsigteInfo"] != null && idArtigo != string.Empty)
            {
                if (HttpContext.Current.Request.Cookies["CookieinsigteSys"]["PDF"] != null)
                {
                    HttpCookie cookie = Request.Cookies["CookieinsigteSys"];
                    string idVal = cookie.Values["PDF"];

                    if (idVal.Contains('|'))
                    {
                        string[] words = idVal.Split('|');
                        if (words.Contains(idArtigo))
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        if (idVal == idArtigo)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }


        }

        protected void btnSelAll_Click(object sender, EventArgs e)
        {
            foreach (DataGridItem drv in dgSearchACN.Items)
            {
                CheckBox chk = (CheckBox)drv.FindControl("chka_AddPDF");
                chk.Checked = true;
            }
        }

        protected String getResource(String resx)
        {
            return Resources.insigte.language.ResourceManager.GetObject(resx).ToString();
        }

        protected string sReturnIdComposed(string strValue)
        {
            string sLink = string.Empty;

            DataTable find = Global.InsigteManager[Session.SessionID].getTableQuery("select * from ML10F_CLIENT_USER_CONTROL_COMPOSED_ADD with(nolock) where convert(varchar,DAT_CREATE,112)=convert(varchar,getdate(),112) ", "findIdComposta");

            Boolean foundit = false;
            if (find.Rows.Count > 0)
            {
                foreach (DataRow row in find.Rows)
                {
                    if (row["ID_NEWS"].ToString() == strValue)
                        foundit = true;
                }
            }

            if (!foundit)
                sLink = strValue;

            return sLink;
        }

        protected string sReturnImgComposed(string strValue)
        {
            string sLink = "Imgs/icons/black/png/push_pin_icon_16_sell.png";

            DataTable find = Global.InsigteManager[Session.SessionID].getTableQuery("select * from ML10F_CLIENT_USER_CONTROL_COMPOSED_ADD with(nolock) where convert(varchar,DAT_CREATE,112)=convert(varchar,getdate(),112) ", "findIdComposta");

            Boolean foundit = false;
            if (find.Rows.Count > 0)
            {
                foreach (DataRow row in find.Rows)
                {
                    if (row["ID_NEWS"].ToString() == strValue)
                        foundit = true;
                }
            }

            if (foundit)
                sLink = "Imgs/icons/black/png/push_pin_icon_16.png";

            return sLink;
        }

        protected bool sReturnVisComposed()
        {
            //Newsletter Composta
            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(7, 1) == "0")
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        protected void bt_AddComposta_Command(Object sender, CommandEventArgs e)
        {
            if (e.CommandArgument.ToString().Length > 1)
            {
                addLinkComposta(e.CommandArgument.ToString());
                lblDataToday.Visible = true;
                //lblDataToday.Text = e.CommandArgument.ToString() + " : Adicionado com sucesso!";
                lblDataToday.Text = "Adicionado ao email com sucesso!";

                ImageButton b = sender as ImageButton;
                b.ImageUrl = "Imgs/icons/black/png/push_pin_icon_16.png";
            }
            else
            {
                lblDataToday.Visible = true;
                //lblDataToday.Text = e.CommandArgument.ToString() + " : Adicionado com sucesso!";
                lblDataToday.Text = "Já se encontra adicionada email!";
            }
        }

        protected void addLinkComposta(string qsPDFArtigo)
        {
            String Query = "insert into ML10F_CLIENT_USER_CONTROL_COMPOSED_ADD ";
            Query += " (ID_CLIENT, COD_NEWSLETTER_COMPOSE, ID_NEWS, DAT_CREATE) ";
            Query += " Select " + Global.InsigteManager[Session.SessionID].IdClient.ToString() + ", 0, " + qsPDFArtigo + ", getdate() ";
            DataTable insertComp = Global.InsigteManager[Session.SessionID].getTableQuery(Query, "InsertComp");
        }

        protected void getDataEditors()
        {
            String SQLquery = "select * from dbo.editors with(nolock) order by name asc ";
            if (Global.InsigteManager[Session.SessionID].inUser.UserPortalFilter != "*")
                SQLquery = "select * from dbo.editors with(nolock) where editorid in (" + Global.InsigteManager[Session.SessionID].inUser.UserPortalFilter + ") order by name asc ";
            Editors = new DataTable();
            Editors = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "FilterEditorsTemas");
        }

        protected void getDataAutor()
        {
            String SQLquery = "select * from dbo.authors with(nolock) order by name asc ";
            Autores = new DataTable();
            Autores = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "Autores");
        }

        protected String getJornalID(String Jornalist)
        {
            try
            {
                if (Jornalist == "Todos")
                    return "*";
                else
                {
                    foreach (DataRow row in Autores.Rows)
                    {
                        if (row["name"].ToString() == Jornalist)
                            return row["authorid"].ToString();
                    }
                }
            }
            catch (Exception)
            {
                return null;
            }
            return null;
        }

        protected String getEditorID(String Editor)
        {
            try
            {
                if (Editor == "Todos")
                    return "*";
                else
                {
                    foreach (DataRow row in Editors.Rows)
                    {
                        if (row["name"].ToString() == Editor)
                            return row["editorid"].ToString();
                    }
                }

            }
            catch (Exception)
            {
                return null;
            }
            return null;
        }

        //ADV Search
        private void advSearchGrid()
        {

            String SHText = Session["ADVSearchTxt"].ToString();
            String SHTema = Session["ADVSearchTema"].ToString();
            String SHPeriodo = Session["ADVSearchPeriodo"].ToString();
            String SHDtIni = Session["ADVSearchDatIni"].ToString();
            String SHDtFim = Session["ADVSearchDatFim"].ToString();
            String SHEditor = Session["ADVSearchEditor"].ToString();
            String SHJornal = Session["ADVSearchJornalistas"].ToString();
            String SHTipo = Session["ADVSearchTipo"].ToString();
            String SHPais = Session["ADVSearchPais"].ToString();
            String SHClasse = Session["ADVSearchClasse"].ToString();

            String Title = "TITLE";
            String text = "text";
            if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
            {
                Title = "TITLE_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage + " as TITLE";
                text = "text_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage;
            }

            String qQuery = "SELECT DISTINCT m.ID as IDMETA, m." + Title + ", e.name as EDITOR, m.DATE, m.filepath, m.page_url, m.tipo, cast(c." + text + " as nvarchar(max)) as text , ( case  isnull( ma.id_news, 0) when  0 then '0' else '1' end ) as [attach] ";
            if (Global.InsigteManager[Session.SessionID].IdClient == "1096")
            {
                qQuery += " , (select AVE_USD from [dbo].[fnCalMediaValue] (m.id) ) as AVE ";
                qQuery += " , (select OTS from [dbo].[fnCalMediaValue] (m.id) ) as OTS ";
            }
            qQuery += " FROM iadvisers.dbo.metadata m with(nolock) ";
            qQuery += " left join metadata_attach ma with(nolock) on ma.id_news = m.id ";
            qQuery += " INNER JOIN iadvisers.dbo.clientarticles ca with(nolock) ON ca.id = m.ID";
            qQuery += " INNER JOIN iadvisers.dbo.contents c with(nolock) ON c.id = m.ID ";
            qQuery += " INNER JOIN iadvisers.dbo.editors e with(nolock) ON m.editorid = e.editorid ";
            if (SHJornal != null && SHJornal != Resources.insigte.language.geralTagTodos)
                qQuery += " INNER JOIN iadvisers.dbo.authors a with(nolock) ON m.authorid = a.authorid ";

            String QUERY_WHERE = " WHERE ";

            // FILTRA TEMA 
            String FTEMA = "";

            if (SHTema != "1")
            {
                foreach (KeyValuePair<int, INinsigteManager.UserTemas> pair in Global.InsigteManager[Session.SessionID].inUser.Temas)
                {
                    if (pair.Value.IdTema + pair.Value.TabOrder.ToString() == SHTema)
                    {
                        if (pair.Value.IdTema.Substring(4, 3) == "000")
                        {
                            if (pair.Value.SubTemas.Length > 1 && pair.Value.SubTemas.Substring(4, 3) != "000")
                                FTEMA += " cast(ca.clientid as varchar) + cast(ca.subjectid as varchar) in ('" + pair.Value.SubTemas.Replace(";", "','") + "') ";
                            else
                                FTEMA += " ca.clientid = " + pair.Value.IdTema.Substring(0, 4) + " ";

                        }
                        else
                        {
                            FTEMA += " cast(ca.clientid as varchar) + cast(ca.subjectid as varchar) = '" + pair.Value.IdTema.Replace(";", "','") + "' ";
                        }

                        if (pair.Value.FILTER_EDITORS_PESQUISA != "*")
                            FTEMA += " AND e.editorid in (" + pair.Value.FILTER_EDITORS_PESQUISA + ") ";
                    }
                }
            }
            else
            {

                if (Global.InsigteManager[Session.SessionID].IdClient != "1002")
                {

                    foreach (KeyValuePair<int, INinsigteManager.UserTemas> pair in Global.InsigteManager[Session.SessionID].inUser.Temas)
                    {
                        FTEMA += " ( ";
                        if (pair.Value.IdTema.Substring(4, 3) == "000")
                        {
                            if (pair.Value.SubTemas.Length > 1 && pair.Value.SubTemas.Substring(4, 3) != "000")
                                FTEMA += " cast(ca.clientid as varchar) + cast(ca.subjectid as varchar) in ('" + pair.Value.SubTemas.Replace(";", "','") + "') ";
                            else
                                FTEMA += " ca.clientid = " + pair.Value.IdTema.Substring(0, 4) + " ";

                        }
                        else
                        {
                            FTEMA += " cast(ca.clientid as varchar) + cast(ca.subjectid as varchar) = '" + pair.Value.IdTema.Replace(";", "','") + "' ";
                        }

                        if (pair.Value.FILTER_EDITORS_PESQUISA != "*")
                            FTEMA += " AND e.editorid in (" + pair.Value.FILTER_EDITORS_PESQUISA + ") ";

                        FTEMA += " ) OR ";
                    }

                    FTEMA = FTEMA.Substring(0, FTEMA.Length - 3);
                }
                else
                {
                    FTEMA = "";
                }
            }

            if (FTEMA != "")
            {
                if (QUERY_WHERE == " WHERE ")
                    QUERY_WHERE += " ( " + FTEMA + " ) ";
                else
                    QUERY_WHERE += " AND ( " + FTEMA + " ) ";
            }


            // FILTRA PERIODO / DATAS
            String FPERIODO = "";

            if (SHDtIni != null && SHDtIni != "")
            {
                if (SHDtFim != null && SHDtFim != "")
                    FPERIODO = " convert(varchar,m.date,112) between convert(varchar,cast('" + SHDtIni + "' as datetime),112) and convert(varchar,cast('" + SHDtFim + "' as datetime),112) ";
                else
                    FPERIODO = " convert(varchar,m.date,112) between convert(varchar,cast('" + SHDtIni + "' as datetime),112) and convert(varchar,getdate(),112) ";
            }
            else
                FPERIODO = " m.date >= getdate()-" + SHPeriodo;


            if (FPERIODO != "")
            {
                if (QUERY_WHERE == " WHERE ")
                    QUERY_WHERE += " ( " + FPERIODO + " ) ";
                else
                    QUERY_WHERE += " AND ( " + FPERIODO + " ) ";
            }

            // FILTRA EDITORES
            String FEDITORS = "";

            if (SHEditor != null && SHEditor != Resources.insigte.language.geralTagTodos)
            {
                String[] edt = SHEditor.Split(',');
                foreach (String x in edt)
                {
                    FEDITORS += getEditorID(x) + ",";
                }
                FEDITORS = FEDITORS.TrimEnd(',');
                FEDITORS = " e.editorid in (" + FEDITORS + ") ";
            }

            if (FEDITORS != "")
            {
                if (QUERY_WHERE == " WHERE ")
                    QUERY_WHERE += " ( " + FEDITORS + " ) ";
                else
                    QUERY_WHERE += " AND ( " + FEDITORS + " ) ";
            }

            // FILTRA JORNALISTAS

            String FJORNALIST = "";

            if (SHJornal != null && SHJornal != Resources.insigte.language.geralTagTodos)
            {
                String[] edt = SHJornal.Split(',');
                foreach (String x in edt)
                {
                    FJORNALIST += getJornalID(x) + ",";
                }
                FJORNALIST = FJORNALIST.TrimEnd(',');
                FJORNALIST = " a.authorid in (" + FJORNALIST + ") ";
            }

            if (FJORNALIST != "")
            {
                if (QUERY_WHERE == " WHERE ")
                    QUERY_WHERE += " ( " + FJORNALIST + " ) ";
                else
                    QUERY_WHERE += " AND ( " + FJORNALIST + " ) ";
            }

            // FILTRA TIPO
            String FTIPO = "";

            if (SHTipo != null && SHTipo != "1")
            {
                FTIPO = " m.tipo = '" + SHTipo + "' ";
            }

            if (FTIPO != "")
            {
                if (QUERY_WHERE == " WHERE ")
                    QUERY_WHERE += " ( " + FTIPO + " ) ";
                else
                    QUERY_WHERE += " AND ( " + FTIPO + " ) ";
            }

            // FILTRA PAÍS
            String FPAIS = "";

            if (SHPais != null && SHPais != "1")
            {
                FPAIS = " e.country = '" + SHPais + "' ";
            }

            if (FPAIS != "")
            {
                if (QUERY_WHERE == " WHERE ")
                    QUERY_WHERE += " ( " + FPAIS + " ) ";
                else
                    QUERY_WHERE += " AND ( " + FPAIS + " ) ";
            }

            // FILTRA CLASSE
            String FCLASSE = "";

            if (SHClasse != null && SHClasse != "1")
            {
                FCLASSE = " e.subject = '" + SHClasse + "' ";
            }

            if (FCLASSE != "")
            {
                if (QUERY_WHERE == " WHERE ")
                    QUERY_WHERE += " ( " + FCLASSE + " ) ";
                else
                    QUERY_WHERE += " AND ( " + FCLASSE + " ) ";
            }


            // FILTRA TEXTO
            String FTEXTO = "";

            if (SHText != null && SHText.Trim() != "" && SHText != "Pesquisar texto")
            {
                FTEXTO = " CONTAINS(c." + text + ",'" + SHText + "') ";
            }

            if (FTEXTO != "")
            {
                if (QUERY_WHERE == " WHERE ")
                    QUERY_WHERE += " ( " + FTEXTO + " ) ";
                else
                    QUERY_WHERE += " AND ( " + FTEXTO + " ) ";
            }

            qQuery += QUERY_WHERE + " order by m.date desc, IDMETA desc";


            //lblDataToday.Text = qQuery;

            Global.InsigteManager[Session.SessionID].inUser.SQLLastSearch = qQuery;

            DataTable SearchRsl = new DataTable();
            SearchRsl = Global.InsigteManager[Session.SessionID].getTableQuery(qQuery, "SearchRsl");

            //lblDataToday.Visible = true;
            //lblDataToday.Text = qQuery + " <br>";

            try
            {

                if (SearchRsl.Rows.Count > 0)
                {
                    /****************************************/
                    Global.InsigteManager[Session.SessionID].publuicacoes = SearchRsl;
                    /****************************************/
                    dgSearchACN.DataSource = SearchRsl;

                    dgSearchACN.DataBind();
                    dgSearchACN.Visible = true;
                    lblWarning.Visible = false;
                    if (FTEXTO != "")

                        lblDataToday.Text += "Foram encontrados <b>" + SearchRsl.Rows.Count.ToString() + "</b> para : <b>" + Session["ADVSearchTxt"].ToString() + "</b><br>";


                    else
                        lblDataToday.Text += "Foram encontrados <b>" + SearchRsl.Rows.Count.ToString() + "</b><br>";

                    //lblDataToday.Text = qQuery;
                }
                else
                {
                    lblWarning.Text += "<br />Não foram encontrados resultados para a sua pesquisa.";
                    lblWarning.Visible = true;
                    dgSearchACN.Visible = false;
                }
            }
            catch (Exception e)
            {
                if (FTEXTO != "")
                    lblDataToday.Text += "Verifique o texto. Para frase exacta utilize a(s) palavra(s) entre aspas ou os operadores: AND, OR, AND NOT e NEAR para pesquisa boleana";



                //lblDataToday.Text = "Erro: Verifique o texto : <b>" + Session["ADVSearchTxt"].ToString() + "</b><br>" + e.Message.ToString();

                lblWarning.Text += "Ocorreu um problema com a pesquisa. para o Texto : <b> " + Session["ADVSearchTxt"].ToString() + "</b><br>";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>String With Filters</returns>
        protected void advSearchGridNew()
        {
            String SHText = Session["ADVSearchTxt"].ToString();
            String SHTema = Session["ADVSearchTema"].ToString();
            String SHPeriodo = Session["ADVSearchPeriodo"].ToString();
            String SHDtIni = Session["ADVSearchDatIni"].ToString();
            String SHDtFim = Session["ADVSearchDatFim"].ToString();
            String SHEditor = Session["ADVSearchEditor"].ToString();
            String SHJornal = Session["ADVSearchJornalistas"].ToString();
            String SHTipo = Session["ADVSearchTipo"].ToString();
            String SHPais = Session["ADVSearchPais"].ToString();
            String SHClasse = Session["ADVSearchClasse"].ToString();

            String Title = "TITLE";
            String text = "text";
            if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
            {
                Title = "TITLE_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage + " as TITLE";
                text = "text_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage;
            }

            String qQuery = "SELECT DISTINCT m.ID as IDMETA, m." + Title + ", e.name as EDITOR, m.DATE, m.filepath, m.page_url, m.tipo, cast(c." + text + " as nvarchar(max)) as text ";
            {
                qQuery += " , (select AVE_USD from [dbo].[fnCalMediaValue] (m.id) ) as AVE ";
                qQuery += " , (select OTS from [dbo].[fnCalMediaValue] (m.id) ) as OTS ";
            }
            qQuery += " FROM iadvisers.dbo.metadata m with(nolock)";
            qQuery += " JOIN iadvisers.dbo.clientarticles ca with(nolock) ON ca.id = m.ID";
            qQuery += " JOIN iadvisers.dbo.contents c with(nolock) ON c.id = m.ID ";
            qQuery += " JOIN iadvisers.dbo.editors e with(nolock) ON m.editorid = e.editorid ";
            if (SHJornal != null && SHJornal != Resources.insigte.language.geralTagTodos)
                qQuery += " JOIN iadvisers.dbo.authors a with(nolock) ON m.authorid = a.authorid ";

            String QUERY_WHERE = " WHERE 1 = 1 AND";

            //FILTRA TEMA
            #region FILTRA TEMA
          
            String FTEMA = "";

            if (SHTema != "1")
            {
                foreach (KeyValuePair<int, INinsigteManager.UserTemas> pair in Global.InsigteManager[Session.SessionID].inUser.Temas)
                {
                    if (pair.Value.IdTema + pair.Value.TabOrder.ToString() == SHTema)
                    {
                        if (pair.Value.IdTema.Substring(4, 3) == "000")
                        {
                            if (pair.Value.SubTemas.Length > 1 && pair.Value.SubTemas.Substring(4, 3) != "000")
                                FTEMA += " cast(ca.clientid as varchar) + cast(ca.subjectid as varchar) in ('" + pair.Value.SubTemas.Replace(";", "','") + "') ";
                            else
                                FTEMA += " ca.clientid = " + pair.Value.IdTema.Substring(0, 4) + " ";

                        }
                        else
                        {
                            FTEMA += " cast(ca.clientid as varchar) + cast(ca.subjectid as varchar) = '" + pair.Value.IdTema.Replace(";", "','") + "' ";
                        }

                        if (pair.Value.FILTER_EDITORS_PESQUISA != "*")
                            FTEMA += " AND e.editorid in (" + pair.Value.FILTER_EDITORS_PESQUISA + ") ";
                    }
                }
            }
            else
            {

                if (Global.InsigteManager[Session.SessionID].IdClient != "1002")
                {

                    foreach (KeyValuePair<int, INinsigteManager.UserTemas> pair in Global.InsigteManager[Session.SessionID].inUser.Temas)
                    {
                        FTEMA += " ( ";
                        if (pair.Value.IdTema.Substring(4, 3) == "000")
                        {
                            if (pair.Value.SubTemas.Length > 1 && pair.Value.SubTemas.Substring(4, 3) != "000")
                                FTEMA += " cast(ca.clientid as varchar) + cast(ca.subjectid as varchar) in ('" + pair.Value.SubTemas.Replace(";", "','") + "') ";
                            else
                                FTEMA += " ca.clientid = " + pair.Value.IdTema.Substring(0, 4) + " ";

                        }
                        else
                        {
                            FTEMA += " cast(ca.clientid as varchar) + cast(ca.subjectid as varchar) = '" + pair.Value.IdTema.Replace(";", "','") + "' ";
                        }

                        if (pair.Value.FILTER_EDITORS_PESQUISA != "*")
                            FTEMA += " AND e.editorid in (" + pair.Value.FILTER_EDITORS_PESQUISA + ") ";

                        FTEMA += " ) OR ";
                    }

                    FTEMA = FTEMA.Substring(0, FTEMA.Length - 3);
                }
                else
                {
                    FTEMA = "";
                }
            }

            QUERY_WHERE += string.IsNullOrEmpty(FTEMA) ? "": " AND ( " + FTEMA + " ) ";

            #endregion //FILTRA TEMA

            // FILTRA PERIODO / DATAS
            #region FILTRA PERIODO / DATAS

            String FPERIODO = "";

            if (SHDtIni != null && SHDtIni != "")
            {
                if (SHDtFim != null && SHDtFim != "")
                    FPERIODO = " convert(varchar,m.date,112) between convert(varchar,cast('" + SHDtIni + "' as datetime),112) and convert(varchar,cast('" + SHDtFim + "' as datetime),112) ";
                else
                    FPERIODO = " convert(varchar,m.date,112) between convert(varchar,cast('" + SHDtIni + "' as datetime),112) and convert(varchar,getdate(),112) ";
            }
            else
                FPERIODO = " m.date >= getdate()-" + SHPeriodo;

            QUERY_WHERE += string.IsNullOrEmpty(FPERIODO) ? "" : " AND ( " + FPERIODO + " ) ";

            #endregion //FILTRA PERIODO / DATAS

            // FILTRA EDITORES
            #region FILTRA EDITORES
            
            if (SHEditor != null && SHEditor != Resources.insigte.language.geralTagTodos)
            {
                QUERY_WHERE += string.IsNullOrEmpty(SHEditor) ? "" : " AND ( " + SHEditor.TrimEnd(',') + " ) ";
            }

            #endregion // FILTRA EDITORES

            // FILTRA JORNALISTAS
            #region FILTRA JORNALISTAS

            if (SHJornal != null && SHJornal != Resources.insigte.language.geralTagTodos)
            {
                QUERY_WHERE += string.IsNullOrEmpty(SHJornal) ? "" : " AND ( " + SHJornal.TrimEnd(',') + " ) ";
            }

            #endregion //FILTRA JORNALISTAS

            // FILTRA TIPO
            #region FILTRA TIPO
            
            String FTIPO = "";

            if (SHTipo != null && SHTipo != "1")
            {
                FTIPO = " m.tipo = '" + SHTipo + "' ";
            }

            QUERY_WHERE += string.IsNullOrEmpty(FTIPO) ? "" : " AND ( " + FTIPO + " ) ";

            #endregion // FILTRA TIPO

            // FILTRA PAÍS
            #region FILTRA PAÍS
            String FPAIS = "";
            if (SHPais != null && SHPais != "1")
            {
                FPAIS = " e.country = '" + SHPais + "' ";
            }

            QUERY_WHERE += string.IsNullOrEmpty(FPAIS) ? "" : " AND ( " + FPAIS + " ) ";

            #endregion // FILTRA PAÍS

            //FILTRA CLASSE
            #region FILTRA CLASSE
            
            String FCLASSE = "";

            if (SHClasse != null && SHClasse != "1")
            {
                FCLASSE = " e.subject = '" + SHClasse + "' ";
            }

            QUERY_WHERE += string.IsNullOrEmpty(FCLASSE) ? "" : " AND ( " + FCLASSE + " ) ";

            #endregion //FILTRA CLASSE

            // FILTRA TEXTO
            #region FILTRA TEXTO
            
            String FTEXTO = "";

            if (SHText != null && SHText.Trim() != "" && SHText != "Pesquisar texto")
            {
                FTEXTO = " CONTAINS(c." + text + ",'" + SHText + "') ";
            }

            QUERY_WHERE += string.IsNullOrEmpty(FTEXTO) ? "" : " AND ( " + FTEXTO + " ) ";

            #endregion //FILTRA TEXTO

            qQuery += QUERY_WHERE + " order by m.date desc, IDMETA desc ";


            //lblDataToday.Text = qQuery;

            Global.InsigteManager[Session.SessionID].inUser.SQLLastSearch = qQuery;

            DataTable SearchRsl = new DataTable();
            
            //lblDataToday.Visible = true;
            //lblDataToday.Text = qQuery + " <br>";

            try
            {
                SearchRsl = Global.InsigteManager[Session.SessionID].getTableQuery(qQuery, "SearchRsl");

                if (SearchRsl.Rows.Count > 0)
                {
                    /****************************************/
                    Global.InsigteManager[Session.SessionID].publuicacoes = SearchRsl;
                    /****************************************/
                    dgSearchACN.DataSource = SearchRsl;

                    dgSearchACN.DataBind();
                    dgSearchACN.Visible = true;
                    lblWarning.Visible = false;
                    if (FTEXTO != "")

                        lblDataToday.Text += "Foram encontrados <b>" + SearchRsl.Rows.Count.ToString() + "</b> para : <b>" + Session["ADVSearchTxt"].ToString() + "</b><br>";


                    else
                        lblDataToday.Text += "Foram encontrados <b>" + SearchRsl.Rows.Count.ToString() + "</b><br>";

                    //lblDataToday.Text = qQuery;
                }
                else
                {
                    lblWarning.Text += "<br />Não foram encontrados resultados para a sua pesquisa.";
                    lblWarning.Visible = true;
                    dgSearchACN.Visible = false;
                }
            }
            catch (Exception e)
            {
                if (FTEXTO != "")
                    lblDataToday.Text += "Verifique o texto. Para frase exacta utilize a(s) palavra(s) entre aspas ou os operadores: AND, OR, AND NOT e NEAR para pesquisa boleana";

                lblWarning.Text += "Ocorreu um problema com a pesquisa. para o Texto : <b> " + Session["ADVSearchTxt"].ToString() + "</b><br>";
            } 
        }

        protected void dgSearchACN_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgSearchACN.CurrentPageIndex = e.NewPageIndex;

            if (Session["ADVSearchBool"] != null && Session["ADVSearchBool"].ToString() == "true")
                advSearchGrid();
        }

        protected string sReturnLinkPasta(string strValue)
        {
            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(0, 1) == "0")
            {
                return null;
            }
            string sLink = string.Empty;
            string idVal = string.Empty;

            string cmdQueryPastas = "select * from dbo.CL10H_PASTAS_ARTICLES with(nolock) where id_client = " + Global.InsigteManager[Session.SessionID].IdClient + " and id_client_user = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + " and articleid=" + strValue;
            SqlConnection conn = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");
            try
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand(cmdQueryPastas);
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;

                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows == true)
                {
                    while (dr.Read())
                        idVal += dr["articleid"].ToString() + "|";
                }

                idVal.Substring(0, idVal.Length - 1);

                dr.Close();

            }
            catch (Exception exp)
            {
                exp.Message.ToString();
            }
            finally
            {
                conn.Dispose();
                conn.Close();
            }


            if (idVal.Contains('|'))
            {
                string[] words = idVal.Split('|');
                if (words.Contains(strValue))
                {
                    sLink = "<a target=" + "\"" + "_self" + "\"" + " href=" + "\"" + "javascript:window.open('Adicionar.aspx?ID=" + strValue + "','CustomPopUp', 'width=400, height=200, menubar=no, resizeble=no, location=no'); void(0)" + "\"" + " style='border-width:0px;text-decoration:none;' >" + "<img src='Imgs/icons/black/png/folder_open_icon_16_sel.png' width='16px' height='16px' alt='" + Resources.insigte.language.defColAccoesPastas + "' title='" + Resources.insigte.language.defColAccoesPastas + "' style='border-width:0px;'/></a>";
                }
                else
                {
                    sLink = "<a target=" + "\"" + "_self" + "\"" + " href=" + "\"" + "javascript:window.open('Adicionar.aspx?ID=" + strValue + "','CustomPopUp', 'width=400, height=200, menubar=no, resizeble=no, location=no'); void(0)" + "\"" + " style='border-width:0px;text-decoration:none;' >" + "<img src='Imgs/icons/black/png/folder_open_icon_16.png' width='16px' height='16px' alt='" + Resources.insigte.language.defColAccoesPastas + "' title='" + Resources.insigte.language.defColAccoesPastas + "' style='border-width:0px;'/></a>";
                }
            }
            else
            {
                if (idVal == strValue)
                {
                    sLink = "<a target=" + "\"" + "_self" + "\"" + " href=" + "\"" + "javascript:window.open('Adicionar.aspx?ID=" + strValue + "','CustomPopUp', 'width=400, height=200, menubar=no, resizeble=no, location=no'); void(0)" + "\"" + " style='border-width:0px;text-decoration:none;' >" + "<img src='Imgs/icons/black/png/folder_open_icon_16_sel.png' width='16px' height='16px' alt='" + Resources.insigte.language.defColAccoesPastas + "' title='" + Resources.insigte.language.defColAccoesPastas + "' style='border-width:0px;'/></a>";
                }
                else
                {
                    sLink = "<a target=" + "\"" + "_self" + "\"" + " href=" + "\"" + "javascript:window.open('Adicionar.aspx?ID=" + strValue + "','CustomPopUp', 'width=400, height=200, menubar=no, resizeble=no, location=no'); void(0)" + "\"" + " style='border-width:0px;text-decoration:none;' >" + "<img src='Imgs/icons/black/png/folder_open_icon_16.png' width='16px' height='16px' alt='" + Resources.insigte.language.defColAccoesPastas + "' title='" + Resources.insigte.language.defColAccoesPastas + "' style='border-width:0px;'/></a>";
                }
            }

            return sLink;
        }

        protected string sReturnTitle(string idArticle, string sTitle)
        {
            string sLink = string.Empty;

            //select clientid, subjectid from clientarticles where id='164400'

            SqlConnection conn = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");
            try
            {
                conn.Open();

                string cmdQuery = "select clientid, subjectid from clientarticles with(nolock) where id='" + idArticle + "'";

                SqlCommand cmd = new SqlCommand(cmdQuery);
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;

                SqlDataReader reader = cmd.ExecuteReader();

                bool isACNArticle = false;

                while (reader.Read())
                {
                    if (reader.GetValue(0).ToString() == Global.InsigteManager[Session.SessionID].TemaCliente.Substring(0, 4) && reader.GetValue(1).ToString() == Global.InsigteManager[Session.SessionID].TemaCliente.Substring(4, 3))
                    {
                        isACNArticle = true;
                    }
                }

                if (isACNArticle)
                {
                    sLink = "<b>" + sTitle + "</b>";
                }
                else
                {
                    sLink = sTitle;
                }

                cmd.Connection.Close();
                cmd.Connection.Dispose();

                return sLink;


            }
            catch (Exception exp)
            {
                exp.Message.ToString();
                return sTitle;

            }
        }

        protected string sReturnImgLinkDetalhe(string strValue)
        {
            string sLink = string.Empty;

            if (HttpContext.Current.Request.Cookies["CookieinsigteInfo"] != null && strValue != string.Empty)
            {
                if (HttpContext.Current.Request.Cookies["CookieinsigteSys"]["ID"] != null)
                {
                    HttpCookie cookie = Request.Cookies["CookieinsigteSys"];
                    string idVal = cookie.Values["ID"];

                    if (idVal.Contains('|'))
                    {
                        string[] words = idVal.Split('|');
                        if (words.Contains(strValue))
                        {
                            sLink = "Imgs/icons/black/png/clipboard_copy_icon_16_sel.png";
                        }
                        else
                        {
                            sLink = "Imgs/icons/black/png/clipboard_copy_icon_16.png";
                        }
                    }
                    else
                    {
                        if (idVal == strValue)
                        {
                            sLink = "Imgs/icons/black/png/clipboard_copy_icon_16_sel.png";
                        }
                        else
                        {
                            sLink = "Imgs/icons/black/png/clipboard_copy_icon_16.png";
                        }
                    }
                }
            }
            else
            {
                sLink = "Imgs/icons/black/png/clipboard_copy_icon_16.png";
            }
            return sLink;
        }

        protected string sReturnIdLinkDetalhe(string strValue)
        {
            string sLink = string.Empty;

            if (HttpContext.Current.Request.Cookies["CookieinsigteInfo"] != null && strValue != string.Empty)
            {
                if (HttpContext.Current.Request.Cookies["CookieinsigteSys"]["ID"] != null)
                {
                    HttpCookie cookie = Request.Cookies["CookieinsigteSys"];
                    string idVal = cookie.Values["ID"];

                    if (idVal.Contains('|'))
                    {
                        string[] words = idVal.Split('|');
                        if (words.Contains(strValue))
                        {
                            sLink = string.Empty;
                        }
                        else
                        {
                            sLink = strValue;
                        }
                    }
                    else
                    {
                        if (idVal == strValue)
                        {
                            sLink = string.Empty;
                        }
                        else
                        {
                            sLink = strValue;
                        }
                    }
                }
            }
            else
            {
                sLink = strValue;
            }
            return sLink;
        }

        protected string sReturnImgPDF(string strValue)
        {
            string sLink = string.Empty;

            if (HttpContext.Current.Request.Cookies["CookieinsigteInfo"] != null && strValue != string.Empty)
            {
                if (HttpContext.Current.Request.Cookies["CookieinsigteSys"]["PDF"] != null)
                {
                    HttpCookie cookie = Request.Cookies["CookieinsigteSys"];
                    string idVal = cookie.Values["PDF"];

                    if (idVal.Contains('|'))
                    {
                        string[] words = idVal.Split('|');
                        if (words.Contains(strValue))
                        {
                            sLink = "Imgs/icons/black/png/book_side_icon_16_sel.png";
                        }
                        else
                        {
                            sLink = "Imgs/icons/black/png/book_side_icon_16.png";
                        }
                    }
                    else
                    {
                        if (idVal == strValue)
                        {
                            sLink = "Imgs/icons/black/png/book_side_icon_16_sel.png";
                        }
                        else
                        {
                            sLink = "Imgs/icons/black/png/book_side_icon_16.png";
                        }
                    }
                }
            }
            else
            {
                sLink = "Imgs/icons/black/png/book_side_icon_16.png";
            }
            return sLink;
        }

        protected string sReturnIdPDF(string strValue)
        {
            string sLink = string.Empty;

            if (HttpContext.Current.Request.Cookies["CookieinsigteInfo"] != null && strValue != string.Empty)
            {
                if (HttpContext.Current.Request.Cookies["CookieinsigteSys"]["PDF"] != null)
                {
                    HttpCookie cookie = Request.Cookies["CookieinsigteSys"];
                    string idVal = cookie.Values["PDF"];

                    if (idVal.Contains('|'))
                    {
                        string[] words = idVal.Split('|');
                        if (words.Contains(strValue))
                        {
                            sLink = string.Empty;
                        }
                        else
                        {
                            sLink = strValue;
                        }
                    }
                    else
                    {
                        if (idVal == strValue)
                        {
                            sLink = string.Empty;
                        }
                        else
                        {
                            sLink = strValue;
                        }
                    }
                }
            }
            else
            {
                sLink = strValue;
            }
            return sLink;
        }

        protected void addLinkPDF(string qsPDFArtigo)
        {

            string ValCookie = string.Empty;
            string PDFCookie = string.Empty;
            string DelCookie = string.Empty;

            HttpCookie cookie = Request.Cookies["CookieinsigteSys"];

            if (cookie.Values["PDF"] != "")
            {
                PDFCookie = cookie.Values["PDF"] + "|" + qsPDFArtigo;
            }
            else
            {
                PDFCookie = qsPDFArtigo;
            }

            DelCookie = cookie.Values["DELNOT"];
            ValCookie = cookie.Values["ID"];

            cookie.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(cookie);

            HttpCookie newCookie = new HttpCookie("CookieinsigteSys");
            newCookie["Version"] = "JUL2012";
            newCookie["ID"] = ValCookie;
            newCookie["PDF"] = PDFCookie;
            newCookie["DELNOT"] = DelCookie;
            newCookie.Expires = DateTime.Now.AddDays(360);
            Response.Cookies.Add(newCookie);

            //lblArtigoAddPDF.Text = "O artigo de ID: " + qsPDFArtigo + ", foi adicionado com sucesso.";

        }

        protected void addNews(string qsIDArtigo)
        {
            string ValCookie = string.Empty;
            string PDFCookie = string.Empty;
            string DelCookie = string.Empty;

            HttpCookie cookie = Request.Cookies["CookieinsigteSys"];

            if (cookie.Values["ID"] != "")
            {
                ValCookie = cookie.Values["ID"] + "|" + qsIDArtigo;
            }
            else
            {
                ValCookie = qsIDArtigo;
            }

            DelCookie = cookie.Values["DELNOT"];
            PDFCookie = cookie.Values["PDF"];

            cookie.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(cookie);

            HttpCookie newCookie = new HttpCookie("CookieinsigteSys");
            newCookie["Version"] = "JUL2012";
            newCookie["ID"] = ValCookie;
            newCookie["PDF"] = PDFCookie;
            newCookie["DELNOT"] = DelCookie;
            newCookie.Expires = DateTime.Now.AddDays(360);
            Response.Cookies.Add(newCookie);

            //lblArtigoAdd.Text = "O artigo de ID: " + qsIDArtigo + ", foi adicionado com sucesso.";
        }

        protected void bt_AddPDF_Command(Object sender, CommandEventArgs e)
        {
            addLinkPDF(e.CommandArgument.ToString());
            lblDataToday.Text = e.CommandArgument.ToString() + " : Adicionado com sucesso!";

            ImageButton b = sender as ImageButton;
            b.ImageUrl = "Imgs/icons/black/png/book_side_icon_16_sel.png";
        }

        protected void bt_AddArquivo_Command(Object sender, CommandEventArgs e)
        {
            addNews(e.CommandArgument.ToString());
            lblDataToday.Text = e.CommandArgument.ToString() + " : Adicionado com sucesso!";

            ImageButton b = sender as ImageButton;
            b.ImageUrl = "Imgs/icons/black/png/clipboard_copy_icon_16_sel.png";
        }

        protected void btnExportXML_Click(object sender, EventArgs e)
        {


            //SqlCommand cmd = new SqlCommand(SQLLASTQUERY);
            //DataTable dt = GetData(cmd);
            DataTable SearchRsl = new DataTable();
            SearchRsl = Global.InsigteManager[Session.SessionID].getTableQuery(Global.InsigteManager[Session.SessionID].inUser.SQLLastSearch, "SearchRsl");


            //Create a dummy GridView
            GridView GridView1 = new GridView();
            GridView1.AllowPaging = false;
            GridView1.DataSource = SearchRsl;
            GridView1.DataBind();

            string timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=Geral_" + timestamp + ".xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";

            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                //Apply text style to each Row
                GridView1.Rows[i].Attributes.Add("class", "textmode");
            }
            GridView1.RenderControl(hw);
            //style to format numbers to string

            string style = @"<style> .textmode { mso-number-format:\@; } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }

        protected void btnBuildPDF_Click(object sender, EventArgs e)
        {

            DataTable SearchRsl = new DataTable();
            SearchRsl = Global.InsigteManager[Session.SessionID].getTableQuery(Global.InsigteManager[Session.SessionID].inUser.SQLLastSearch, "SearchRsl");

            string timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
            string varSolutionDir = "C:\\files\\";
            string nomeOutFile = "RevistaImprensaMISC" + timestamp + ".pdf";
            string varOutputFile = "C:\\pdf\\" + nomeOutFile;
            string varIndice = "C:\\pdf\\" + "findiceMISC" + timestamp + ".pdf";
            string varCapa = "C:\\pdf\\template_first.pdf";

            //Gerar Indice
            APToolkitNET.Toolkit objTK = new APToolkitNET.Toolkit();
            int intOpenOutputFile = objTK.OpenOutputFile(varIndice);

            if (intOpenOutputFile != 0)
            {
                ErrorHandler("OpenOutputFile", intOpenOutputFile);
            }

            objTK.NewPage();
            objTK.SetFont("Helvetica", 18f);
            objTK.PrintText(470f, 700f, "insigte");

            objTK.SetFont("Helvetica", 18f);
            objTK.PrintText(50f, 640f, "media intelligence");

            objTK.SetFont("Helvetica", 16f);
            objTK.PrintText(50f, 600f, Resources.insigte.language.defPDFDossierImp);

            objTK.SetFont("Helvetica", 10f);
            objTK.PrintText(50f, 560f, Resources.insigte.language.defPDFDossierImpIndice);


            String Title = "TITLE";
            String texto = "TEXT";
            if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
            {
                Title = "TITLE_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage;
                texto = "TEXT_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage;
            }

            //Buscar a Listagem
            float x = 0f;

            foreach (DataRow nw in SearchRsl.Rows)
            {
                x += 20f;
                objTK.SetFont("Helvetica", 10f);

                String Titulo = nw[Title].ToString();

                if (Titulo.Length > 75)
                {
                    objTK.PrintText(50f, 560f - x, Titulo.Substring(0, Titulo.IndexOf(' ', 65)));
                    objTK.PrintText(400f, 560f - x, nw["EDITOR"].ToString());
                    objTK.PrintText(500f, 560f - x, nw["DATE"].ToString().Substring(0, 8));

                    x += 20f;
                    objTK.SetFont("Helvetica", 10f);

                    objTK.PrintText(50f, 560f - x, Titulo.Substring(Titulo.IndexOf(' ', 65), Titulo.Length - (Titulo.IndexOf(' ', 65))));
                    objTK.PrintText(400f, 560f - x, " ");
                    objTK.PrintText(500f, 560f - x, " ");
                }
                else
                {

                    objTK.PrintText(50f, 560f - x, nw[Title].ToString());
                    objTK.PrintText(400f, 560f - x, nw["EDITOR"].ToString());
                    objTK.PrintText(500f, 560f - x, nw["DATE"].ToString().Substring(0, 10));
                }
            }

            objTK.CloseOutputFile();
            objTK = null;

            //Gera Principal
            APToolkitNET.Toolkit TK = new APToolkitNET.Toolkit();
            int varReturn = TK.OpenOutputFile(varOutputFile);

            if (varReturn != 0)
            {
                ErrorHandler("OpenOutputFile", varReturn);
            }

            string varMergeFile1 = varCapa;

            varReturn = TK.MergeFile(varMergeFile1, 0, 0);

            if (varReturn <= 0)
            {
                ErrorHandler("MergeFile", varReturn);
            }

            string varMergeFile2 = varIndice;

            varReturn = TK.MergeFile(varMergeFile2, 0, 0);

            if (varReturn <= 0)
            {
                ErrorHandler("MergeFile", varReturn);
            }

            foreach (DataRow nw in SearchRsl.Rows)
            {
                if (nw["filepath"].ToString().Length > 0 && Global.InsigteManager[Session.SessionID].inUser.CodLanguage == "pt")
                {
                    varMergeFile1 = varSolutionDir + nw["filepath"].ToString();
                    varReturn = TK.MergeFile(varMergeFile1, 0, 0);
                    if (varReturn <= 0)
                    {
                        ErrorHandler("MergeFile", varReturn);
                        //lblWarning.Text += "Bate aqui - Erro1: " + reader.GetValue(4).ToString() + "<br/>";
                    }
                }
                else
                {
                    TK.SetTextColor(0, 84, 132, 0, 0);
                    string varTexto = nw["text"].ToString();
                    string varEditor = nw["EDITOR"].ToString();
                    string varDate = nw["DATE"].ToString().Substring(0, 10);
                    string varTitulo = nw["TITLE"].ToString();
                    TK.SetFont("Arial", 11f);
                    TK.PrintText(40f, 700f, varEditor);
                    TK.PrintText(40f, 680f, varDate);
                    TK.GreyBar(40f, 660f, 500f, 1f, 0.5f);
                    TK.SetTextColor(0, 0, 0, 0, 0);
                    TK.SetFont("Arial", 11f);
                    TK.PrintText(40f, 640f, varTitulo);
                    TK.PrintMultilineText("Arial", 10f, 40f, 620f, 500f, 600f, varTexto, 0, 0);
                    //varTexto = TK.ClipText; //Qué isto? - Dá Erro
                    while (TK.ClipText != "")
                    {
                        TK.NewPage();
                        TK.PrintMultilineText("Arial", 10f, 40f, 700f, 500f, 680f, TK.ClipText, 0, 0);
                    }

                    TK.NewPage();
                    //lblWarning.Text += "Bate aqui 2: " + reader.GetValue(4).ToString() + "<br/>";
                }

            }
            //CloseOutputFile
            TK.CloseOutputFile();
            //Clear Object
            TK = null;

            File.Copy(varOutputFile, varSolutionDir + nomeOutFile);

            string DownloadPath = "C:\\files\\" + nomeOutFile;
            HttpContext.Current.Response.ContentType = "application/octet-stream";
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + nomeOutFile + "\"");
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.WriteFile(DownloadPath);
            HttpContext.Current.Response.End();
        }

        public static void ErrorHandler(string strMethod, object rtnCode)
        {
            System.Diagnostics.Debug.WriteLine(strMethod + " error:  " + rtnCode.ToString());
        }

        protected string sReturnIconTipoPasta(string url, string tipo, string file)
        {
            //Imprensa
            //Insigte
            //Online
            //Rádio
            //Televisão
            switch (tipo)
            {
                case "Imprensa":
                    String FileHelperPath = "ficheiros";
                    if (Global.InsigteManager[Session.SessionID].CostumPDFLogo == "1")
                    {
                        string auxfile = @"G:\cfiles\" + Global.InsigteManager[Session.SessionID].IdClient + "\\" + file.Replace("/", "\\");
                        bool urlexists = INinsigteManager.Utils.URLExists("http://insigte.com/cficheiros/" + Global.InsigteManager[Session.SessionID].IdClient + "\\" + file);

                        if (File.Exists(auxfile) || urlexists)
                        {
                            FileHelperPath = "cficheiros/" + Global.InsigteManager[Session.SessionID].IdClient;
                        }

                        //if (File.Exists(@"G:\cfiles\" + Global.InsigteManager[Session.SessionID].IdClient + "\\" + file.Replace("/", "\\")))
                        //{
                        //    FileHelperPath = "cficheiros/" + Global.InsigteManager[Session.SessionID].IdClient;
                        //}
                    }
                    return string.Format("<a href=\"http://insigte.com/" + FileHelperPath + "/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/doc_export_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download PDF\" style=\"border-width:0px;\" title=\"Download PDF\" /></a>", file);
                    //return string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/doc_export_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download PDF\" style=\"border-width:0px;\" title=\"Download PDF\" /></a>", file);
                    break;
                case "Online":
                    return string.Format("<a href=\"{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/globe_1_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Link\" style=\"border-width:0px;\" title=\"Link\" /></a>", url);
                    break;
                case "Rádio":
                    return string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/headphones_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download MP3\" style=\"border-width:0px;\" title=\"Download MP3\" /></a>", file);
                    break;
                case "Televisão":
                    return string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/movie_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download MP4\" style=\"border-width:0px;\" title=\"Download MP4\" /></a>", file);
                    break;
                default:
                    return string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/doc_export_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download PDF\" style=\"border-width:0px;\" title=\"Download PDF\" /></a>", file);
                    break;
            }
            return null;
        }

        protected void btnDossier_Click(object sender, EventArgs e)
        {
            lblDataToday.Visible = true;

            string ValCookie = string.Empty;
            string PDFCookie = string.Empty;
            string DelCookie = string.Empty;

            HttpCookie cookie = Request.Cookies["CookieinsigteSys"];
            String Artigos = "";
            foreach (DataGridItem drv in dgSearchACN.Items)
            {
                CheckBox chk = (CheckBox)drv.FindControl("chka_AddPDF");
                ImageButton btn = (ImageButton)drv.FindControl("ImageButton1");
                if (chk.Checked)
                {
                    Artigos += chk.Attributes["NewsId"].ToString() + "|";
                    btn.ImageUrl = "Imgs/icons/black/png/book_side_icon_16_sel.png";
                    chk.Checked = false;
                }
            }

            Artigos = Artigos.TrimEnd('|');

            if (cookie.Values["PDF"] != "")
            {
                PDFCookie = cookie.Values["PDF"] + "|" + Artigos;
            }
            else
            {
                PDFCookie = Artigos;
            }

            DelCookie = cookie.Values["DELNOT"];
            ValCookie = cookie.Values["ID"];

            cookie.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(cookie);

            HttpCookie newCookie = new HttpCookie("CookieinsigteSys");
            newCookie["Version"] = "JUL2012";
            newCookie["ID"] = ValCookie;
            newCookie["PDF"] = PDFCookie;
            newCookie["DELNOT"] = DelCookie;
            newCookie.Expires = DateTime.Now.AddDays(360);
            Response.Cookies.Add(newCookie);

            //drv.Cells[0].Text;

            lblDataToday.Text += " Artigos adicionados com sucesso!";

        }

        protected void btnNewsLetterCp_Click(object sender, EventArgs e)
        {
            lblDataToday.Visible = true;

            string query = "";

            foreach (DataGridItem drv in dgSearchACN.Items)
            {
                CheckBox chk = (CheckBox)drv.FindControl("chka_AddPDF");
                if (chk.Checked)
                {
                    query = " insert into ML10F_CLIENT_NEWSLETTER_CHOICE ";
                    query += " (ID_CLIENT_USER,ID_CLIENT,ID_NEWS) ";
                    query += " values(" + Global.InsigteManager[Session.SessionID].inUser.IdClientUser.ToString() + "," + Global.InsigteManager[Session.SessionID].IdClient.ToString() + "," + chk.Attributes["NewsId"].ToString() + "); ";

                    DataTable NewsLetterChoice = Global.InsigteManager[Session.SessionID].getTableQuery(query, "InsNewsLetterChoice");
                }
            }


            //lblDataToday.Text += " Artigos adicionados com sucesso!";
        }

        protected void btnAddtoFolders_Click(object sender, EventArgs e)
        {
            string chkbox = "";
            foreach (DataGridItem drv in dgSearchACN.Items)
            {
                CheckBox chk = (CheckBox)drv.FindControl("chka_AddPDF");
                if (chk.Checked)
                {
                    chkbox += chk.Attributes["NewsId"].ToString() + ";";
                }
            }

            ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('Adicionar.aspx?ID=" + chkbox.ToString() + "','CustomPopUp', 'width=400, height=200, menubar=no, resizeble=no, location=no');", true);

            //lblDataToday.Visible = true;
            //lblDataToday.Text = chkbox;
        }

        protected void btnAddtoClip_Click(object sender, EventArgs e)
        {
            string items = "";
            foreach (DataGridItem drv in dgSearchACN.Items)
            {
                CheckBox chk = (CheckBox)drv.FindControl("chka_AddPDF");
                ImageButton btn = (ImageButton)drv.FindControl("bt_AddArquivo");
                if (chk.Checked)
                {

                    //RemArt(chk.Attributes["NewsId"].ToString());
                    items += chk.Attributes["NewsId"].ToString() + "|";
                    btn.ImageUrl = "Imgs/icons/black/png/clipboard_copy_icon_16_sel.png";
                    chk.Checked = false;
                    //drv.Visible = false;

                }
            }
            addNews(items.TrimEnd('|'));

        }

        protected void chkSelAll_changed(object sender, EventArgs e)
        {
            CheckBox chk2 = (CheckBox)sender;
            //CheckBox chk1 = (CheckBox)dgPasta.FindControl("chkSelAll");
            foreach (DataGridItem drv in dgSearchACN.Items)
            {
                CheckBox chk = (CheckBox)drv.FindControl("chka_AddPDF");

                chk.Checked = chk2.Checked;
            }
        }

        protected void btnDossierSearch_Click(object sender, EventArgs e)
        {
            lblDataToday.Visible = true;


            string ValCookie = string.Empty;
            string PDFCookie = string.Empty;
            string DelCookie = string.Empty;

            HttpCookie cookie = Request.Cookies["CookieinsigteSys"];
            String Artigos = "";
            foreach (DataGridItem drv in dgSearchACN.Items)
            {
                CheckBox chk = (CheckBox)drv.FindControl("chka_AddPDF");
                ImageButton btn = (ImageButton)drv.FindControl("ImageButton1");
                if (chk.Checked)
                {
                    Artigos += chk.Attributes["NewsId"].ToString() + "|";
                    btn.ImageUrl = "Imgs/icons/black/png/book_side_icon_16_sel.png";
                    chk.Checked = false;
                }
            }

            Artigos = Artigos.TrimEnd('|');

            if (cookie.Values["PDF"] != "")
            {
                PDFCookie = cookie.Values["PDF"] + "|" + Artigos;
            }
            else
            {
                PDFCookie = Artigos;
            }

            DelCookie = cookie.Values["DELNOT"];
            ValCookie = cookie.Values["ID"];

            cookie.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(cookie);

            HttpCookie newCookie = new HttpCookie("CookieinsigteSys");
            newCookie["Version"] = "JUL2012";
            newCookie["ID"] = ValCookie;
            newCookie["PDF"] = PDFCookie;
            newCookie["DELNOT"] = DelCookie;
            newCookie.Expires = DateTime.Now.AddDays(360);
            Response.Cookies.Add(newCookie);

            //drv.Cells[0].Text;

            lblDataToday.Text += " Artigos adicionados com sucesso!";

        }

        protected void OnConfirm(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "Yes")
            {
                //this.Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('You clicked YES!')", true);

                lblDataToday.Visible = true;


                string ValCookie = string.Empty;
                string PDFCookie = string.Empty;
                string DelCookie = string.Empty;

                HttpCookie cookie = Request.Cookies["CookieinsigteSys"];
                String Artigos = "";
                //DataTable omfg = (DataTable)this.dgSearchACN.DataSource;
                //DataView dv = omfg.DefaultView;
                foreach (DataRow drv in Global.InsigteManager[Session.SessionID].publuicacoes.Rows)
                {
                    Artigos += drv["IDMETA"].ToString() + "|";
                }


                Artigos = Artigos.TrimEnd('|');

                if (cookie.Values["PDF"] != "")
                {
                    PDFCookie = cookie.Values["PDF"] + "|" + Artigos;
                }
                else
                {
                    PDFCookie = Artigos;
                }

                DelCookie = cookie.Values["DELNOT"];
                ValCookie = cookie.Values["ID"];

                cookie.Expires = DateTime.Now.AddDays(-1d);
                Response.Cookies.Add(cookie);

                HttpCookie newCookie = new HttpCookie("CookieinsigteSys");
                newCookie["Version"] = "JUL2012";
                newCookie["ID"] = ValCookie;
                newCookie["PDF"] = PDFCookie;
                newCookie["DELNOT"] = DelCookie;
                newCookie.Expires = DateTime.Now.AddDays(360);
                Response.Cookies.Add(newCookie);

                //drv.Cells[0].Text;




                foreach (DataGridItem drv1 in dgSearchACN.Items)
                {
                    CheckBox chk = (CheckBox)drv1.FindControl("chka_AddPDF");
                    ImageButton btn = (ImageButton)drv1.FindControl("ImageButton1");
                    btn.ImageUrl = "Imgs/icons/black/png/book_side_icon_16_sel.png";
                    chk.Checked = false;
                }

                lblDataToday.Text += " Artigos adicionados com sucesso!";



            }
            else
            {
                //this.Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('You clicked NO!')", true);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using AjaxControlToolkit;
using INInsigteManager;
using System.Net;
using System.Xml.Linq;


namespace Insigte
{
    public partial class publist : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["Report"] = "/Publicidade/RL_Publicidade_Menu";
            //Global.InsigteManager[Session.SessionID].inUser.RSVLastReport = "/Publicidade/RL_Publicidade_Menu";

            if (Global.InsigteManager.ContainsKey(Session.SessionID) == false)
            {
                INManager IN = new INManager(Session.SessionID, GetVisitorIpAddress(), GetLanIPAddress());
                Global.InsigteManager.Add(Session.SessionID, IN);
            }

            if (!Global.InsigteManager[Session.SessionID].IsLogged || Global.InsigteManager[Session.SessionID] == null)
            {
                Response.Redirect("login.aspx", true);
            }

            dgPub.PagerStyle.BackColor = System.Drawing.Color.FromName(Global.InsigteManager[Session.SessionID].inUser.Cores[0].ToString());
            dgPub.HeaderStyle.BackColor = System.Drawing.Color.FromName(Global.InsigteManager[Session.SessionID].inUser.Cores[1].ToString());

            if (!Page.IsPostBack)
            {
                fillST();
                fillDD();
                fillCP();
                getPub();
            }

        }

        protected void btnReport_Click(object sender, EventArgs e)
        {
            Session["Report"] = "/Publicidade/RL_Publicidade_Menu";
        }

        protected void btnpesquisa_Click(object sender, EventArgs e)
        {
            getPub();
        }

        protected void fillST()
        {
            String SQL = "";
            SQL += " select distinct HIER_LEVEL_1";
            SQL += " from ADVERTISING.dbo.AD10H_ADVERTISER aa with(nolock)";
            SQL += " join iadvisers.dbo.CL10H_CLIENT_ADVERTISER ca with(nolock)";
            SQL += " on ca.ID_ADVERTISER = aa.ID_ADVERTISER";
            SQL += " where ca.ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient + " and aa.HIER_LEVEL_1 is not null";
            SQL += " order by HIER_LEVEL_1";

            DataTable str = Global.InsigteManager[Session.SessionID].getTableQuery(SQL, "getPubSTR");

            ListItem x = new ListItem("Todos", "-1");
            ddStr.Items.Add(x);
            foreach (DataRow row in str.Rows)
            {
                ddStr.Items.Add(new ListItem(row["HIER_LEVEL_1"].ToString(), row["HIER_LEVEL_1"].ToString()));
            }
        }

        protected void fillDD()
        {
            try
            {
                String SQL = "";
                SQL += " select aa.ID_ADVERTISER,aa.DES_ADVERTISER ";
                SQL += " from ADVERTISING.dbo.AD10H_ADVERTISER aa with(nolock)";
                SQL += " join iadvisers.dbo.CL10H_CLIENT_ADVERTISER ca with(nolock)";
                SQL += " on ca.ID_ADVERTISER = aa.ID_ADVERTISER";
                SQL += " where ca.ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient + " and aa.HIER_LEVEL_1 is not null";

                if (ddStr.SelectedValue != "-1")
                {
                    SQL += " and aa.HIER_LEVEL_1 = '" + ddStr.SelectedValue + "' ";
                }

                SQL += " order by aa.DES_ADVERTISER ";

                DataTable adv = Global.InsigteManager[Session.SessionID].getTableQuery(SQL, "getPubADV");

                ListItem x = new ListItem("Todos", "-1");
                ddAdv.Items.Clear();
                ddAdv.Items.Add(x);
                foreach (DataRow row in adv.Rows)
                {
                    ddAdv.Items.Add(new ListItem(row["DES_ADVERTISER"].ToString(), row["ID_ADVERTISER"].ToString()));
                }
            }
            catch (Exception er)
            {
                er.Message.ToString();
            }
        }

        protected void fillCP()
        {
            try
            {


                String SQL = "";
                SQL += " select cp.ID_CAMPANHA, cp.NOM_CAMPANHA";
                SQL += " from ADVERTISING.dbo.AD10D_CAMPANHA cp with(nolock)";
                SQL += " join ADVERTISING.dbo.AD10H_ADVERTISER aa with(nolock)";
                SQL += " on cp.ID_ADVERTISER = aa.ID_ADVERTISER";
                SQL += " join iadvisers.dbo.CL10H_CLIENT_ADVERTISER ca with(nolock)";
                SQL += " on ca.ID_ADVERTISER = aa.ID_ADVERTISER";
                SQL += " where ca.ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient;

                if (ddStr.SelectedValue != "-1")
                {
                    SQL += " and aa.HIER_LEVEL_1 = '" + ddStr.SelectedValue + "' ";
                }

                if (ddAdv.SelectedValue != "-1")
                {
                    SQL += " and cp.ID_ADVERTISER = '" + ddAdv.SelectedValue + "' ";
                }

                SQL += "order by cp.NOM_CAMPANHA ";

                DataTable cpn = Global.InsigteManager[Session.SessionID].getTableQuery(SQL, "getPubCPN");

                ListItem x = new ListItem("Todas", "-1");
                ddCpn.Items.Clear();
                ddCpn.Items.Add(x);
                foreach (DataRow row in cpn.Rows)
                {
                    ddCpn.Items.Add(new ListItem(row["NOM_CAMPANHA"].ToString(), row["ID_CAMPANHA"].ToString()));
                }
            }
            catch (Exception er)
            {
                er.Message.ToString();
            }
        }

        protected void getPub()
        {

            String SQL = "";

            SQL += " select * from ADVERTISING.dbo.[vWSpotsPortal] as vw where 1= 1 and vw.country = 'Angola' ";

            if (ddStr.SelectedValue == "-1")
            {

                SQL += " and  vw.Sector in ( ";
                SQL += " select aa.HIER_LEVEL_1 ";
                //SQL += " from ADVERTISING.dbo.AD10D_CAMPANHA cp with(nolock)";
                SQL += " from ADVERTISING.dbo.AD10H_ADVERTISER aa with(nolock)";
                //SQL += " on cp.ID_ADVERTISER = aa.ID_ADVERTISER";
                SQL += " join iadvisers.dbo.CL10H_CLIENT_ADVERTISER ca with(nolock)";
                SQL += " on ca.ID_ADVERTISER = aa.ID_ADVERTISER";
                SQL += " where ca.ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient + " ) ";
            }
            else
            {
                SQL += " and  vw.Sector in ( '" + ddStr.SelectedValue + "')";
            }

            if (ddAdv.SelectedValue != "-1")
            {
                SQL += " and vw.IdAdvertiser = " + ddAdv.SelectedValue + " ";
            }

            //if(ddStr.SelectedValue !)

            //if (ddCpn.SelectedValue != "-1")
            //{
            //    SQL += " and h.ID_CAMPANHA = " + ddCpn.SelectedValue + " ";
            //}

            if (TextBox1.Text.Length > 0)
            {
                SQL += " and vw.Date between '" + TextBox1.Text + "' and '" + TextBox2.Text + "'";
            }
            else
            {
                SQL += " and convert(varchar,vw.Date,112) >= convert(varchar,getdate()-90,112) ";
            }

            //SQL += " AND ( f.ID_SUPORTE_IMPRENSA_FA <> -1 or f.ID_PROGRAMA >0 ) ";
            SQL += "  order by vw.DATe desc";

            #region SQL
            //Lb_Tema.Text = SQL;
            //SQL += "select	distinct f.ID_F_SPOT ";
            //SQL += "	,st.DES_SPOT_TYPE  ";
            //SQL += "    ,ad.DES_ADVERTISER  ";
            //SQL += "    ,s.DES_SECTOR  ";
            //SQL += "    ,isnull(p.DES_PRODUCT,'-1') DES_PRODUCT  ";
            //SQL += "    ,CASE WHEN h.ID_SPOT_SUPORTE = 2 THEN e.name + '/' + prg.DES_PROGRAM ELSE e.name END AS DES_EDITOR  ";
            //SQL += "    ,h.ID_SPOT_SUPORTE  ";
            //SQL += "    ,cmb.DES_VAL_DES + cast( cast((isnull(fa.VAL_CUSTO, ct.VAL_CUSTO) * cmb.VAL_CAMBIO) as numeric (18,2))  as varchar) as VALOR  ";
            //SQL += "    ,isnull(fa.DES_VAL, ct.DES_VAL) + cast(isnull(fa.VAL_CUSTO, ct.VAL_CUSTO) as varchar) as VALOR_ORIGINAL  ";
            //SQL += "    ,h.ID_SPOT, '&nbsp;' + h.DES_SPOT DES_SPOT, h.ID_SPOT_TYPE, h.ID_CREATIVE_AGENCY, h.ID_MEDIA_AGENCY, h.ID_ADVERTISER, h.ID_SECTOR, h.ID_PRODUCT, h.COD_FILE, h.TXT_SLOGAN, h.VAL_LITERAL, h.ID_FORMATO  ";
            //SQL += "    ,f.ID_EDITOR, f.ID_SUPORTE_IMPRENSA_FA  ";
            //SQL += "    ,CASE WHEN h.ID_SPOT_SUPORTE = 2 THEN CONVERT(varchar, f.DAT_SPOT, 108) ELSE f.NUM_PAGINA END AS NUM_PAGINA  ";
            //SQL += "    ,f.TXT_TECHNICAL_CONDITIONS, f.DAT_SPOT, f.DAT_CREATION  ";
            //SQL += "from ADVERTISING.dbo.AD10F_SPOT f with(nolock) ";
            //SQL += "join ADVERTISING.dbo.AD10H_SPOT h with(nolock) on h.ID_SPOT = f.ID_SPOT ";
            //SQL += "join iadvisers.dbo.CL10H_CLIENT_ADVERTISER wtf with(nolock) on wtf.ID_ADVERTISER = h.ID_ADVERTISER ";
            //SQL += "left join ADVERTISING.dbo.AD10D_CAMPANHA cp with(nolock) on cp.ID_CAMPANHA = h.ID_CAMPANHA  ";
            //SQL += "left join ADVERTISING.dbo.AD10D_SPOT_TYPE st with(nolock) on st.ID_SPOT_TYPE = h.ID_SPOT_TYPE ";
            //SQL += "left join iadvisers.dbo.editors e with(nolock) on e.editorid = f.ID_EDITOR  ";
            //SQL += "left join ADVERTISING.dbo.AD10D_PROGRAM as prg with(nolock) on prg.ID_PROGRAM = f.ID_PROGRAMA  ";
            //SQL += "join ADVERTISING.dbo.AD10H_ADVERTISER ad with(nolock) on ad.ID_ADVERTISER = h.ID_ADVERTISER  ";
            //SQL += "left join ADVERTISING.dbo.AD10H_SUPORTE_IMPRENSA_FA fa with(nolock) on fa.ID_SUPORTE_IMPRENSA_FA = f.ID_SUPORTE_IMPRENSA_FA and fa.ID_EDITOR = f.ID_EDITOR ";
            //SQL += "left join ADVERTISING.dbo.AD10H_GRELHA_DE_PROGRAMACAO gp with(nolock) ";
            //SQL += "	on gp.ID_PROGRAMA = f.ID_PROGRAMA and gp.NUM_BREAK  = f.NUM_BREAK  ";
            //SQL += "	and gp.WEEK_DAY = DATEPART(DW,f.DAT_SPOT)  ";
            //SQL += "    and CONVERT(varchar, f.DAT_SPOT, 108) BETWEEN "; 
            //SQL += "		( CASE WHEN CONVERT(varchar, gp.HOR_INICIO_PROGRAMA, 108) < '00:30:00' then  ";
            //SQL += "			CONVERT(varchar, gp.HOR_INICIO_PROGRAMA, 108)  ";
            //SQL += "			else CONVERT(varchar, dateadd(mi,-30,gp.HOR_INICIO_PROGRAMA), 108) end )  ";
            //SQL += "	and ( CASE WHEN CONVERT(varchar, gp.HOR_FIM_PROGRAMA, 108) > '23:29:59'  ";
            //SQL += "			then CONVERT(varchar, gp.HOR_FIM_PROGRAMA, 108)  ";
            //SQL += "			else CONVERT(varchar, dateadd(mi,30,gp.HOR_FIM_PROGRAMA), 108) end )  ";
            //SQL += "	and gp.COD_CLASSIFICACAO <> 'T00' ";
            //SQL += "left join ADVERTISING.dbo.AD10D_CLASSIFICACAO_TEMPO ct with(nolock) ";
            //SQL += "	on gp.COD_CLASSIFICACAO = ct.COD_CLASSIFICACAO  ";
            //SQL += "	and gp.ID_EDITOR = ct.ID_EDITOR  ";
            //SQL += "	and h.VAL_DURACAO between ct.VAL_TEMPO_MIN and ct.VAL_TEMPO_MAX  ";
            //SQL += "left join iadvisers..BI10X_CAMBIO cmb with(nolock)  ";
            //SQL += "	on isnull(fa.DES_VAL, ct.DES_VAL) = cmb.DES_VAL_ORI  ";
            ////SQL += "left join ADVERTISING.dbo.AD10H_SECTOR s with(nolock) on s.ID_SECTOR = h.ID_SECTOR  ";
            //SQL += "left join ADVERTISING.dbo.AD10D_PRODUCT p with(nolock) on p.ID_PRODUCT = h.ID_PRODUCT  ";

            //SQL += " where wtf.ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient + " ";



            //if (ddAdv.SelectedValue != "-1")
            //{
            //    SQL += " and h.ID_ADVERTISER = " + ddAdv.SelectedValue + " ";
            //}

            ////if (ddCpn.SelectedValue != "-1")
            ////{
            ////    SQL += " and h.ID_CAMPANHA = " + ddCpn.SelectedValue + " ";
            ////}

            //if (TextBox1.Text.Length > 0)
            //{
            //    SQL += " and f.DAT_SPOT between '" + TextBox1.Text + "' and '" + TextBox2.Text + "'";
            //}
            //else
            //{
            //    SQL += "    and convert(varchar,f.DAT_SPOT,112) >= convert(varchar,getdate()-30,112) ";
            //}

            //SQL += " AND ( f.ID_SUPORTE_IMPRENSA_FA <> -1 or f.ID_PROGRAMA >0 ) ";
            //SQL += "  order by f.DAT_SPOT desc, f.DAT_CREATION desc ";

            //Lb_Tema.Text = SQL;
            #endregion

            DataTable pub = Global.InsigteManager[Session.SessionID].getTableQuery(SQL, "getPub");

            dgPub.DataSource = pub;
            dgPub.DataBind();
            dgPub.Visible = true;

        }

        protected void btnExportXML_Click(object sender, EventArgs e)
        {
        }

        protected void dgPub_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgPub.CurrentPageIndex = e.NewPageIndex;
            getPub();
        }


        protected void ddStr_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillDD();
            fillCP();
            dgPub.CurrentPageIndex = 0;
            getPub();
        }

        protected void ddAdv_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillCP();
            dgPub.CurrentPageIndex = 0;
            getPub();
        }

        protected void ddCpn_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgPub.CurrentPageIndex = 0;
            getPub();
        }

        protected string sReturnIconTipoPasta(string type, string url, string file)
        {
            String tipo = "";

            switch (type)
            {
                case "1":
                    tipo = "Imprensa";
                    break;
                case "2":
                    tipo = "Televisão";
                    break;
                default:
                    tipo = "Imprensa";
                    break;
            }

            switch (tipo)
            {
                case "Imprensa":
                    String FileHelperPath = "ficheiros";
                    if (Global.InsigteManager[Session.SessionID].CostumPDFLogo == "1")
                    {
                        if (File.Exists(@"G:\cfiles\" + Global.InsigteManager[Session.SessionID].IdClient + "\\" + file.Replace("/", "\\")))
                        {
                            FileHelperPath = "cficheiros/" + Global.InsigteManager[Session.SessionID].IdClient;
                        }
                    }
                    return string.Format("<a href=\"http://insigte.com/" + FileHelperPath + "/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/doc_export_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download PDF\" style=\"border-width:0px;\" title=\"Download PDF\" /></a>", file);

                case "Online":
                    return string.Format("<a href=\"{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/globe_1_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Link\" style=\"border-width:0px;\" title=\"Link\" /></a>", url);
                case "Rádio":
                    return string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/headphones_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download MP3\" style=\"border-width:0px;\" title=\"Download MP3\" /></a>", file);
                case "Televisão":
                    return string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/movie_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download MP4\" style=\"border-width:0px;\" title=\"Download MP4\" /></a>", file);
                default:
                    return string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/doc_export_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download PDF\" style=\"border-width:0px;\" title=\"Download PDF\" /></a>", file);
            }
        }

        protected String getResource(String resx)
        {
            return Resources.insigte.language.ResourceManager.GetObject(resx).ToString();
        }

        public string GetVisitorIpAddress()
        {
            string stringIpAddress;
            stringIpAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (stringIpAddress == null) //may be the HTTP_X_FORWARDED_FOR is null
            {
                stringIpAddress = Request.ServerVariables["REMOTE_ADDR"];//we can use REMOTE_ADDR
            }
            return stringIpAddress;
        }

        //Get Lan Connected IP address method
        public string GetLanIPAddress()
        {
            //Get the Host Name
            string stringHostName = Dns.GetHostName();
            //Get The Ip Host Entry
            IPHostEntry ipHostEntries = Dns.GetHostEntry(stringHostName);
            //Get The Ip Address From The Ip Host Entry Address List
            IPAddress[] arrIpAddress = ipHostEntries.AddressList;
            return arrIpAddress[arrIpAddress.Length - 1].ToString();
        }
    }
}
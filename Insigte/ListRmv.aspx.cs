﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using APToolkitNET;

namespace Insigte
{
    public partial class ListRmv : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(0, 1) == "0")
            {
                Response.Redirect("Default.aspx");
            }
            if (!Page.IsPostBack)
            {
                string qsIDPasta = Request.QueryString["PID"];

                lbPasta.Text = qsIDPasta;

                getPasta(qsIDPasta);
            }
        }


        public void getPasta(string pasta)
        {
            DataTable lists = Global.InsigteManager[Session.SessionID].getTableQuery("select * from CL10D_LISTS with(nolock) where id_client = " + Global.InsigteManager[Session.SessionID].IdClient + " and id_client_user = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + " and ID_LIST = " + pasta, "getList");

            foreach (DataRow dr in lists.Rows)
            {
                lbPasta.Text = "Deseja mesmo apagar a Lista : " + dr["NOM_LIST"].ToString() + " (" + dr["DES_LIST"].ToString() + ") e todos os autores guardados?";
            }

        }

        protected void btApagar_Click(object sender, EventArgs e)
        {
            string qsIDPasta = Request.QueryString["PID"];

            string cmdQueryPastas = " delete from dbo.CL10D_LISTS where id_list = " + qsIDPasta + " and id_client = " + Global.InsigteManager[Session.SessionID].IdClient + " and id_client_user = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + " ";
            string cmdQueryArticles = " delete from dbo.CL10H_LIST_AUTOR where ID_LISTS = " + qsIDPasta + " and id_client = " + Global.InsigteManager[Session.SessionID].IdClient + " and id_client_user = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + " ";

            DataTable dllists = Global.InsigteManager[Session.SessionID].getTableQuery(cmdQueryPastas, "dllists");
            DataTable dllistsautors = Global.InsigteManager[Session.SessionID].getTableQuery(cmdQueryArticles, "dllistsautors");

            Response.Redirect("listscfg.aspx");

            //Response.Write("<script language='javascript'> { self.close() }</script>");

        }

        protected void cancelar_click(object sender, EventArgs e)
        {
            Response.Redirect("listscfg.aspx");
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Web.Services;

namespace Insigte
{
    public partial class FbNews : BasePage
    {
        string qsFb = "";
        string commentId = "";
        bool mostrar = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            //dgFbNews.PagerStyle.BackColor = System.Drawing.Color.FromName(Global.InsigteManager[Session.SessionID].inUser.Cores[0].ToString());
            //dgFbNews.HeaderStyle.BackColor = System.Drawing.Color.FromName(Global.InsigteManager[Session.SessionID].inUser.Cores[1].ToString());


            qsFb = Request.QueryString["fb"];
            //lblDataToday.Visible = true;
            
            if (!Page.IsPostBack)
            {
                getNoticiaOnline();
            }

        }

        // Declare an Age property of type int:
        public bool mostrarTrueFalse()
        {
                return mostrar;
        }


        protected void btn_comentarios_click(object sender, EventArgs e)
        {
           
            var btnclick = (LinkButton)sender;

            foreach (RepeaterItem x in rpt_Fb_posts.Items)
            {
                HtmlGenericControl divs = (HtmlGenericControl)x.FindControl("_divComents");
                if (divs.Attributes["postid"].ToString() == btnclick.CommandArgument.ToString())
                {
                    divs.Visible = divs.Visible == true ? false : true;
                    lblDataToday.Text = divs.Attributes["postid"].ToString();
                }
            }
            
            
            mostrar = true;                
        }

        [WebMethod]
        public static string ProcessIT(string teste)
        {
            string result = teste;
            return result;
        }

        protected string mostralikes(string t)
        {
            
            //var btnclick = (LinkButton)sender;
           
            //lblDataToday.Text = t.ToString(); //btnclick.CommandArgument.ToString();
            return t.ToString(); //btnclick.CommandArgument.ToString();
        }

        protected string mostraComent()
        {
            //if (!mostrar)
            //    return "";


            string query = " select * from [FB10D_POST_COMENT] with(nolock) where fb_id_news_posts = " + Eval("fb_id_news_posts").ToString();

            DataTable comments = Global.InsigteManager[Session.SessionID].getTableQuery(query, "GetNews");

            if (comments.Rows.Count == 0) //verifica se tem tokens
                return "";
            string teste = "<div style='margin-left:5px; float:left;' >";
            teste += "<div style='margin-left:5px; margin-bottom: 5px; font-size:13px; color:black; font-weight:bold;' >Comentarios</div>";           
            int i = 0;
            
            foreach (DataRow r in comments.Rows)
            {
                i++;
                teste += "<div class='ui-widget-contentrpt2' >";
                teste += "<div style='margin-left:10px; margin-top: 10px; margin-bottom:10px; color:black;' >";
                teste += r["fb_nome_user_coment"].ToString() + "<br />";
                //teste += "comentario #" + i.ToString() + " <br /> ";
                teste += r["fb_coment_create_time"].ToString() + "<br />";
                teste += "</div> ";
                teste += "<div class='MessagemComents' >";
                teste += r["fb_msg_user_coment"].ToString();
                teste += "</div>";
                teste += "<div style='float:right; margin:5px;'>";
                teste += "</div>";
                teste += "</div>";   
            }
            teste += "</div>";
            return teste;
        }

        //protected void rpt_Fb_posts_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        //{
        //    var _repeter = (Repeater)e.Item.FindControl("rpt_Fb_posts_comments");
        //    var _lbl_aux = (Label)e.Item.FindControl("lbl_fb_id");

        //    commentId = _lbl_aux.Text;

        //    lblDataToday.Text += "<br>" + commentId.ToString() + "<br>";
        //    lblDataToday.Visible = true;
        //    string query = " select * from [FB10D_POST_COMENT] where fb_id_news_posts = " + commentId.ToString();
        //    DataTable comments = Global.InsigteManager[Session.SessionID].getTableQuery(query, "GetNews");

        //    _repeter.DataSource = comments;
        //    _repeter.DataBind();
        //}

        protected void getNoticiaOnline()
        {
            
            string SQLNEWSONLINE = "";
            try
            {
                SQLNEWSONLINE = " select * " ;// m.id,fbn.fb_id_news_posts,fbn.fb_id_post_user_name m.title, m.title_en, fbn.fb_updated_time, fbn.fb_coments_val, fbn.fb_likes_val, fbn.fb_shares_val, fbn.fb_post_url ";
                SQLNEWSONLINE += " from metadata m with(nolock) inner join [FB10F_NEWS_POST] fbn with(nolock) ";
                SQLNEWSONLINE += " on m.id = fbn.id_noticia ";
                SQLNEWSONLINE += " where fbn.id_noticia = " + qsFb;
                //SQLNEWSONLINE += " order by fbn.fb_updated_time desc " ;

               
                DataTable news = Global.InsigteManager[Session.SessionID].getTableQuery(SQLNEWSONLINE, "GetFbNews");
                //lbl_titulo.Text =" teste : " +news.Rows[0]["title"].ToString();
                foreach (DataRow r in news.Rows)
                {
                    lbl_titulo.Text = r["title"].ToString();
                }

                rpt_Fb_posts.DataSource = news;               
                rpt_Fb_posts.DataBind();               
                rpt_Fb_posts.Visible = true;   
            }
            catch (Exception er)
            {
                lblDataToday.Visible = true;
                lblDataToday.Text += "<br> erro getfbnews:" + er.Message.ToString();
                lblDataToday.Text += "query"+ SQLNEWSONLINE.ToString();
                
            }
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using AjaxControlToolkit;

namespace Insigte
{
    public partial class AdvSearch : BasePage
    {
        private DataTable Editors;
        private DataTable Autores;
        private DataTable Classes;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Global.InsigteManager[Session.SessionID].IsLogged || Global.InsigteManager[Session.SessionID] == null)
            {
                Response.Redirect("login.aspx", true);
            }

            //imgArrowAdvSearch0.ImageUrl = "Cli/" + Global.InsigteManager[Session.SessionID].CodCliente + "/red-arrow.gif";
            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(2, 1) == "0")
            {
                Response.Redirect("Default.aspx");
            }

            getDataEditors();

            if (!Page.IsPostBack)
            {
                ta_jornalists_codes.Value = Resources.insigte.language.geralTagTodos;
                ta_editores_codes.Value = Resources.insigte.language.geralTagTodos;

                tbSearchAdvance.Text = Resources.insigte.language.advStxtPesquisa;
                tbSearchAdvance.Attributes.Add("onclick", "clearSearch(this.value);this.style.fontStyle='normal';");

                getDataEditors();
                getDataAutor();
                getDataClasses();
                popEditors();
                popAutores();
                popTemas();
                popClasses();
                popDDSearch();

                //Fabio 20150116 
                //OLD - if (Global.InsigteManager[Session.SessionID].IdClient == "1002" )
                /****new****/
                //if (Global.InsigteManager[Session.SessionID].IdClient == "1002" && Global.InsigteManager[Session.SessionID].inUser.IdClientUser != "9")
                ///***********/
                //{
                //    DropDownCountry.Visible = false;
                //    LbCountry.Visible = false;
                //}
            }
        }

        protected String getResource(String resx)
        {
            return Resources.insigte.language.ResourceManager.GetObject(resx).ToString();
        }

        protected void popDDSearch()
        {
            DataTable temp = Global.InsigteManager[Session.SessionID].getTableQuery("Select * from CL10H_CLIENT_SEARCH with(nolock) where ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient + " and ID_CLIENT_USER = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser, "Search");

            foreach (DataRow srch in temp.Rows)
            {
                ListItem i = new ListItem(srch["DES_CLIENT_SEARCH"].ToString(), srch["ID_CLIENT_SEARCH"].ToString());
                DDSearchs.Items.Add(i);
                if (Session["ADVSearchLastSelected"] != null && srch["DES_CLIENT_SEARCH"].ToString() == Session["ADVSearchLastSelected"].ToString())
                {
                    DDSearchs.SelectedValue = i.Value;
                    getSelected(null, null);
                }
            }

        }

        protected void popClasses()
        {
            foreach (DataRow x in Classes.Rows)
            {
                DropDownSubject.Items.Add(new ListItem(x["subject"].ToString(), x["subject"].ToString()));
            }
        }

        protected void popTemas()
        {
            foreach (KeyValuePair<int, INinsigteManager.UserTemas> pair in Global.InsigteManager[Session.SessionID].inUser.Temas)
            {
                DDL_Temas.Items.Add(new ListItem(pair.Value.Name, pair.Value.IdTema + pair.Value.TabOrder.ToString()));
            }
        }

        protected void popEditors()
        {
            LbEditors.Items.Add(Resources.insigte.language.geralTagTodos);

            foreach (DataRow x in Editors.Rows)
            {
                switch (x["country"].ToString())
                {
                    case "Portugal":
                        LbEditorsPT.Items.Add(x["name"].ToString());
                        break;
                    case "Angola":
                        LbEditorsAO.Items.Add(x["name"].ToString());
                        break;
                    case "Moçambique":
                        LbEditorsMZ.Items.Add(x["name"].ToString());
                        break;
                    default:
                        LbEditorsOU.Items.Add(x["name"].ToString());
                        break;
                }
                LbEditors.Items.Add(x["name"].ToString());
            }
        }

        protected void popEditorsNew()
        {
            LbEditors.Items.Add(Resources.insigte.language.geralTagTodos);

            foreach (DataRow x in Editors.Rows)
            {
                switch (x["country"].ToString())
                {
                    case "Portugal":
                        LbEditorsPT.Items.Add(new ListItem { Text = x["name"].ToString(), Value = x["editorid"].ToString() });
                        break;
                    case "Angola":
                        LbEditorsAO.Items.Add(new ListItem { Text = x["name"].ToString(), Value = x["editorid"].ToString() });
                        break;
                    case "Moçambique":
                        LbEditorsMZ.Items.Add(new ListItem { Text = x["name"].ToString(), Value = x["editorid"].ToString() });
                        break;
                    default:
                        LbEditorsOU.Items.Add(new ListItem { Text = x["name"].ToString(), Value = x["editorid"].ToString() });
                        break;
                }
                LbEditors.Items.Add(x["name"].ToString());
            }
        }

        protected void popAutores()
        {

            LbJornalistAD.Items.Add(Resources.insigte.language.geralTagTodos);
            LbJornalistEH.Items.Add(Resources.insigte.language.geralTagTodos);
            LbJornalistIL.Items.Add(Resources.insigte.language.geralTagTodos);
            LbJornalistMP.Items.Add(Resources.insigte.language.geralTagTodos);
            LbJornalistQT.Items.Add(Resources.insigte.language.geralTagTodos);
            LbJornalistUX.Items.Add(Resources.insigte.language.geralTagTodos);
            LbJornalistYN.Items.Add(Resources.insigte.language.geralTagTodos);

            foreach (DataRow x in Autores.Rows)
            {
                if (x["name"].ToString().Length > 0)
                {
                    switch (x["name"].ToString().Substring(0, 1).ToUpper().Replace("Á", "A").Replace("Â", "A").Replace("Ó", "A"))
                    {
                        case "A":
                        case "B":
                        case "C":
                        case "D":
                            LbJornalistAD.Items.Add(x["name"].ToString());
                            break;
                        case "E":
                        case "F":
                        case "G":
                        case "H":
                            LbJornalistEH.Items.Add(x["name"].ToString());
                            break;
                        case "I":
                        case "J":
                        case "K":
                        case "L":
                            LbJornalistIL.Items.Add(x["name"].ToString());
                            break;
                        case "M":
                        case "N":
                        case "O":
                        case "P":
                            LbJornalistMP.Items.Add(x["name"].ToString());
                            break;
                        case "Q":
                        case "R":
                        case "S":
                        case "T":
                            LbJornalistQT.Items.Add(x["name"].ToString());
                            break;
                        case "U":
                        case "V":
                        case "W":
                        case "X":
                            LbJornalistUX.Items.Add(x["name"].ToString());
                            break;
                        case "Y":
                        case "Z":
                            LbJornalistYN.Items.Add(x["name"].ToString());
                            break;
                        default:
                            LbJornalistYN.Items.Add(x["name"].ToString());
                            break;
                    }
                }
            }
        }

        protected void popAutoresNew()
        {

            LbJornalistAD.Items.Add(Resources.insigte.language.geralTagTodos);
            LbJornalistEH.Items.Add(Resources.insigte.language.geralTagTodos);
            LbJornalistIL.Items.Add(Resources.insigte.language.geralTagTodos);
            LbJornalistMP.Items.Add(Resources.insigte.language.geralTagTodos);
            LbJornalistQT.Items.Add(Resources.insigte.language.geralTagTodos);
            LbJornalistUX.Items.Add(Resources.insigte.language.geralTagTodos);
            LbJornalistYN.Items.Add(Resources.insigte.language.geralTagTodos);

            foreach (DataRow x in Autores.Rows)
            {
                if (x["name"].ToString().Length > 0)
                {
                    switch (x["name"].ToString().Substring(0, 1).ToUpper().Replace("Á", "A").Replace("Â", "A").Replace("Ó", "A"))
                    {
                        case "A":
                        case "B":
                        case "C":
                        case "D":
                            LbJornalistAD.Items.Add(new ListItem { Text = x["name"].ToString(), Value = x["authorid"].ToString() });
                            break;
                        case "E":
                        case "F":
                        case "G":
                        case "H":
                            LbJornalistEH.Items.Add(new ListItem { Text = x["name"].ToString(), Value = x["authorid"].ToString() });
                            break;
                        case "I":
                        case "J":
                        case "K":
                        case "L":
                            LbJornalistIL.Items.Add(new ListItem { Text = x["name"].ToString(), Value = x["authorid"].ToString() });
                            break;
                        case "M":
                        case "N":
                        case "O":
                        case "P":
                            LbJornalistMP.Items.Add(new ListItem { Text = x["name"].ToString(), Value = x["authorid"].ToString() });
                            break;
                        case "Q":
                        case "R":
                        case "S":
                        case "T":
                            LbJornalistQT.Items.Add(new ListItem { Text = x["name"].ToString(), Value = x["authorid"].ToString() });
                            break;
                        case "U":
                        case "V":
                        case "W":
                        case "X":
                            LbJornalistUX.Items.Add(new ListItem { Text = x["name"].ToString(), Value = x["authorid"].ToString() });
                            break;
                        case "Y":
                        case "Z":
                            LbJornalistYN.Items.Add(new ListItem { Text = x["name"].ToString(), Value = x["authorid"].ToString() });
                            break;
                        default:
                            LbJornalistYN.Items.Add(new ListItem { Text = x["name"].ToString(), Value = x["authorid"].ToString() });
                            break;
                    }
                }
            }
        }

        protected void getDataClasses()
        {
            String Subj = "subject";

            if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
                Subj += "_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage;

            String SQLquery = "select distinct " + Subj + " as subject from editors with(nolock) where len(isnull(" + Subj + ",'')) > 0 order by 1 desc ";
            Classes = new DataTable();
            Classes = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "Classes");
        }

        protected void getDataEditors()
        {
            String SQLquery = "select * from dbo.editors with(nolock) where len(isnull(name,'')) > 0 order by name asc ";
            if (Global.InsigteManager[Session.SessionID].inUser.UserPortalFilter != "*")
                SQLquery = "select * from dbo.editors with(nolock) where editorid in (" + Global.InsigteManager[Session.SessionID].inUser.UserPortalFilter + ") order by name asc ";
            Editors = new DataTable();
            Editors = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "FilterEditorsTemas");
        }

        protected void getDataAutor()
        {
            String SQLquery = "select * from dbo.authors with(nolock) where len(isnull(name,'')) > 0 order by name asc ";
            Autores = new DataTable();
            Autores = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "Autores");
        }

        protected void Pesquisar_Click(object sender, EventArgs e)
        {
            lnkPesquisa.Enabled = false;

            Session["ADVSearchBool"] = "true";
            Session["ADVSearchTxt"] = tbSearchAdvance.Text.Replace("'", "''");
            Session["ADVSearchTema"] = DDL_Temas.SelectedValue.ToString().Replace("'", "''");
            Session["ADVSearchPeriodo"] = DDL_Data.SelectedValue.ToString().Replace("'", "''");
            Session["ADVSearchDatIni"] = tbDateDe.Text.Replace("'", "''");
            Session["ADVSearchDatFim"] = tbDateAte.Text.Replace("'", "''");
            Session["ADVSearchEditor"] = ta_editores_codes.Value.Replace("'", "''");
            Session["ADVSearchJornalistas"] = ta_jornalists_codes.Value.Replace("'", "''");
            Session["ADVSearchTipo"] = DropDownMeio.SelectedValue.ToString().Replace("'", "''");
            Session["ADVSearchPais"] = DropDownCountry.SelectedValue.ToString().Replace("'", "''");
            Session["ADVSearchClasse"] = DropDownSubject.SelectedValue.ToString().Replace("'", "''");

            Response.Redirect("Search.aspx");
        }

        protected void Pesquisar_ClickNew(object sender, EventArgs e)
        {
            lnkPesquisa.Enabled = false;

            Session["ADVSearchBool"] = "true";
            Session["ADVSearchTxt"] = tbSearchAdvance.Text.Replace("'", "''");
            Session["ADVSearchTema"] = DDL_Temas.SelectedValue.ToString().Replace("'", "''");
            Session["ADVSearchPeriodo"] = DDL_Data.SelectedValue.ToString().Replace("'", "''");
            Session["ADVSearchDatIni"] = tbDateDe.Text.Replace("'", "''");
            Session["ADVSearchDatFim"] = tbDateAte.Text.Replace("'", "''");
            Session["ADVSearchEditor"] = ta_editores_codes.Value.Replace("'", "''");
            Session["ADVSearchJornalistas"] = ta_jornalists_codes.Value.Replace("'", "''");
            Session["ADVSearchTipo"] = DropDownMeio.SelectedValue.ToString().Replace("'", "''");
            Session["ADVSearchPais"] = DropDownCountry.SelectedValue.ToString().Replace("'", "''");
            Session["ADVSearchClasse"] = DropDownSubject.SelectedValue.ToString().Replace("'", "''");

            Response.Redirect("Search.aspx");
        }

        protected void Guardar_Click(object sender, EventArgs e)
        {
            //if (txt_aux_novo.Value.Trim().Length > 0 && DDSearchs.SelectedValue == "-1") para utilizar 
            if (txt_aux_novo.Value.Trim().Length > 0 && txt_aux_novo.Value != "Nova Pesquisa")
            {
                String SQLquery = " insert into CL10H_CLIENT_SEARCH ";
                SQLquery += " (ID_CLIENT, ID_CLIENT_USER, DES_CLIENT_SEARCH, COD_SEARCH_TEXT, COD_SEARCH_TEMA, COD_SEARCH_PERIODO, COD_SEARCH_DAT_INI, COD_SEARCH_DAT_FIM, COD_SEARCH_EDITOR, COD_SEARCH_JORNALIST, COD_SEARCH_TIPO, COD_SEARCH_PAIS, COD_SEARCH_CLASS) ";
                SQLquery += " select " + Global.InsigteManager[Session.SessionID].IdClient + "," + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + ", '" + txt_aux_novo.Value.Replace("'", "") + "', ";
                SQLquery += " '" + tbSearchAdvance.Text.Replace("'", "''") + "', '" + DDL_Temas.SelectedValue.ToString().Replace("'", "''") + "', '" + DDL_Data.SelectedValue.ToString().Replace("'", "''") + "', ";
                SQLquery += " '" + tbDateDe.Text.Replace("'", "''") + "', '" + tbDateAte.Text.Replace("'", "''") + "', '" + ta_editores_codes.Value.Replace("'", "''") + "', '" + ta_jornalists_codes.Value.Replace("'", "''") + "', ";
                SQLquery += " '" + DropDownMeio.SelectedValue.ToString().Replace("'", "''") + "', '" + DropDownCountry.SelectedValue.ToString().Replace("'", "''") + "', '" + DropDownSubject.SelectedValue.ToString().Replace("'", "''") + "' ";

                DataTable Save = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "Save");

                Session["ADVSearchLastSelected"] = txt_aux_novo.Value.Replace("'", "");
                Response.Redirect("AdvSearch.aspx");

            }
            else
            {
                lblDataToday.Visible = true;
                lblDataToday.Text = "É necessário atribuir um nome válido!";
            }
        }

        protected void Guardar_ClickNew(object sender, EventArgs e)
        {
            //if (txt_aux_novo.Value.Trim().Length > 0 && DDSearchs.SelectedValue == "-1") para utilizar 
            if (txt_aux_novo.Value.Trim().Length > 0 && txt_aux_novo.Value != "Nova Pesquisa")
            {
                String SQLquery = " insert into CL10H_CLIENT_SEARCH ";
                SQLquery += " (ID_CLIENT, ID_CLIENT_USER, DES_CLIENT_SEARCH, COD_SEARCH_TEXT, COD_SEARCH_TEMA, COD_SEARCH_PERIODO, COD_SEARCH_DAT_INI, COD_SEARCH_DAT_FIM, COD_SEARCH_EDITOR, COD_SEARCH_JORNALIST, COD_SEARCH_TIPO, COD_SEARCH_PAIS, COD_SEARCH_CLASS) ";
                SQLquery += " select " + Global.InsigteManager[Session.SessionID].IdClient + "," + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + ", '" + txt_aux_novo.Value.Replace("'", "") + "', ";
                SQLquery += " '" + tbSearchAdvance.Text.Replace("'", "''") + "', '" + DDL_Temas.SelectedValue.ToString().Replace("'", "''") + "', '" + DDL_Data.SelectedValue.ToString().Replace("'", "''") + "', ";
                SQLquery += " '" + tbDateDe.Text.Replace("'", "''") + "', '" + tbDateAte.Text.Replace("'", "''") + "', '" + ta_editores_codes.Value.Replace("'", "''") + "', '" + ta_jornalists_codes.Value.Replace("'", "''") + "', ";
                SQLquery += " '" + DropDownMeio.SelectedValue.ToString().Replace("'", "''") + "', '" + DropDownCountry.SelectedValue.ToString().Replace("'", "''") + "', '" + DropDownSubject.SelectedValue.ToString().Replace("'", "''") + "' ";

                DataTable Save = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "Save");

                Session["ADVSearchLastSelected"] = txt_aux_novo.Value.Replace("'", "");
                Response.Redirect("AdvSearch.aspx");

            }
            else
            {
                lblDataToday.Visible = true;
                lblDataToday.Text = "É necessário atribuir um nome válido!";
            }
        }

        protected void Apagar_Click(object sender, EventArgs e)
        {

            if (DDSearchs.SelectedValue != "-1")
            {
                String SQLquery = " delete from CL10H_CLIENT_SEARCH ";
                SQLquery += " where ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient + " and ID_CLIENT_USER = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + " and ID_CLIENT_SEARCH = " + DDSearchs.SelectedValue;

                DataTable Delete = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "Delete");

                DDSearchs.Items.Remove(DDSearchs.SelectedItem);


            }
            else
            {
                lblDataToday.Visible = true;
                lblDataToday.Text = "É necessário seleccionar uma pesquisa válida!";
            }
        }

        protected void getSelected(object sender, EventArgs e)
        {

            if (DDSearchs.SelectedValue == "-1")
            {
                Session["ADVSearchLastSelected"] = null;
                Response.Redirect("AdvSearch.aspx");

            }
            else
            {

                String SQLQUERY = "Select * from CL10H_CLIENT_SEARCH with(nolock) where ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient + " and ID_CLIENT_USER = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + " and ID_CLIENT_SEARCH= " + DDSearchs.SelectedValue;
                DataTable temp = Global.InsigteManager[Session.SessionID].getTableQuery(SQLQUERY, "Search");

                foreach (DataRow srch in temp.Rows)
                {

                    tbSearchAdvance.Text = srch["COD_SEARCH_TEXT"].ToString();
                    DDL_Temas.SelectedValue = srch["COD_SEARCH_TEMA"].ToString();
                    DDL_Data.SelectedValue = srch["COD_SEARCH_PERIODO"].ToString();
                    tbDateDe.Text = srch["COD_SEARCH_DAT_INI"].ToString();
                    tbDateAte.Text = srch["COD_SEARCH_DAT_FIM"].ToString();
                    ta_editores_codes.Value = srch["COD_SEARCH_EDITOR"].ToString();
                    ta_jornalists_codes.Value = srch["COD_SEARCH_JORNALIST"].ToString();
                    DropDownMeio.SelectedValue = srch["COD_SEARCH_TIPO"].ToString();
                    DropDownCountry.SelectedValue = srch["COD_SEARCH_PAIS"].ToString();
                    DropDownSubject.SelectedValue = srch["COD_SEARCH_CLASS"].ToString();

                }

            }
        }

        protected void getSelectedNew(object sender, EventArgs e)
        {

            if (DDSearchs.SelectedValue == "-1")
            {
                Session["ADVSearchLastSelected"] = null;
                Response.Redirect("AdvSearch.aspx");

            }
            else
            {

                String SQLQUERY = "Select * from CL10H_CLIENT_SEARCH  with(nolock) where ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient + " and ID_CLIENT_USER = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + " and ID_CLIENT_SEARCH= " + DDSearchs.SelectedValue;
                DataTable temp = Global.InsigteManager[Session.SessionID].getTableQuery(SQLQUERY, "Search");

                foreach (DataRow srch in temp.Rows)
                {

                    tbSearchAdvance.Text = srch["COD_SEARCH_TEXT"].ToString();
                    DDL_Temas.SelectedValue = srch["COD_SEARCH_TEMA"].ToString();
                    DDL_Data.SelectedValue = srch["COD_SEARCH_PERIODO"].ToString();
                    tbDateDe.Text = srch["COD_SEARCH_DAT_INI"].ToString();
                    tbDateAte.Text = srch["COD_SEARCH_DAT_FIM"].ToString();
                    ta_editores_codes.Value = srch["COD_SEARCH_EDITOR"].ToString();
                    ta_jornalists_codes.Value = srch["COD_SEARCH_JORNALIST"].ToString();
                    DropDownMeio.SelectedValue = srch["COD_SEARCH_TIPO"].ToString();
                    DropDownCountry.SelectedValue = srch["COD_SEARCH_PAIS"].ToString();
                    DropDownSubject.SelectedValue = srch["COD_SEARCH_CLASS"].ToString();

                }

            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using APToolkitNET;

namespace Insigte
{
    public partial class PastRmv : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(0, 1) == "0")
            {
                Response.Redirect("Default.aspx");
            }
            if (!Page.IsPostBack)
            {
                string qsIDPasta = Request.QueryString["PID"];

                lbPasta.Text = qsIDPasta;

                getPasta(qsIDPasta);
            }
        }


        public void getPasta(string pasta)
        {

            string cmdQueryPastas = "select id_client, nome, des_pasta, dat_criacao, cod_dia_criacao from CL10D_PASTAS with(nolock) where id_client = " + Global.InsigteManager[Session.SessionID].IdClient + " and id_client_user = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + " and id = " + pasta;
            SqlConnection conn = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");
            try
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand(cmdQueryPastas);
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;

                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows == true)
                {
                    dr.Read();
                    lbPasta.Text = "Deseja mesmo apagar a Pasta : " + dr["nome"].ToString() + " (" + dr["des_pasta"].ToString() + ") e todos os artigos guardados?";
                }

                dr.Close();


            }
            catch (Exception exp)
            {
                exp.Message.ToString();
            }

        }

        protected void btApagar_Click(object sender, EventArgs e)
        {
            string qsIDPasta = Request.QueryString["PID"];

            string cmdQueryPastas = " delete from dbo.CL10D_PASTAS where id = " + qsIDPasta + " and id_client = " + Global.InsigteManager[Session.SessionID].IdClient + " and id_client_user = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + " ";
            string cmdQueryArticles = " delete from dbo.CL10H_PASTAS_ARTICLES where pastaid = " + qsIDPasta + " and id_client = " + Global.InsigteManager[Session.SessionID].IdClient + " and id_client_user = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + " ";
            
            SqlConnection conn = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");
            try
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand(cmdQueryPastas);
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;

                cmd.ExecuteNonQuery();

                SqlCommand cmdAr = new SqlCommand(cmdQueryArticles);
                cmdAr.Connection = conn;
                cmdAr.CommandType = CommandType.Text;

                cmdAr.ExecuteNonQuery();

            }
            catch (Exception exp)
            {
                exp.Message.ToString();
            }
            finally
            {
                conn.Dispose();
                conn.Close();
            }

            Response.Redirect("pastascfg.aspx");

            //Response.Write("<script language='javascript'> { self.close() }</script>");
        }

        protected void cancelar_click(object sender, EventArgs e)
        {
            Response.Redirect("pastascfg.aspx");
        }
    }
}
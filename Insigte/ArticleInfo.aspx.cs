﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using AjaxControlToolkit;


namespace Insigte
{
    public partial class ArticleInfo : BasePage
    {
        string SearchActive = "0";
        string qsIDArtigo = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Global.InsigteManager[Session.SessionID].IsLogged || Global.InsigteManager[Session.SessionID] == null)
            {
                Response.Redirect("login.aspx", true);
            }

            qsIDArtigo = Request.QueryString["ID"];
            SearchActive = Request.QueryString["sh"];
            string qsIDEntidade = Request.QueryString["etd"];

            //ImgShare.ImageUrl = "Cli/" + Global.InsigteManager[Session.SessionID].CodCliente + "/mail32_color.png";
            //ImgPrint.ImageUrl = "Cli/" + Global.InsigteManager[Session.SessionID].CodCliente + "/printer32.png";
            //imgBackArticles.ImageUrl = "Cli/" + Global.InsigteManager[Session.SessionID].CodCliente + "/backarrow32.png";

            ImgShare.ImageUrl = "Imgs/icons/black/png/share_icon_16.png";
            ImgPrint.ImageUrl = "Imgs/icons/black/png//print_icon_16.png";
            imgBackArticles.ImageUrl = "Imgs/icons/black/png/arrow_left_icon_16.png";

            if (!Page.IsPostBack)
            {
                getArtigo(qsIDArtigo, qsIDEntidade);

                //RECEBI UMA ENTIDADE
                if (Request.QueryString["etd"] != null)
                {

                    getEntidade(qsIDArtigo, qsIDEntidade);


                }

            }
        }

        private void getEntidade(string qsIDArtigo, string qsIDEntidade)
        {

            SqlConnection conn3 = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");
            conn3.Open();

            string entidadeSel = " select * from iadvisers.dbo.dim_entidade with(nolock) where id_entidade=" + qsIDEntidade;

            SqlCommand cmdEntS = new SqlCommand(entidadeSel);
            cmdEntS.Connection = conn3;
            cmdEntS.CommandType = CommandType.Text;
            SqlDataReader readerEntSel = cmdEntS.ExecuteReader();

            String Pesquisa = "";

            while (readerEntSel.Read())
            {
                lbArticleInfoTitulo.Text = "<b>" + readerEntSel["nom_entidade"].ToString() + "</b>" + "<br /><br />";

                if (readerEntSel["ceo"].ToString().Length > 0)
                    lbArticleInfoCEO.Text = "CEO: " + readerEntSel["ceo"].ToString() + "<br /><br />";
                else
                    lbArticleInfoCEO.Visible = false;
                
                if (readerEntSel["des_entidade"].ToString().Length > 0)
                    lbArticleInfoText.Text = readerEntSel["des_entidade"].ToString().Replace(Environment.NewLine, "<br />").Replace("\"", "").Replace("'", "");
                else
                    lbArticleInfoText.Visible = false;
                
                if (readerEntSel["telefone"].ToString().Length > 0)
                    lbArticleInfoTelefone.Text = "Telefone: " + readerEntSel["telefone"].ToString() + "<br /><br />";
                else
                    lbArticleInfoTelefone.Visible = false;
                
                if (readerEntSel["fax"].ToString().Length > 0)
                    lbArticleInfoFax.Text = "Fax: " + readerEntSel["fax"].ToString() + "<br /><br />";
                else
                    lbArticleInfoFax.Visible = false;
                
                if (readerEntSel["morada"].ToString().Length > 0)
                    lbArticleInfoMorada.Text = "Endereço: " + readerEntSel["morada"].ToString() + "<br /><br />";
                else
                    lbArticleInfoMorada.Visible = false;
                
                if (readerEntSel["cidade"].ToString().Length > 0)
                    lbArticleInfoCidade.Text = "Cidade: " + readerEntSel["cidade"].ToString() + "<br /><br />";
                else
                    lbArticleInfoCidade.Visible = false;
                
                if (readerEntSel["pais"].ToString().Length > 0)
                    lbArticleInfoPais.Text = "Pais: " + readerEntSel["pais"].ToString() + "<br /><br />";
                else
                    lbArticleInfoPais.Visible = false;
                
                if (readerEntSel["cp"].ToString().Length > 0)
                    lbArticleInfoCP.Text = "CP: " + readerEntSel["cp"].ToString() + "<br /><br />";
                else
                    lbArticleInfoCP.Visible = false;

                if (readerEntSel["website"].ToString().Length > 0)
                    lbArticleInfoWeb.Text = "Web: <a class=\"ent\" href=\"" + readerEntSel["website"].ToString() + "\">" + readerEntSel["website"].ToString() + "</a>" + "<br /><br />";
                else
                    lbArticleInfoWeb.Visible = false;
                //lbArticleInfoWeb.Text = "Web: <a class=\"ent\" href=\"http://www.google.com\">Google</a>";

                String[] trata = readerEntSel["nom_entidade"].ToString().Split(' ');
                foreach (String x in trata)
                {
                    Pesquisa += x + " and ";
                }
                Pesquisa = Pesquisa.Substring(0, Pesquisa.Length - 5);


            }

            conn3.Close();
            conn3.Dispose();

            SqlConnection conn4 = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");
            conn4.Open();

            string entidadeTop5 = " select top 5 m.id, m.date, rtrim(ltrim(m.title)) as title, substring(c.text,1,500) + '...' as text, m.editor from metadata m with(nolock) inner join contents c with(nolock) on m.id = c.id where Contains(c.text, '" + Pesquisa + "') order by m.date desc ";

            SqlCommand cmdEntTop5 = new SqlCommand(entidadeTop5);
            cmdEntTop5.Connection = conn4;
            cmdEntTop5.CommandType = CommandType.Text;
            SqlDataReader readerEntTop5 = cmdEntTop5.ExecuteReader();

            int i = 1;
            while (readerEntTop5.Read())
            {
                //
                lbArticleInfotop5n.Text += "<div class=\"topnews\"><a class=\"fivenews\" title=\"" + readerEntTop5["text"].ToString().Replace("\"", "") + "\" href=\"Article.aspx?ID=" + readerEntTop5["id"].ToString() + "\">" + readerEntTop5["editor"].ToString() + " | " + readerEntTop5["date"].ToString().Substring(0, 10) + " <br> " + readerEntTop5["title"].ToString() + "</a></div>";
                i++;
            }

            conn4.Close();
            conn4.Dispose();



        }

        private void getArtigo(string qsIDArtigo, string qsIDEntidade)
        {
            SqlConnection conn = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");
            try
            {

                SqlConnection conn2 = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");
                conn2.Open();

                string entidade = " select id_entidade, cod_terms from iadvisers.dbo.dim_entidade with(nolock) ";

                SqlCommand cmdEnt = new SqlCommand(entidade);
                cmdEnt.Connection = conn2;
                cmdEnt.CommandType = CommandType.Text;
                SqlDataReader readerEnt = cmdEnt.ExecuteReader();

                List<ListItem> Entidades = new List<ListItem>();
                while (readerEnt.Read())
                {
                    ListItem x = new ListItem(readerEnt["cod_terms"].ToString(), readerEnt["id_entidade"].ToString());
                    Entidades.Add(x);
                }

                conn2.Close();
                conn2.Dispose();


                conn.Open();

                string cmdQuery = "SELECT text, TITLE, EDITOR, DATE, iadvisers.dbo.metadata.ID, filepath, page_url ";
                cmdQuery += "FROM iadvisers.dbo.metadata with(nolock) INNER JOIN iadvisers.dbo.contents with(nolock) ON iadvisers.dbo.metadata.ID=iadvisers.dbo.contents.ID ";
                cmdQuery += "where iadvisers.dbo.metadata.ID='" + qsIDArtigo + "'";

                SqlCommand cmd = new SqlCommand(cmdQuery);
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    lblArticleTitle.Text = reader.GetValue(1).ToString();
                    lblArticleEditorData.Text = "(" + reader.GetValue(2).ToString() + " | " + reader.GetValue(3).ToString().Substring(0, 10) + ")";
                    lblArticleText.Text = reader.GetValue(0).ToString().Replace(Global.InsigteManager[Session.SessionID].CodCliente, "<b>" + Global.InsigteManager[Session.SessionID].CodCliente + "</b>").Replace(Environment.NewLine, "<br />");
                    //lblArticleText.Text = reader.GetValue(0).ToString().Replace(Environment.NewLine, "<br />");

                    foreach (ListItem enti in Entidades)
                    {
                        String[] term = enti.Text.Split(';');
                        foreach (String x in term)
                        {
                            if (enti.Value.ToString() == qsIDEntidade)
                                lblArticleText.Text = lblArticleText.Text.Replace(x, "<a class=\"ent\" href=\"ArticleInfo.aspx?ID=" + qsIDArtigo + "&etd=" + enti.Value + "\"><b>" + x + "</b></a>");
                            else
                                lblArticleText.Text = lblArticleText.Text.Replace(x, "<a class=\"ent\" href=\"ArticleInfo.aspx?ID=" + qsIDArtigo + "&etd=" + enti.Value + "\">" + x + "</a>");
                        }
                    }


                    lblEmailLink.Text = "<a id=\"share-news\" onclick=\"shareTheNews('" + reader.GetValue(4).ToString() + "','" + reader.GetValue(1).ToString().Replace("'", "") + "')\" style='font-family:Arial; font-size:12px; text-decoration:none; color:#000000;' href=\"#\" >" + Resources.insigte.language.shareFormDesc + "</a>";

                    if (reader.GetValue(6).ToString() == null || reader.GetValue(6).ToString() == string.Empty)
                    {
                        String FileHelperPath = "ficheiros";
                        if (Global.InsigteManager[Session.SessionID].CostumPDFLogo == "1" && reader.GetValue(7).ToString() == "Imprensa")
                        {

                            string auxfile = @"G:\cfiles\" + Global.InsigteManager[Session.SessionID].IdClient + "\\" + reader.GetValue(5).ToString().Replace("/", "\\");
                            bool urlexists = INinsigteManager.Utils.URLExists("http://insigte.com/cficheiros/" + Global.InsigteManager[Session.SessionID].IdClient + "\\" + reader.GetValue(5).ToString());

                            if (File.Exists(auxfile) || urlexists)
                            {
                                FileHelperPath = "cficheiros/" + Global.InsigteManager[Session.SessionID].IdClient;
                            }

                            //if (File.Exists(@"G:\cfiles\" + Global.InsigteManager[Session.SessionID].IdClient + "\\" + reader.GetValue(5).ToString().Replace("/", "\\")))
                            //{
                            //    FileHelperPath = "cficheiros/" + Global.InsigteManager[Session.SessionID].IdClient;
                            //}
                        }

                        imgDLorLINK.ImageUrl = "Imgs/icons/black/png/download_icon_16.png";
                        lblPath.Text = "<a href='http://www.insigte.com/" + FileHelperPath + "/" + reader.GetValue(5).ToString() + "' target='_blank' style='font-family:Arial; font-size:12px; text-decoration:none; color:#000000;'>Download</a>";
                    }
                    else
                    {
                        imgDLorLINK.ImageUrl = "Imgs/icons/black/png/link_icon_16.png";
                        imgDLorLINK.Height = 16;
                        lblPath.Text = "<a href='" + reader.GetValue(6).ToString() + "' target='_blank' style='font-family:Arial; font-size:12px; text-decoration:none; color:#000000;'>Link</a>";
                    }

                    if (chkMyNewsArchive(reader.GetValue(4).ToString()))
                    {
                        imgAddMyArticles.ImageUrl = "Imgs/icons/black/png/clipboard_copy_icon_16_sel.png";
                        imgAddMyArticles.ToolTip = Resources.insigte.language.arColClipsGuardados;
                        lblAddMyArticle.Text = Resources.insigte.language.arColClipsGuardados;
                    }
                    else
                    {
                        imgAddMyArticles.ImageUrl = "Imgs/icons/black/png/clipboard_copy_icon_16.png";
                        imgAddMyArticles.ToolTip = Resources.insigte.language.arColClipsAdicionar;
                        lblAddMyArticle.Text = Resources.insigte.language.arColClipsAdicionar;
                    }

                    if (chkMyPDFArchive(reader.GetValue(4).ToString()))
                    {
                        imgPDFMyArticles.ImageUrl = "Imgs/icons/black/png/book_side_icon_16_sel.png";
                        imgPDFMyArticles.ToolTip = Resources.insigte.language.ucArcLink3;
                        lblPDFMyArticle.Text = Resources.insigte.language.ucArcLink3;
                    }
                    else
                    {
                        imgPDFMyArticles.ImageUrl = "Imgs/icons/black/png/book_side_icon_16.png";
                        imgPDFMyArticles.ToolTip = Resources.insigte.language.ucArcLink3;
                        lblPDFMyArticle.Text = Resources.insigte.language.ucArcLink3;
                    }
                }

                cmd.Connection.Close();
                cmd.Connection.Dispose();
            }
            catch (Exception exp)
            {
                exp.Message.ToString();
            }
        }

        private bool chkMyNewsArchive(string idArticle)
        {
            if (HttpContext.Current.Request.Cookies["CookieinsigteSys"] == null)
            {
                return false;
            }
            else
            {
                if (HttpContext.Current.Request.Cookies["CookieinsigteSys"]["ID"] == null)
                {
                    return false;
                }
                else
                {
                    HttpCookie cookie = Request.Cookies["CookieinsigteSys"];
                    string idVal = cookie.Values["ID"];

                    if (idVal.Contains('|'))
                    {
                        string[] words = idVal.Split('|');
                        if (words.Contains(idArticle))
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        if (idVal == idArticle)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
        }

        private bool chkMyPDFArchive(string idArticle)
        {
            if (HttpContext.Current.Request.Cookies["CookieinsigteSys"] == null)
            {
                return false;
            }
            else
            {
                if (HttpContext.Current.Request.Cookies["CookieinsigteSys"]["PDF"] == null)
                {
                    return false;
                }
                else
                {
                    HttpCookie cookie = Request.Cookies["CookieinsigteSys"];
                    string idVal = cookie.Values["PDF"];

                    if (idVal.Contains('|'))
                    {
                        string[] words = idVal.Split('|');
                        if (words.Contains(idArticle))
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        if (idVal == idArticle)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
        }

        protected void bt_AddPDF_Command(Object sender, CommandEventArgs e)
        {
            addLinkPDF(qsIDArtigo);
            imgPDFMyArticles.ImageUrl = "Imgs/icons/black/png/book_side_icon_16_sel.png";
        }

        protected void bt_AddArquivo_Command(Object sender, CommandEventArgs e)
        {
            addNews(qsIDArtigo);

            imgAddMyArticles.ImageUrl = "Imgs/icons/black/png/clipboard_copy_icon_16_sel.png";
            lblAddMyArticle.Text = Resources.insigte.language.arColClipsGuardados;
        }

        protected String getResource(String resx)
        {
            return Resources.insigte.language.ResourceManager.GetObject(resx).ToString();
        }

        protected void addNews(string qsIDArtigo)
        {
            string ValCookie = string.Empty;
            string PDFCookie = string.Empty;
            string DelCookie = string.Empty;

            HttpCookie cookie = Request.Cookies["CookieinsigteSys"];

            if (cookie.Values["ID"] != "")
            {
                ValCookie = cookie.Values["ID"] + "|" + qsIDArtigo;
            }
            else
            {
                ValCookie = qsIDArtigo;
            }

            DelCookie = cookie.Values["DELNOT"];
            PDFCookie = cookie.Values["PDF"];

            cookie.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(cookie);

            HttpCookie newCookie = new HttpCookie("CookieinsigteSys");
            newCookie["Version"] = "JUL2012";
            newCookie["ID"] = ValCookie;
            newCookie["PDF"] = PDFCookie;
            newCookie["DELNOT"] = DelCookie;
            newCookie.Expires = DateTime.Now.AddDays(360);
            Response.Cookies.Add(newCookie);

        }

        protected void addLinkPDF(string qsPDFArtigo)
        {

            string ValCookie = string.Empty;
            string PDFCookie = string.Empty;
            string DelCookie = string.Empty;

            HttpCookie cookie = Request.Cookies["CookieinsigteSys"];

            if (cookie.Values["PDF"] != "")
            {
                PDFCookie = cookie.Values["PDF"] + "|" + qsPDFArtigo;
            }
            else
            {
                PDFCookie = qsPDFArtigo;
            }

            DelCookie = cookie.Values["DELNOT"];
            ValCookie = cookie.Values["ID"];

            cookie.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(cookie);

            HttpCookie newCookie = new HttpCookie("CookieinsigteSys");
            newCookie["Version"] = "JUL2012";
            newCookie["ID"] = ValCookie;
            newCookie["PDF"] = PDFCookie;
            newCookie["DELNOT"] = DelCookie;
            newCookie.Expires = DateTime.Now.AddDays(360);
            Response.Cookies.Add(newCookie);

        }
    }
}